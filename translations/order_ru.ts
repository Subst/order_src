<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>TAttestationModel</name>
    <message>
        <location filename="../src/TAttestationModel.cpp" line="8"/>
        <source>Attestations</source>
        <translation>Виды аттестации</translation>
    </message>
    <message>
        <location filename="../src/TAttestationModel.cpp" line="19"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/TAttestationModel.cpp" line="20"/>
        <source>Attestation</source>
        <translation>Вид аттестации</translation>
    </message>
</context>
<context>
    <name>TCollegeInfoDlg</name>
    <message>
        <location filename="../ui/TCollegeDlg.ui" line="20"/>
        <source>College Info</source>
        <translation>Информация о ВУЗе</translation>
    </message>
    <message>
        <location filename="../ui/TCollegeDlg.ui" line="47"/>
        <source>College Title</source>
        <translation>Наименование ВУЗа</translation>
    </message>
    <message>
        <location filename="../ui/TCollegeDlg.ui" line="66"/>
        <source>City</source>
        <translation>Город</translation>
    </message>
    <message>
        <location filename="../ui/TCollegeDlg.ui" line="85"/>
        <source>Region</source>
        <translation>Область</translation>
    </message>
    <message>
        <location filename="../ui/TCollegeDlg.ui" line="104"/>
        <source>Rector</source>
        <translation>Ректор</translation>
    </message>
    <message>
        <location filename="../ui/TCollegeDlg.ui" line="138"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../ui/TCollegeDlg.ui" line="158"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>TCollegeModel</name>
    <message>
        <location filename="../src/TCollegeModel.cpp" line="8"/>
        <source>College</source>
        <translation>ВУЗ</translation>
    </message>
    <message>
        <location filename="../src/TCollegeModel.cpp" line="17"/>
        <source>Title</source>
        <translation>Наименование ВУЗа</translation>
    </message>
    <message>
        <location filename="../src/TCollegeModel.cpp" line="18"/>
        <source>City</source>
        <translation>Город</translation>
    </message>
    <message>
        <location filename="../src/TCollegeModel.cpp" line="19"/>
        <source>Region</source>
        <translation>Область</translation>
    </message>
    <message>
        <location filename="../src/TCollegeModel.cpp" line="20"/>
        <source>Rector</source>
        <translation>Ректор</translation>
    </message>
</context>
<context>
    <name>TDirectoryDlg</name>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="20"/>
        <source>Directory</source>
        <translation>Словарь</translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="87"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="109"/>
        <location filename="../ui/TDirectoryDlg.ui" line="112"/>
        <source>Remove Record</source>
        <translation>Удалить запись</translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="115"/>
        <source>Ctrl+D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="124"/>
        <location filename="../ui/TDirectoryDlg.ui" line="127"/>
        <source>Append Record</source>
        <translation>Добавить запись</translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="130"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="139"/>
        <location filename="../ui/TDirectoryDlg.ui" line="142"/>
        <source>Edit Record</source>
        <translation>Редактировать запись</translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="145"/>
        <source>Ctrl+E</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="154"/>
        <location filename="../ui/TDirectoryDlg.ui" line="157"/>
        <source>Load List</source>
        <translation>Загрузить список</translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="160"/>
        <source>Ctrl+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="169"/>
        <location filename="../ui/TDirectoryDlg.ui" line="172"/>
        <source>Clear List</source>
        <translation>Очистить список</translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="175"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="187"/>
        <location filename="../ui/TDirectoryDlg.ui" line="190"/>
        <source>Sort List</source>
        <translation>Сортировать список</translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="193"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="202"/>
        <location filename="../ui/TDirectoryDlg.ui" line="205"/>
        <source>Move Up</source>
        <translation>Переместить вверх</translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="208"/>
        <source>Ctrl+Up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="217"/>
        <location filename="../ui/TDirectoryDlg.ui" line="220"/>
        <source>Move Down</source>
        <translation>Переместить вниз</translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="223"/>
        <source>Ctrl+Down</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="232"/>
        <source>Edit in window</source>
        <translation>Редактировать в окне</translation>
    </message>
    <message>
        <location filename="../ui/TDirectoryDlg.ui" line="235"/>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/TDirectoryDlg.cpp" line="156"/>
        <source>New %1</source>
        <translation>Новый %1</translation>
    </message>
    <message>
        <location filename="../src/TDirectoryDlg.cpp" line="191"/>
        <source>selected records</source>
        <translation>выделенные записи</translation>
    </message>
    <message>
        <location filename="../src/TDirectoryDlg.cpp" line="191"/>
        <source>selected record</source>
        <translation>выделенную запись</translation>
    </message>
    <message>
        <location filename="../src/TDirectoryDlg.cpp" line="192"/>
        <source>Confirm removing</source>
        <translation>Подтверждение удаления</translation>
    </message>
    <message>
        <location filename="../src/TDirectoryDlg.cpp" line="192"/>
        <source>Remove %1?</source>
        <translation>Удалить %1?</translation>
    </message>
    <message>
        <location filename="../src/TDirectoryDlg.cpp" line="308"/>
        <source>Open input file &apos;%1&apos;</source>
        <translation>Открыть исходный файл %1</translation>
    </message>
    <message>
        <location filename="../src/TDirectoryDlg.cpp" line="308"/>
        <source>Text files (*.txt *.csv)</source>
        <translation>Текстовые файлы (*.txt *.csv)</translation>
    </message>
    <message>
        <location filename="../src/TDirectoryDlg.cpp" line="316"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/TDirectoryDlg.cpp" line="316"/>
        <source>Can&apos;t open file 
%1</source>
        <translation>Невозможно открыть файл
%1</translation>
    </message>
    <message>
        <location filename="../src/TDirectoryDlg.cpp" line="375"/>
        <source>Confirm clearing</source>
        <translation>Подтверждение очистки</translation>
    </message>
    <message>
        <location filename="../src/TDirectoryDlg.cpp" line="375"/>
        <source>Clear List?</source>
        <translation>Очистить список?</translation>
    </message>
    <message>
        <location filename="../src/TDirectoryDlg.cpp" line="479"/>
        <source>Edit text</source>
        <translation>Редактирование текста</translation>
    </message>
</context>
<context>
    <name>TEditDlg</name>
    <message>
        <location filename="../ui/TEditDlg.ui" line="20"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="../ui/TEditDlg.ui" line="65"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../ui/TEditDlg.ui" line="85"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>TItemDelegate</name>
    <message>
        <location filename="../src/TItemDelegate.cpp" line="44"/>
        <location filename="../src/TItemDelegate.cpp" line="83"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../src/TItemDelegate.cpp" line="44"/>
        <location filename="../src/TItemDelegate.cpp" line="82"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
</context>
<context>
    <name>TMainWnd</name>
    <message>
        <location filename="../ui/TMainWnd.ui" line="20"/>
        <location filename="../src/TMainWnd.cpp" line="674"/>
        <location filename="../src/TMainWnd.cpp" line="706"/>
        <source>Order</source>
        <translation>Приказ</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="546"/>
        <location filename="../ui/TMainWnd.ui" line="756"/>
        <source>Specialities</source>
        <translation>Специальности</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="569"/>
        <location filename="../ui/TMainWnd.ui" line="768"/>
        <source>Profiles</source>
        <translation>Направленности (Профили)</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="579"/>
        <location filename="../ui/TMainWnd.ui" line="780"/>
        <source>Persons</source>
        <translation>Персоны</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="574"/>
        <location filename="../ui/TMainWnd.ui" line="792"/>
        <source>Attestations</source>
        <translation>Виды аттестации</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="71"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="87"/>
        <source>&amp;Options</source>
        <translation>&amp;Опции</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="91"/>
        <source>Directories</source>
        <translation>Словари</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="111"/>
        <source>&amp;Help</source>
        <translation>&amp;Помощь</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="120"/>
        <source>&amp;Report</source>
        <translation>&amp;Отчет</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="129"/>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="150"/>
        <location filename="../ui/TMainWnd.ui" line="696"/>
        <source>Database toolbar</source>
        <translation>Панель баз данных</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="169"/>
        <location filename="../ui/TMainWnd.ui" line="711"/>
        <location filename="../ui/TMainWnd.ui" line="714"/>
        <source>Orders toolbar</source>
        <translation>Панель приказов</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="188"/>
        <location filename="../ui/TMainWnd.ui" line="729"/>
        <source>Reports toolbar</source>
        <translation>Панель отчетов</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="207"/>
        <location filename="../ui/TMainWnd.ui" line="744"/>
        <source>Service toolbar</source>
        <translation>Сервисная панель</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="229"/>
        <source>    Specialities</source>
        <translation>    Специальности</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="260"/>
        <source>Sort Specialities</source>
        <translation>Сортировать Специальности</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="303"/>
        <source>    Profiles</source>
        <translation>    Направленности (Профили)</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="334"/>
        <source>Sort Profiles</source>
        <translation>Сортировать Направленности</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="377"/>
        <source>    Persons</source>
        <translation>    Персоны</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="408"/>
        <source>Sort Persons</source>
        <translation>Сортировать Персоны</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="451"/>
        <source>    Attestations</source>
        <translation>    Виды аттестации</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="482"/>
        <source>Sort Attestations</source>
        <translation>Сортировать Виды аттестации</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="523"/>
        <source>Open Order</source>
        <translation>Открыть Приказ</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="532"/>
        <source>Save Order</source>
        <translation>Сохранить Приказ</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="541"/>
        <source>Exit</source>
        <translation>Выйти из программы</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="555"/>
        <source>About QT</source>
        <translation>О QT</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="564"/>
        <location filename="../src/TMainWnd.cpp" line="394"/>
        <source>About program</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="588"/>
        <location filename="../ui/TMainWnd.ui" line="591"/>
        <source>College Info</source>
        <translation>Информация о ВУЗе</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="600"/>
        <source>New Order</source>
        <translation>Новый Приказ</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="609"/>
        <source>Design Report</source>
        <translation>Шаблон отчета</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="618"/>
        <source>Preview Report</source>
        <translation>Предпросмотр отчета</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="627"/>
        <source>Print Report</source>
        <translation>Печать отчета</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="636"/>
        <source>Print Report To PDF</source>
        <translation>Печать отчета в PDF</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="645"/>
        <location filename="../src/TMainWnd.cpp" line="460"/>
        <location filename="../src/TMainWnd.cpp" line="516"/>
        <source>Create Database</source>
        <translation>Создать БД</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="654"/>
        <location filename="../src/TMainWnd.cpp" line="493"/>
        <source>Open Database</source>
        <translation>Открыть БД</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="663"/>
        <source>Save Database</source>
        <translation>Сохранить БД</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="672"/>
        <location filename="../src/TMainWnd.cpp" line="534"/>
        <source>Remove Database</source>
        <translation>Удалить БД</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="681"/>
        <source>Remove Order</source>
        <translation>Удалить Приказ</translation>
    </message>
    <message>
        <location filename="../ui/TMainWnd.ui" line="801"/>
        <location filename="../ui/TMainWnd.ui" line="804"/>
        <source>Update program</source>
        <translation>Обновить программу</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="63"/>
        <location filename="../src/TMainWnd.cpp" line="85"/>
        <location filename="../src/TMainWnd.cpp" line="109"/>
        <location filename="../src/TMainWnd.cpp" line="469"/>
        <location filename="../src/TMainWnd.cpp" line="479"/>
        <location filename="../src/TMainWnd.cpp" line="501"/>
        <location filename="../src/TMainWnd.cpp" line="524"/>
        <location filename="../src/TMainWnd.cpp" line="543"/>
        <location filename="../src/TMainWnd.cpp" line="548"/>
        <location filename="../src/TMainWnd.cpp" line="636"/>
        <location filename="../src/TMainWnd.cpp" line="642"/>
        <location filename="../src/TMainWnd.cpp" line="702"/>
        <location filename="../src/TMainWnd.cpp" line="973"/>
        <location filename="../src/TMainWnd.cpp" line="1157"/>
        <location filename="../src/TMainWnd.cpp" line="1298"/>
        <location filename="../src/TMainWnd.cpp" line="1302"/>
        <location filename="../src/TMainWnd.cpp" line="1379"/>
        <location filename="../src/TMainWnd.cpp" line="1391"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="63"/>
        <source>Can&apos;t open last working database.
Press &apos;Open&apos; to open existing database
or &apos;Save&apos; to create new one.</source>
        <translation>Невозможно открыть последнюю рабочую БД.
Нажмите &apos;Открыть&apos; для открытия существующей БД
или &apos;Сохранить&apos; для создания новой.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="76"/>
        <source>Select new database</source>
        <translation>Выберите новую БД</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="76"/>
        <location filename="../src/TMainWnd.cpp" line="93"/>
        <location filename="../src/TMainWnd.cpp" line="460"/>
        <location filename="../src/TMainWnd.cpp" line="493"/>
        <location filename="../src/TMainWnd.cpp" line="516"/>
        <location filename="../src/TMainWnd.cpp" line="534"/>
        <source>Database files (*.sqt)</source>
        <translation>Файлы баз данных (*.sqt)</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="85"/>
        <source>Can&apos;t create database file.
%1
Program now exit.</source>
        <translation>Невозможно создать файл БД.
%1
Программа будет закрыта.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="93"/>
        <source>Select existing database</source>
        <translation>Выберите существующую БД</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="109"/>
        <source>Can&apos;t open database file.
%1
Program now exit.</source>
        <translation>Невозможно открыть файл БД.
%1
Программа будет закрыта.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="115"/>
        <location filename="../src/TMainWnd.cpp" line="271"/>
        <location filename="../src/TMainWnd.cpp" line="486"/>
        <location filename="../src/TMainWnd.cpp" line="508"/>
        <location filename="../src/TMainWnd.cpp" line="562"/>
        <location filename="../src/TMainWnd.cpp" line="590"/>
        <location filename="../src/TMainWnd.cpp" line="618"/>
        <source>Order [%1] , [%2]</source>
        <translation>Приказ [%1] , [%2]</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="212"/>
        <source>Append to Order</source>
        <translation>Добавить в Приказ</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="216"/>
        <source>Remove from Order</source>
        <translation>Изъять из Приказа</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="220"/>
        <source>Append empty record</source>
        <translation>Добавить пустую запись</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="224"/>
        <source>Expand All</source>
        <translation>Развернуть все</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="228"/>
        <source>Collapse All</source>
        <translation>Свернуть все</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="236"/>
        <source>Edit in window</source>
        <translation>Редактировать в окне</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="240"/>
        <source>Export Branch</source>
        <translation>Экспорт ветки</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="244"/>
        <source>Import Branch</source>
        <translation>Импорт ветки</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="275"/>
        <location filename="../src/TMainWnd.cpp" line="596"/>
        <location filename="../src/TMainWnd.cpp" line="1319"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="275"/>
        <location filename="../src/TMainWnd.cpp" line="596"/>
        <source>The Order has been automatically corrected
to coply with new order format.</source>
        <translation>Приказ был автоматически скорректирован
для соответствия новому формату приказов.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="399"/>
        <location filename="../src/TMainWnd.cpp" line="411"/>
        <location filename="../src/TMainWnd.cpp" line="538"/>
        <location filename="../src/TMainWnd.cpp" line="555"/>
        <location filename="../src/TMainWnd.cpp" line="569"/>
        <location filename="../src/TMainWnd.cpp" line="631"/>
        <location filename="../src/TMainWnd.cpp" line="1047"/>
        <location filename="../src/TMainWnd.cpp" line="1275"/>
        <location filename="../src/TMainWnd.cpp" line="1334"/>
        <location filename="../src/TMainWnd.cpp" line="1402"/>
        <source>Confirmation</source>
        <translation>Подтверждение</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="399"/>
        <source>Exit program?</source>
        <translation>Выйти из программы?</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="411"/>
        <source>Current Order has been changed.
Save it before exit?</source>
        <translation>Текущий Приказ был изменен.
Сохранить его перед выходом?</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="469"/>
        <source>Can&apos;t create database file.
%1.</source>
        <translation>Невозможно создать файл БД.
%1.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="479"/>
        <location filename="../src/TMainWnd.cpp" line="501"/>
        <source>Can&apos;t open database file.
%1.</source>
        <translation>Невозможно открыть файл БД.
%1.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="524"/>
        <source>Can&apos;t save database file.
%1.</source>
        <translation>Невозможно сохранить файл БД.
%1.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="538"/>
        <source>Remove Database?</source>
        <translation>Удалить БД?</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="543"/>
        <source>Can&apos;t remove current database file.
%1.</source>
        <translation>Невозможно удалить текущий файл БД.
%1.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="548"/>
        <source>Can&apos;t remove database file.
%1.</source>
        <translation>Невозможно удалить файл БД.
%1.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="555"/>
        <location filename="../src/TMainWnd.cpp" line="569"/>
        <source>Current Order has been changed.
Save it before open new one?</source>
        <translation>Текущий Приказ был изменен.
Сохранить его перед открытием нового?</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="561"/>
        <source>untitled</source>
        <translation>без имени</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="727"/>
        <source>Preview - %1</source>
        <translation>Предпросомтр - %1</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="973"/>
        <source>Inpossible to append record
&apos;%1&apos;
to section
&apos;%2&apos;.</source>
        <translation>Невозможно добавить запись
&apos;%1&apos;
в раздел
&apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1047"/>
        <source>Remove the following Item from Order?
&apos;%1&apos;</source>
        <translation>Изъять следующий Элемент из Приказа?
&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1124"/>
        <source>Order file to Export</source>
        <translation>Файл Приказа для Экспорта</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1147"/>
        <source>Order file to Import</source>
        <translation>Файл Приказа для Импорта</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1157"/>
        <source>You can&apos;t import this branch here.</source>
        <translation>Вы не можете импортировать эту ветку сюда.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1273"/>
        <location filename="../src/TMainWnd.cpp" line="1332"/>
        <location filename="../src/TMainWnd.cpp" line="1400"/>
        <source>New &apos;Order&apos; version is ready to install. It&apos;s will be installed when exiting the program.</source>
        <translation>Новая версия &apos;Приказ&apos; готова к установке. Она будет установленна при выходе из программы.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1274"/>
        <location filename="../src/TMainWnd.cpp" line="1333"/>
        <location filename="../src/TMainWnd.cpp" line="1401"/>
        <source>New &apos;Order&apos; version is ready to install.
It&apos;s will be installed when exiting the program.
Exit program now?</source>
        <translation>Новая версия Приказ&apos; готова к установке.
Она будет установленна при выходе из программы.
Выйти сейчас?</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1293"/>
        <location filename="../src/TMainWnd.cpp" line="1298"/>
        <location filename="../src/TMainWnd.cpp" line="1378"/>
        <location filename="../src/TMainWnd.cpp" line="1379"/>
        <location filename="../src/TMainWnd.cpp" line="1390"/>
        <location filename="../src/TMainWnd.cpp" line="1391"/>
        <source>Update error:
%1.</source>
        <translation>Ошибка обновления:
%1.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1302"/>
        <source>Update error. Invalid or expired access token.
If you have valid access token, enter it below.</source>
        <translation>Ошибка обновления. Неверный или истекший токен доступа.
Если у вас есть правильный токен доступа, введите его ниже.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1318"/>
        <location filename="../src/TMainWnd.cpp" line="1319"/>
        <source>Actual &apos;Order&apos; version installed.</source>
        <translation>Установлена актуальная версия &apos;Приказ&apos;.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1344"/>
        <source>Downloading progress</source>
        <translation>Прогресс загрузки</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1345"/>
        <location filename="../src/TMainWnd.cpp" line="1362"/>
        <source>Downloaded %1 percents.</source>
        <translation>Загружено %1 процентов.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1350"/>
        <location filename="../src/TMainWnd.cpp" line="1364"/>
        <source>New &apos;Order&apos; version download process: %1 % completed.</source>
        <translation>Загрузка новой версии &apos;Приказ&apos;: %1 % завершено.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1390"/>
        <location filename="../src/TMainWnd.cpp" line="1391"/>
        <source>Can&apos;t save downloaded file</source>
        <translation>Невозможно сохранить загруженный файл</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1397"/>
        <source>New &apos;Order&apos; version download completed.</source>
        <translation>Загрузка новой версии &apos;Приказ&apos; завершена.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="580"/>
        <location filename="../src/TMainWnd.cpp" line="610"/>
        <location filename="../src/TMainWnd.cpp" line="627"/>
        <location filename="../src/TMainWnd.cpp" line="1124"/>
        <location filename="../src/TMainWnd.cpp" line="1147"/>
        <source>XML files (*.xml)</source>
        <translation>Файлы XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="232"/>
        <source>View in window</source>
        <translation>Показать в окне</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="394"/>
        <source>Order. Bla-bla about...
Copyrigh (c) Denis P.Classen, since 2017.
Version: %1</source>
        <translation>Приказ. Что-то о программе...
Copyrigh (c) Денис П. Классен, с 2017 г.
Версия: %1</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="580"/>
        <location filename="../src/TMainWnd.cpp" line="610"/>
        <source>Order file</source>
        <translation>Файл приказа</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="627"/>
        <source>Remove Order file</source>
        <translation>Удалить файл Приказа</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="631"/>
        <source>Remove Order?</source>
        <translation>Удалить Приказ?</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="636"/>
        <source>Can&apos;t remove current order file.
%1.</source>
        <translation>Невозможно удалить текущий файл Приказа.
%1.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="642"/>
        <source>Can&apos;t remove order file.
%1.</source>
        <translation>Невозможно удалить файл Приказа.
%1.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="651"/>
        <source>Report preparing</source>
        <translation>Подготовка отчета</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="702"/>
        <source>Can&apos;t open report template file
%1.</source>
        <translation>Невозможно jоткрыть шаблон отчета
%1.</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="748"/>
        <source>Save to PDF</source>
        <translation>Сохранить в PDF</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="748"/>
        <source>PDF files (*.pdf)</source>
        <translation>Файлы PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1070"/>
        <source>Show text</source>
        <translation>Отображение текста</translation>
    </message>
    <message>
        <location filename="../src/TMainWnd.cpp" line="1083"/>
        <source>Edit text</source>
        <translation>Редактирование текста</translation>
    </message>
</context>
<context>
    <name>TOrderModel</name>
    <message>
        <location filename="../src/TOrderModel.cpp" line="103"/>
        <source>Title</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../src/TOrderModel.cpp" line="103"/>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <location filename="../src/TOrderModel.cpp" line="103"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../src/TOrderModel.cpp" line="103"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../src/TOrderModel.cpp" line="103"/>
        <source>Permanent</source>
        <translation>Постоянное</translation>
    </message>
    <message>
        <location filename="../src/TOrderModel.cpp" line="107"/>
        <source>Root</source>
        <translation>Корень</translation>
    </message>
    <message>
        <location filename="../src/TOrderModel.cpp" line="610"/>
        <location filename="../src/TOrderModel.cpp" line="660"/>
        <source>Confirmation</source>
        <translation>Подтверждение</translation>
    </message>
    <message>
        <location filename="../src/TOrderModel.cpp" line="610"/>
        <source>The following Item allready exists in Order.
&apos;%1&apos;

What need to do?</source>
        <translation>Следующий Элемент уже присутствует в Приказе.
&apos;%1&apos;

Что необходимо сделать?</translation>
    </message>
    <message>
        <location filename="../src/TOrderModel.cpp" line="613"/>
        <source>Remember the choice.</source>
        <translation>Запомнить выбор.</translation>
    </message>
    <message>
        <location filename="../src/TOrderModel.cpp" line="616"/>
        <source>Replace</source>
        <translation>Заменить</translation>
    </message>
    <message>
        <location filename="../src/TOrderModel.cpp" line="617"/>
        <source>Append</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../src/TOrderModel.cpp" line="618"/>
        <source>Skip</source>
        <translation>Пропустить</translation>
    </message>
    <message>
        <location filename="../src/TOrderModel.cpp" line="660"/>
        <source>Not found the record with same Title,
and can&apos;t append another.
Replace first existing record?</source>
        <translation>Не найдена запись с таким же Заголовком,
и невозможно добавить другую.
Заменить первую существующую запись?</translation>
    </message>
</context>
<context>
    <name>TPersonModel</name>
    <message>
        <location filename="../src/TPersonModel.cpp" line="8"/>
        <source>Persons</source>
        <translation>Персоны</translation>
    </message>
    <message>
        <location filename="../src/TPersonModel.cpp" line="20"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/TPersonModel.cpp" line="21"/>
        <source>Person</source>
        <translation>Персона</translation>
    </message>
    <message>
        <location filename="../src/TPersonModel.cpp" line="22"/>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
</context>
<context>
    <name>TProfileModel</name>
    <message>
        <location filename="../src/TProfileModel.cpp" line="8"/>
        <source>Profiles</source>
        <translation>Направленности (Профили)</translation>
    </message>
    <message>
        <location filename="../src/TProfileModel.cpp" line="21"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/TProfileModel.cpp" line="22"/>
        <source>Profile</source>
        <translation>Направленность (Профиль)</translation>
    </message>
    <message>
        <location filename="../src/TProfileModel.cpp" line="23"/>
        <source>Speciality ID</source>
        <translation>ID Специальности</translation>
    </message>
    <message>
        <location filename="../src/TProfileModel.cpp" line="24"/>
        <source>Speciality</source>
        <translation>Специальность</translation>
    </message>
</context>
<context>
    <name>TSpecialityModel</name>
    <message>
        <location filename="../src/TSpecialityModel.cpp" line="8"/>
        <source>Specialities</source>
        <translation>Специальности</translation>
    </message>
    <message>
        <location filename="../src/TSpecialityModel.cpp" line="19"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/TSpecialityModel.cpp" line="20"/>
        <source>Speciality</source>
        <translation>Специальность</translation>
    </message>
</context>
</TS>
