// spaces
function spaces(count)
  {
  var result="";
  for (i = 0; i < count; i++)
    result = result + "&nbsp;";
  return result;
  }

// table
function createTable(count, values, aligns, widths)
  {
  var result = "<table width='100%' cols='"+count+"'><tr>";
  for (i = 0; i < count; i++)
    {
    let value = values[i];
    let align = aligns[i];
    let width = widths[i];
    result = result + "<td align='" + align + "' width='" +width + "%'>" + value + "</td>";
    }
  return result + "</tr></table>";
  }

// main data band
function createData()
  {
  var previous = getVariable("previous");
  var previousTitle = getVariable("previousTitle");

  if (getField("order.name") != "person") 
    {
    setVariable("previous", getField("order.name"));
    setVariable("previousTitle", getField("order.title"));
    }

  if (getField("order.name") == "order")
    return getField("order.description") + "<br>";

  if (getField("order.name") == "paragraph_data")
    {
    if (getField("order.title") != "")
      return spaces(4) + getField("order.title") + getField("order.description") + "<br>";
    
    return "<br>";
    }

  if (getField("order.name") == "reason_data")
    return "<left>" + spaces(4) + getField("order.title") + spaces(1) + getField("order.description") + "</left>";

  if (getField("order.name") == "speciality" || getField("order.name") == "profile")
    return "<center><b>" + getField("order.title") + "</b></center>";

  if (getField("order.name") == "profile_item")
    return "<center><b>" + getField("order.title") + "</b></center>";
  
  if (getField("order.name") == "attestation_item" && getField("order.title") != "")
    return "<center><b>" + getField("order.title") + "</b></center>";

  if (getField("order.name") == "chairman" || getField("order.name") == "member")
    return "<br><left><b>" + getField("order.title") + "</b></left>";
  
  if (getField("order.name") == "management")
    return "<left><b>" + getField("order.title") + "</b></left><br>";
  
  if (getField("order.name") != "person")
    return "";
  
  if (previous == "chairman" || previous == "member")
    return createTable(3, [getField("order.title"), spaces(8), getField("order.description")], ['left', 'left', 'left'], [30, 10, 60]) + "<br>";

  if (previous == "secretary")
    return "<left><b>Секретарь:</b>" + spaces(1) + getField("order.title") + ", " + spaces(1) + getField("order.description") + "<br>";

  if (previous == "rector")
    return createTable(2, ['Ректор', getField("order.title")], ['left', 'right'], [50, 50]) + "<br>";
        
  if (previous == "management")
    return createTable(2, [getField("order.description"), getField("order.title")], ['left', 'right'], [50, 50]) + "<br>";
      
  return "";
  }