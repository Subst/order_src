TEMPLATE = app
TARGET = Order

QT += core \
      gui \
      widgets \
      sql \
      xml \
      printsupport \
      qml \
      network
#
FORMS += \
    ui/TMainWnd.ui \
    ui/TDirectoryDlg.ui \
    ui/TCollegeDlg.ui \
    ui/TEditDlg.ui

HEADERS += \
    src/3rdparty/QAesEncryption/QAesEncryption.h \
    src/TSqlRelation.h \
    src/TSqlTableModel.h \
    src/TMainWnd.h \
    src/TDataModule.h \
    src/TSettings.h \
    src/TSpecialityModel.h \
    src/TDirectoryDlg.h \
    src/TItemDelegate.h \
    src/TComboBox.h \
    src/TProfileModel.h \
    src/TAttestationModel.h \
    src/TOrderModel.h \
    src/TTableView.h \
    src/TPersonModel.h \
    src/TCollegeDlg.h \
    src/TCollegeModel.h \
    src/TEditDlg.h \
    src/TOrderDelegate.h \
    src/TTreeView.h
SOURCES += src/main.cpp \
    src/3rdparty/QAesEncryption/QAesEncryption.cpp \
    src/TMainWnd.cpp \
    src/TDataModule.cpp \
    src/TSettings.cpp \
    src/TSpecialityModel.cpp \
    src/TDirectoryDlg.cpp \
    src/TItemDelegate.cpp \
    src/TComboBox.cpp \
    src/TProfileModel.cpp \
    src/TAttestationModel.cpp \
    src/TOrderModel.cpp \
    src/TSqlRelation.cpp \
    src/TSqlTableModel.cpp \
    src/TTableView.cpp \
    src/TPersonModel.cpp \
    src/TCollegeDlg.cpp \
    src/TCollegeModel.cpp \
    src/TEditDlg.cpp \
    src/TOrderDelegate.cpp \
    src/TTreeView.cpp

UI_DIR = ui_src
DESTDIR = bin
MOC_DIR = moc
OBJECTS_DIR = obj

RC_ICONS = res/order.ico
TRANSLATIONS = translations/order_ru.ts

RESOURCES += res/order.qrc

INCLUDEPATH += $$PWD/src/3rdparty/QAesEncryption
LIMEREPORT_DIR = $$[QT_INSTALL_PREFIX]/3rdparty/LimeReport
#LIBS += -L$$LIMEREPORT_DIR/bin
LIBS += -L$$LIMEREPORT_DIR/lib -llimereport -lQtZint
INCLUDEPATH += $$LIMEREPORT_DIR/include

DEFINES += LIMEREPORT_IMPORTS

# TSPU_Order
# TODO: надо ли добавить строку поиска над orderTree ?
# А как насчет QAbstraxtXmlNodeModel, может для TOrderModel заюзать?
# TODO: запилить patchConfig() а-ля Quectel ?
# TODO: срипты переделать в python'е
VERSION = 3.2.0.7

QMAKE_TARGET_COMPANY = TSPU
QMAKE_TARGET_PRODUCT = Order
QMAKE_TARGET_DESCRIPTION = Order
QMAKE_TARGET_COPYRIGHT =  Den P.Classen (C)

DEFINES -= VERSION
DEFINES += ORDER_VERSION=\\\"$$VERSION\\\"

win32 {
  message($${VERSION})
  system(cmd /c echo $${VERSION}>$${OUT_PWD}/$${DESTDIR}/version)
  #QMAKE_POST_LINK += cmd /c $${PWD}/scripts/installer.bat
  }

#DISTFILES += \
QMAKE_CXXFLAGS += -Wno-unused-variable -Wno-unused-parameter -Wno-sign-compare# -Wno-attributes

