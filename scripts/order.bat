@echo off
rem сразу в UTF8 уйдем
chcp 65001

Setlocal EnableDelayedExpansion
for %%v in (x64 x86) do (
  set variant=%%v
	if !variant!==x64 ( 
	  set exception_file=libgcc_s_seh-1.dll
		set ssl_file=libssl-1_1-x64.dll
		set crypto_file=libcrypto-1_1-x64.dll
	  ) else (
		set exception_file=libgcc_s_dw2-1.dll
		set ssl_file=libssl-1_1.dll
		set crypto_file=libcrypto-1_1.dll
		)
  
  rem Order
  copy D:\QtProjects\Order\translations\order_ru.qm D:\QtProjects\order\Qt_win_!variant!\Release\bin\translations\order_ru.qm
  copy D:\QtProjects\Order\translations\order_ru.qm E:\QtDistr\Order_install\source\!variant!\translations\order_ru.qm
  copy D:\QtProjects\Order\Qt_win_!variant!\Release\bin\Order.exe E:\QtDistr\Order_install\source\!variant!\Order.exe
	if ERRORLEVEL 1 goto :eof	
 
  rem mingw
  copy D:\Programs\MinGW\!variant!\bin\!exception_file! E:\QtDistr\Order_install\source\!variant!\!exception_file!
  copy "D:\Programs\MinGW\!variant!\bin\libstdc++-6.dll" "E:\QtDistr\Order_install\source\!variant!\libstdc++-6.dll"
  copy D:\Programs\MinGW\!variant!\bin\libwinpthread-1.dll E:\QtDistr\Order_install\source\!variant!\libwinpthread-1.dll
	if ERRORLEVEL 1 goto :eof

  rem openSSL
  copy D:\ToolChains\Win_!variant!\OpenSSL_1.1\bin\!ssl_file! E:\QtDistr\Order_install\source\!variant!\!ssl_file!
  copy D:\ToolChains\Win_!variant!\OpenSSL_1.1\bin\!crypto_file! E:\QtDistr\Order_install\source\!variant!\!crypto_file!
	if ERRORLEVEL 1 goto :eof

  rem LimeReport
  copy D:\ToolChains\Win_!variant!\Qt_5.15.2\3rdparty\LimeReport\bin\limereport.dll E:\QtDistr\Order_install\source\!variant!\limereport.dll
  copy D:\ToolChains\Win_!variant!\Qt_5.15.2\3rdparty\LimeReport\bin\QtZint.dll E:\QtDistr\Order_install\source\!variant!\QtZint.dll
  copy D:\ToolChains\Win_!variant!\Qt_5.15.2\3rdparty\LimeReport\bin\LRDesigner.exe E:\QtDistr\Order_install\source\!variant!\LRDesigner.exe
  copy D:\ToolChains\Win_!variant!\Qt_5.15.2\3rdparty\LimeReport\bin\translations\limereport_ru.qm E:\QtDistr\Order_install\source\!variant!\translations\limereport_ru.qm
	if ERRORLEVEL 1 goto :eof
  )
	
echo done.