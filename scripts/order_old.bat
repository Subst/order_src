rem x86 Order

move E:\QtDistr\Order\x86\order.ini E:\order.ini

rmdir /S /Q E:\QtDistr\Order\x86

mkdir E:\QtDistr\Order\x86
mkdir E:\QtDistr\Order\x86\sqldrivers
mkdir E:\QtDistr\Order\x86\printsupport
mkdir E:\QtDistr\Order\x86\iconengines
mkdir E:\QtDistr\Order\x86\imageformats
mkdir E:\QtDistr\Order\x86\platforms
mkdir E:\QtDistr\Order\x86\styles
mkdir E:\QtDistr\Order\x86\translations
mkdir E:\QtDistr\Order\x86\report
mkdir E:\QtDistr\Order\x86\templates

move E:\order.ini E:\QtDistr\Order\x86\order.ini

copy D:\QtProjects\Order\Qt_win_x86\Release\bin\Order.exe E:\QtDistr\Order\x86\Order.exe
copy D:\QtProjects\Order\translations\order_ru.qm E:\QtDistr\Order\x86\translations\order_ru.qm

copy D:\QtProjects\Order\files\report\order.lrxml E:\QtDistr\Order\x86\report\order.lrxml

copy D:\QtProjects\Order\res\*.xml E:\QtDistr\Order\x86\templates\*.xml

rem QT libs

copy D:\ToolChains\win_x86\Qt\bin\Qt5Core.dll E:\QtDistr\Order\x86\Qt5Core.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Gui.dll E:\QtDistr\Order\x86\Qt5Gui.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Widgets.dll E:\QtDistr\Order\x86\Qt5Widgets.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5PrintSupport.dll E:\QtDistr\Order\x86\Qt5PrintSupport.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Sql.dll E:\QtDistr\Order\x86\Qt5Sql.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Xml.dll E:\QtDistr\Order\x86\Qt5Xml.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Qml.dll E:\QtDistr\Order\x86\Qt5Qml.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Designer.dll E:\QtDistr\Order\x86\Qt5Designer.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5DesignerComponents.dll E:\QtDistr\Order\x86\Qt5DesignerComponents.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Network.dll E:\QtDistr\Order\x86\Qt5Network.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Svg.dll E:\QtDistr\Order\x86\Qt5Svg.dll

rem QT plugins

copy D:\ToolChains\win_x86\Qt\plugins\sqldrivers\qsqlite.dll E:\QtDistr\Order\x86\sqldrivers\qsqlite.dll
copy D:\ToolChains\win_x86\Qt\plugins\printsupport\windowsprintersupport.dll E:\QtDistr\Order\x86\printsupport\windowsprintersupport.dll
copy D:\ToolChains\win_x86\Qt\plugins\iconengines\*.dll E:\QtDistr\Order\x86\iconengines\*.dll
copy D:\ToolChains\win_x86\Qt\plugins\imageformats\*.dll E:\QtDistr\Order\x86\imageformats\*.dll
copy D:\ToolChains\win_x86\Qt\plugins\platforms\*.dll E:\QtDistr\Order\x86\platforms\*.dll
copy D:\ToolChains\win_x86\Qt\plugins\styles\*.dll E:\QtDistr\Order\x86\styles\*.dll

copy D:\ToolChains\win_x86\Qt\translations\qtbase_ru.qm E:\QtDistr\Order\x86\translations\qtbase_ru.qm
copy D:\ToolChains\win_x86\Qt\translations\designer_ru.qm E:\QtDistr\Order\x86\translations\designer_ru.qm
rem copy D:\ToolChains\win_x86\Qt\translations\linguist_ru.qm E:\QtDistr\Order\x86\translations\linguist_ru.qm

rem mingw

copy D:\Programs\MinGW\x86\bin\libgcc_s_dw2-1.dll E:\QtDistr\Order\x86\libgcc_s_dw2-1.dll
copy "D:\Programs\MinGW\x86\bin\libstdc++-6.dll" "E:\QtDistr\Order\x86\libstdc++-6.dll"
copy D:\Programs\MinGW\x86\bin\libwinpthread-1.dll E:\QtDistr\Order\x86\libwinpthread-1.dll

rem LimeReport

copy D:\ToolChains\win_x86\Qt\3rdparty\LimeReport\bin\limereport.dll E:\QtDistr\Order\x86\limereport.dll
copy D:\ToolChains\win_x86\Qt\3rdparty\LimeReport\bin\QtZint.dll E:\QtDistr\Order\x86\QtZint.dll
copy D:\ToolChains\win_x86\Qt\3rdparty\LimeReport\bin\LRDesigner.exe E:\QtDistr\Order\x86\LRDesigner.exe
copy D:\ToolChains\win_x86\Qt\3rdparty\LimeReport\bin\translations\limereport_ru.qm E:\QtDistr\Order\x86\translations\limereport_ru.qm

rem x64 Order

move E:\QtDistr\Order\x64\order.ini E:\order.ini

rmdir /S /Q E:\QtDistr\Order\x64

mkdir E:\QtDistr\Order\x64
mkdir E:\QtDistr\Order\x64\sqldrivers
mkdir E:\QtDistr\Order\x64\printsupport
mkdir E:\QtDistr\Order\x64\iconengines
mkdir E:\QtDistr\Order\x64\imageformats
mkdir E:\QtDistr\Order\x64\platforms
mkdir E:\QtDistr\Order\x64\styles
mkdir E:\QtDistr\Order\x64\translations
mkdir E:\QtDistr\Order\x64\report
mkdir E:\QtDistr\Order\x64\templates

move E:\order.ini E:\QtDistr\Order\x64\order.ini

copy D:\QtProjects\Order\Qt_win_x64\Release\bin\Order.exe E:\QtDistr\Order\x64\Order.exe
copy D:\QtProjects\Order\translations\order_ru.qm E:\QtDistr\Order\x64\translations\order_ru.qm

copy D:\QtProjects\Order\files\report\order.lrxml E:\QtDistr\Order\x64\report\order.lrxml

copy D:\QtProjects\Order\res\*.xml E:\QtDistr\Order\x64\templates\*.xml

rem QT libs

copy D:\ToolChains\win_x64\Qt\bin\Qt5Core.dll E:\QtDistr\Order\x64\Qt5Core.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Gui.dll E:\QtDistr\Order\x64\Qt5Gui.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Widgets.dll E:\QtDistr\Order\x64\Qt5Widgets.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5PrintSupport.dll E:\QtDistr\Order\x64\Qt5PrintSupport.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Sql.dll E:\QtDistr\Order\x64\Qt5Sql.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Xml.dll E:\QtDistr\Order\x64\Qt5Xml.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Qml.dll E:\QtDistr\Order\x64\Qt5Qml.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Designer.dll E:\QtDistr\Order\x64\Qt5Designer.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5DesignerComponents.dll E:\QtDistr\Order\x64\Qt5DesignerComponents.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Network.dll E:\QtDistr\Order\x64\Qt5Network.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Svg.dll E:\QtDistr\Order\x64\Qt5Svg.dll

rem QT plugins

copy D:\ToolChains\win_x64\Qt\plugins\sqldrivers\qsqlite.dll E:\QtDistr\Order\x64\sqldrivers\qsqlite.dll
copy D:\ToolChains\win_x64\Qt\plugins\printsupport\windowsprintersupport.dll E:\QtDistr\Order\x64\printsupport\windowsprintersupport.dll
copy D:\ToolChains\win_x64\Qt\plugins\iconengines\*.dll E:\QtDistr\Order\x64\iconengines\*.dll
copy D:\ToolChains\win_x64\Qt\plugins\imageformats\*.dll E:\QtDistr\Order\x64\imageformats\*.dll
copy D:\ToolChains\win_x64\Qt\plugins\platforms\*.dll E:\QtDistr\Order\x64\platforms\*.dll
copy D:\ToolChains\win_x64\Qt\plugins\styles\*.dll E:\QtDistr\Order\x64\styles\*.dll

copy D:\ToolChains\win_x64\Qt\translations\qtbase_ru.qm E:\QtDistr\Order\x64\translations\qtbase_ru.qm
copy D:\ToolChains\win_x64\Qt\translations\designer_ru.qm E:\QtDistr\Order\x64\translations\designer_ru.qm
rem copy D:\ToolChains\win_x64\Qt\translations\linguist_ru.qm E:\QtDistr\Order\x64\translations\linguist_ru.qm

rem mingw

copy D:\Programs\MinGW\x64\bin\libgcc_s_seh-1.dll E:\QtDistr\Order\x64\libgcc_s_seh-1.dll
copy "D:\Programs\MinGW\x64\bin\libstdc++-6.dll" "E:\QtDistr\Order\x64\libstdc++-6.dll"
copy D:\Programs\MinGW\x64\bin\libwinpthread-1.dll E:\QtDistr\Order\x64\libwinpthread-1.dll

rem LimeReport

copy D:\ToolChains\win_x64\Qt\3rdparty\LimeReport\bin\limereport.dll E:\QtDistr\Order\x64\limereport.dll
copy D:\ToolChains\win_x64\Qt\3rdparty\LimeReport\bin\QtZint.dll E:\QtDistr\Order\x64\QtZint.dll
copy D:\ToolChains\win_x64\Qt\3rdparty\LimeReport\bin\LRDesigner.exe E:\QtDistr\Order\x64\LRDesigner.exe
copy D:\ToolChains\win_x64\Qt\3rdparty\LimeReport\bin\translations\limereport_ru.qm E:\QtDistr\Order\x64\translations\limereport_ru.qm

rem x86 Order_install

copy D:\QtProjects\Order\Qt_win_x86\Release\bin\Order.exe E:\QtDistr\Order_install\x86\packages\Order\data\Order.exe
copy D:\QtProjects\Order\translations\order_ru.qm E:\QtDistr\Order_install\x86\packages\Order\data\translations\order_ru.qm

copy D:\QtProjects\Order\files\report\order.lrxml E:\QtDistr\Order_install\x86\packages\Report\data\report\order.lrxml

copy D:\QtProjects\Order\res\*.xml E:\QtDistr\Order_install\x86\packages\Order\data\orders\templates\*.xml

copy D:\ToolChains\win_x86\Qt\bin\Qt5Core.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\Qt5Core.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Gui.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\Qt5Gui.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Widgets.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\Qt5Widgets.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5PrintSupport.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\Qt5PrintSupport.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Sql.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\Qt5Sql.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Xml.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\Qt5Xml.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Qml.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\Qt5Qml.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Designer.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\Qt5Designer.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5DesignerComponents.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\Qt5DesignerComponents.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Network.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\Qt5Network.dll
copy D:\ToolChains\win_x86\Qt\bin\Qt5Svg.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\Qt5Svg.dll

copy D:\ToolChains\win_x86\Qt\plugins\sqldrivers\qsqlite.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\sqldrivers\qsqlite.dll
copy D:\ToolChains\win_x86\Qt\plugins\printsupport\windowsprintersupport.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\printsupport\windowsprintersupport.dll
copy D:\ToolChains\win_x86\Qt\plugins\iconengines\*.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\iconengines\*.dll
copy D:\ToolChains\win_x86\Qt\plugins\imageformats\*.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\imageformats\*.dll
copy D:\ToolChains\win_x86\Qt\plugins\platforms\*.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\platforms\*.dll
copy D:\ToolChains\win_x86\Qt\plugins\styles\*.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\styles\*.dll

copy D:\ToolChains\win_x86\Qt\translations\qtbase_ru.qm E:\QtDistr\Order_install\x86\packages\QtLibraries\data\translations\qtbase_ru.qm
copy D:\ToolChains\win_x86\Qt\translations\designer_ru.qm E:\QtDistr\Order_install\x86\packages\QtLibraries\data\translations\designer_ru.qm
rem copy D:\ToolChains\win_x86\Qt\translations\linguist_ru.qm E:\QtDistr\Order_install\x86\packages\QtLibraries\data\translations\linguist_ru.qm

copy D:\Programs\MinGW\x86\bin\libgcc_s_dw2-1.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\libgcc_s_dw2-1.dll
copy "D:\Programs\MinGW\x86\bin\libstdc++-6.dll" "E:\QtDistr\Order_install\x86\packages\QtLibraries\data\libstdc++-6.dll"
copy D:\Programs\MinGW\x86\bin\libwinpthread-1.dll E:\QtDistr\Order_install\x86\packages\QtLibraries\data\libwinpthread-1.dll

copy D:\ToolChains\win_x86\Qt\3rdparty\LimeReport\bin\limereport.dll E:\QtDistr\Order_install\x86\packages\LimeReport\data\limereport.dll
copy D:\ToolChains\win_x86\Qt\3rdparty\LimeReport\bin\QtZint.dll E:\QtDistr\Order_install\x86\packages\LimeReport\data\QtZint.dll
copy D:\ToolChains\win_x86\Qt\3rdparty\LimeReport\bin\LRDesigner.exe E:\QtDistr\Order_install\x86\packages\LimeReport\data\LRDesigner.exe

copy D:\ToolChains\win_x86\Qt\3rdparty\LimeReport\bin\translations\limereport_ru.qm E:\QtDistr\Order_install\x86\packages\LimeReport\data\translations\limereport_ru.qm

cd D:\Programs\QtInstaller\bin\
call D:\Programs\QtInstaller\bin\binarycreator.exe --offline-only -t installerbase.exe -p E:\QtDistr\Order_install\x86\packages -c E:\QtDistr\Order_install\x86\config\config.xml E:\QtDistr\Order_install\Order_install_x86.exe

rem x64 Order_install

copy D:\QtProjects\Order\Qt_win_x64\Release\bin\Order.exe E:\QtDistr\Order_install\x64\packages\Order\data\Order.exe
copy D:\QtProjects\Order\translations\order_ru.qm E:\QtDistr\Order_install\x64\packages\Order\data\translations\order_ru.qm

copy D:\QtProjects\Order\files\report\order.lrxml E:\QtDistr\Order_install\x64\packages\Report\data\report\order.lrxml

copy D:\QtProjects\Order\res\*.xml E:\QtDistr\Order_install\x64\packages\Order\data\orders\templates\*.xml

copy D:\ToolChains\win_x64\Qt\bin\Qt5Core.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\Qt5Core.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Gui.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\Qt5Gui.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Widgets.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\Qt5Widgets.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5PrintSupport.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\Qt5PrintSupport.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Sql.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\Qt5Sql.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Xml.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\Qt5Xml.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Qml.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\Qt5Qml.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Designer.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\Qt5Designer.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5DesignerComponents.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\Qt5DesignerComponents.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Network.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\Qt5Network.dll
copy D:\ToolChains\win_x64\Qt\bin\Qt5Svg.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\Qt5Svg.dll

copy D:\ToolChains\win_x64\Qt\plugins\sqldrivers\qsqlite.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\sqldrivers\qsqlite.dll
copy D:\ToolChains\win_x64\Qt\plugins\printsupport\windowsprintersupport.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\printsupport\windowsprintersupport.dll
copy D:\ToolChains\win_x64\Qt\plugins\iconengines\*.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\iconengines\*.dll
copy D:\ToolChains\win_x64\Qt\plugins\imageformats\*.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\imageformats\*.dll
copy D:\ToolChains\win_x64\Qt\plugins\platforms\*.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\platforms\*.dll
copy D:\ToolChains\win_x64\Qt\plugins\styles\*.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\styles\*.dll

copy D:\ToolChains\win_x64\Qt\translations\qtbase_ru.qm E:\QtDistr\Order_install\x64\packages\QtLibraries\data\translations\qtbase_ru.qm
copy D:\ToolChains\win_x64\Qt\translations\designer_ru.qm E:\QtDistr\Order_install\x64\packages\QtLibraries\data\translations\designer_ru.qm
rem copy D:\ToolChains\win_x64\Qt\translations\linguist_ru.qm E:\QtDistr\Order_install\x64\packages\QtLibraries\data\translations\linguist_ru.qm

copy D:\Programs\MinGW\x64\bin\libgcc_s_seh-1.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\libgcc_s_seh-1.dll
copy "D:\Programs\MinGW\x64\bin\libstdc++-6.dll" "E:\QtDistr\Order_install\x64\packages\QtLibraries\data\libstdc++-6.dll"
copy D:\Programs\MinGW\x64\bin\libwinpthread-1.dll E:\QtDistr\Order_install\x64\packages\QtLibraries\data\libwinpthread-1.dll

copy D:\ToolChains\win_x64\Qt\3rdparty\LimeReport\bin\limereport.dll E:\QtDistr\Order_install\x64\packages\LimeReport\data\limereport.dll
copy D:\ToolChains\win_x64\Qt\3rdparty\LimeReport\bin\QtZint.dll E:\QtDistr\Order_install\x64\packages\LimeReport\data\QtZint.dll
copy D:\ToolChains\win_x64\Qt\3rdparty\LimeReport\bin\LRDesigner.exe E:\QtDistr\Order_install\x64\packages\LimeReport\data\LRDesigner.exe

copy D:\ToolChains\win_x64\Qt\3rdparty\LimeReport\bin\translations\limereport_ru.qm E:\QtDistr\Order_install\x64\packages\LimeReport\data\translations\limereport_ru.qm

cd D:\Programs\QtInstaller\bin\
call D:\Programs\QtInstaller\bin\binarycreator.exe --offline-only -t installerbase.exe -p E:\QtDistr\Order_install\x64\packages -c E:\QtDistr\Order_install\x64\config\config.xml E:\QtDistr\Order_install\Order_install_x64.exe

pause