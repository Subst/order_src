rem @echo off
setlocal enabledelayedexpansion
rem TODO: эта иконка только для отображения в Программы и компоненты.
set icon_path=%root_path%\order.ico
set GUID=A240FA27-925A-635B-08DC-28C9E4F9216D
set product_name=Order
rem Мне табы надо ставить только в целях отлдаки, если что. Для создания msi табы не нужны
set "tab=  "
rem Для сквозной нумерации компонент нужен
set /a count=1
set components=

rem Для уникальности и в тоже время предсказуемости Product Id формируем его из GUID и версии Агента
set version_string=!order_version:.= !
set through_version=
for %%s in (!version_string!) do (
  set part=%%s
  set part=0!part!
  set part=!part:~-2!
  set through_version=!through_version!!part!
  )
set version_guid=!GUID:~0,28!!through_version!
rem Эта часть будет для поиска установленного Order использоваться
set version_part=!GUID:~0,28!

echo ^<^?xml version="1.0"^?^>
echo ^<Wix xmlns="http://schemas.microsoft.com/wix/2006/wi" xmlns:util="http://schemas.microsoft.com/wix/UtilExtension"^>
echo !tab!^<Product Id="!version_guid!" Name="!product_name!" Language="1049" Codepage="65001" Version="!order_version!" Manufacturer="Den P.Classen" UpgradeCode="!GUID!"^>
echo !tab!!tab!^<Package Id="*" Description="Order installer package" Comments="Comment for ..." Manufacturer="TSPU" Platform="!variant!" InstallerVersion="300" Compressed="yes" InstallScope="perMachine"/^>
rem InstallScope="perMachine", но можно же еще "perUser" или как-то так, куда ставить будем в конечном итоге? От этого зависиь, и где потом искать
echo !tab!!tab!^<Icon Id="order.ico" SourceFile="!icon_path!"/^>
echo !tab!!tab!^<Property Id="ARPPRODUCTICON" Value="order.ico"/^>
echo !tab!!tab!^<Media Id="1" Cabinet="Order.cab" EmbedCab="yes"/^>
echo.
rem echo ^<!-- По поводу обновлений, обновляем все и в любую сторону, будет Warning, не обращать внимание --^>
rem echo !tab!!tab!^<Upgrade Id="!GUID!"^>
rem echo !tab!!tab!!tab!^<UpgradeVersion OnlyDetect="no" Minimum="0.0.0.0" Maximum="99.99.99.99" Property="OLDERVERSIONBEINGUPGRADED" IncludeMinimum="yes" IncludeMaximum="yes"/^>
rem echo !tab!!tab!^</Upgrade^>
echo !tab!!tab!^<MajorUpgrade AllowDowngrades="yes" Schedule="afterInstallInitialize"/^>
echo.
echo !tab!!tab!^<Property Id="PartiallyGuid"^>!version_part!^</Property^>
echo !tab!!tab!^<Property Id="FullGuid"^>!version_guid!^</Property^>
echo.
echo !tab!!tab!^<SetDirectory Id="WINDOWSVOLUME" Value="[WindowsVolume]"/^>

echo !tab!!tab!^<Directory Id="TARGETDIR" Name="SourceDir"^>
echo !tab!!tab!!tab!^<Directory Id="WINDOWSVOLUME" Name="ProgramFiles"^>
echo !tab!!tab!!tab!!tab!^<Directory Id="INSTALL_DIRECTORY" Name="Order"^>

set prefix=!tab!!tab!!tab!!tab!
set /a level=0

for /r %deploy_path% %%d in (.) do (
  set absolute=%%d
  set absolute=!absolute:~0,-2!
  set relative=!absolute:%deploy_path%\=!

  for /f %%s in ("!relative!") do (
    set folder=%%~ns
    )

  set /a previous=!level!
  if !deploy_path!==!relative! (
    set directory_id=Order
    set /a level=0
    ) else (
    set directory_id=Order_!relative:\=_!
    rem НАДО подсчитать уровень вложенности папки -  ЭТО просто кол-во \ в relative
    set /a level=1
    set line=!relative!
    call :find_slash
    )

  if !level! equ !previous! (
    if !deploy_path! neq !relative! (
      echo !prefix!^</Directory^>
      echo !prefix!^<Directory Id="!directory_id!" Name="!folder!"^>
      )
    ) else (
    if !level! gtr !previous! (
      set prefix=!prefix!!tab!
      echo !prefix!^<Directory Id="!directory_id!" Name="!folder!"^>
      ) else (
      for /l %%i in (!level!,1,!previous!) do (
        echo !prefix!^</Directory^>
        set prefix=!prefix:~0,-1!
        )
      set prefix=!prefix!!tab!
      echo !prefix!^<Directory Id="!directory_id!" Name="!folder!"^>
      )
    )

  if !variant!==x64 (
    set is_x64=yes
    ) else (
    set is_x64=no
    )
    
  call :form_file_guid
  echo !prefix!!tab!^<Component Id="!directory_id!_!count!" Win64="!is_x64!" Guid="!file_guid!"^>
  set components=!components! !directory_id!_!count!
  for %%f in (!absolute!\*) do (
    set file_id=%%~nxf
    call :correct_file_id
    echo !prefix!!tab!!tab!^<File Id="!file_id!" Name="%%~nxf" Source="%%~ff" DiskId="1" Checksum="yes"/^>
    )
  echo !prefix!!tab!^</Component^>
  )

echo !prefix!^</Directory^>
echo !tab!!tab!!tab!!tab!^</Directory^>
echo !tab!!tab!!tab!^</Directory^>

echo !tab!!tab!!tab!^<Directory Id="ProgramMenuFolder"^>
echo !tab!!tab!!tab!!tab!^<Directory Id="ApplicationProgramsFolder" Name="Order"^>
call :form_file_guid
echo !prefix!^<Component Id="Programs_Shortcut_!count!" Guid="!file_guid!"^>
set components=!components! Programs_Shortcut_!count!
echo !prefix!!tab!^<Shortcut Id="Programs_Shortcut" Name="Order" Description="Order" Target="[INSTALL_DIRECTORY]Order.exe" WorkingDirectory="INSTALL_DIRECTORY"/^>
echo !prefix!!tab!^<Shortcut Id="Uninstall_Shortcut" Name="Uninstall Ordera" Description="Uninstall Order" Target="[SystemFolder]msiexec.exe" Arguments="/uninstall [ProductCode]"/^>
echo !prefix!!tab!^<RemoveFolder Id="ApplicationProgramsFolder" On="uninstall"/^>
echo !prefix!!tab!^<RegistryValue Root="HKCU" Key="Software\TSPU\Order" Name="installed" Type="integer" Value="1" KeyPath="yes"/^>
echo !prefix!^</Component^>
echo !tab!!tab!!tab!!tab!^</Directory^>
echo !tab!!tab!!tab!^</Directory^>

echo !tab!!tab!!tab!^<Directory Id="DesktopFolder" Name="Desktop"^>
call :form_file_guid
echo !tab!!tab!!tab!!tab!^<Component Id="Desktop_Shortcut_!count!" Guid="!file_guid!"^>
set components=!components! Desktop_Shortcut_!count!
echo !prefix!^<Shortcut Id="Desktop_Shortcut" Name="Order" Description="Order" Target="[INSTALL_DIRECTORY]Order.exe" WorkingDirectory="INSTALL_DIRECTORY"/^>
echo !prefix!^<RegistryValue Root="HKCU" Key="Software\TSPU\Order" Name="installed" Type="integer" Value="1" KeyPath="yes"/^>
echo !tab!!tab!!tab!!tab!^</Component^>
echo !tab!!tab!!tab!^</Directory^>

echo !tab!!tab!^</Directory^>

echo !tab!!tab!^<Feature Id="Order" Title="Order" Level="1"^>
for %%s in (!components!) do (
  echo !tab!!tab!!tab!^<ComponentRef Id="%%s"/^>
  )
echo !tab!!tab!^</Feature^>
echo !tab!!tab!^<util:CloseApplication Id="CloseOrder" Target="Order.exe" CloseMessage="yes" RebootPrompt="no" Timeout="30"^>^</util:CloseApplication^>
echo !tab!^</Product^>
echo ^</Wix^>

endlocal
goto :eof


:permanent_component
echo !prefix!!tab!^</Component^>
call :form_file_guid
echo !prefix!!tab!^<Component Id="!directory_id!_!count!" Win64="!is_x64!" Guid="!file_guid!" Permanent="yes" NeverOverwrite="yes"^>
set components=!components! !directory_id!_!count!
echo !prefix!!tab!!tab!^<File Id="!file_id!" Name="!permanent_name!" Source="!permanent_file!" DiskId="1" Checksum="yes"/^>
echo !prefix!!tab!^</Component^>
call :form_file_guid
echo !prefix!!tab!^<Component Id="!directory_id!_!count!" Win64="!is_x64!" Guid="!file_guid!"^>
echo !prefix!!tab!!tab!^<CreateFolder/^>
goto :eof

:nested_file
call :form_file_guid
echo !prefix!!tab!^<Component Id="!directory_id!_!count!" Win64="!is_x64!" Guid="!file_guid!" Permanent="yes" NeverOverwrite="yes"^>
set components=!components! !directory_id!_!count!
echo !prefix!!tab!!tab!^<File Id="!file_id!" Name="%%~nxf" Source="%%~ff" DiskId="1" Checksum="yes"/^>
set "file_set=true"
echo !prefix!!tab!^</Component^>
goto :eof
      
:find_slash
rem echo %line%
if "!line:~0,1!" == "\" (
  set /a level+=1
  )
set "line=!line:~1!"

if defined line (
  goto :find_slash
  )  else (
  goto :eof
  )

:form_file_guid
set count_string=000!count!
set count_string=!count_string:~-4!
set file_guid=!GUID:~0,19!!count_string!!GUID:~-13!
set /a count=count+=1
goto :eof

:correct_file_id
set file_id=!file_id:-=_!
set file_id=!file_id:+=_!
set file_id=!file_id: =_!
set file_id=!directory_id!_!file_id!
goto :eof
