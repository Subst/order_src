@echo off
rem сразу в UTF8 уйдем
chcp 65001

set root_path=%cd%
set wix_path=d:\programs\wix
set install_path=e:\qtdistr\order_install
set distrib_path=e:\qtdistr\order_update

rem del %distrib_path%\*.msi
rem для x64 и x86 вариантов два раза одно и тоже с разными исходниками и разными результатами. Потому цикл
Setlocal EnableDelayedExpansion
for %%v in (x64 x86) do (
  set variant=%%v
	echo Current toolchain for Win_!variant!
  set source_path=d:\qtprojects\order\qt_win_!variant!\release\bin
  set deploy_path=e:\qtdistr\order_install\source\!variant!
  if ERRORLEVEL 1 goto :eof
	
	copy /y !source_path!\order.exe !deploy_path!
	rem copy /y %source_path%\operators.xml %deploy_path%
  copy /y !source_path!\translations\order_ru.qm !deploy_path!\translations
  copy /y !source_path!\version !install_path!
  if ERRORLEVEL 1 goto :eof
	
	set /p order_version= <!install_path!\version
  echo order version - !order_version!_!variant!
  if ERRORLEVEL 1 goto :eof
	
	if not exist !install_path!\generated (
    md !install_path!\generated
		)
	if ERRORLEVEL 1 goto :eof
  rem Мне значительно удобней держать msi_xml.bat отдельно, в том числе и для отладки
  (call msi_xml.bat) > !install_path!\generated\order_install_!variant!.xml
	if ERRORLEVEL 1 goto :eof
  !wix_path!\candle.exe !install_path!\generated\order_install_!variant!.xml -ext !wix_path!\WixUtilExtension.dll -o !install_path!\generated\order_install_!variant!.wixobj
  if ERRORLEVEL 1 goto :eof
	!wix_path!\light.exe !install_path!\generated\order_install_!variant!.wixobj -ext !wix_path!\WixUtilExtension.dll -sw1076 -pdbout !install_path!\generated\order_install_!variant!.wixpdb -o !install_path!\order_!variant!_!order_version!.msi
  if ERRORLEVEL 1 goto :eof
		
  copy /y !install_path!\order_!variant!_!order_version!.msi !distrib_path!\order_!variant!_!order_version!.msi 
  copy /y !install_path!\version !distrib_path!\version
  rem Подчистить за собой
  rem rmdir /s /q %install_path%\generated\
  cd !root_path!
	)