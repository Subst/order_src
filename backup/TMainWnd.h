﻿#pragma once

#include "ui_TMainWnd.h"

#include <QSqlDatabase>
#include <LimeReport>
#include "TDataModule.h"
#include "TOrderModel.h"

class TSpecialityModel;
class TProfileModel;
class TAttestationModel;
class TPersonModel;

class TDataModule;
class QSignalMapper;

class QProgressDialog;
class QStandardItem;

class TOrderDelegate;

class TMainWnd : public QMainWindow, private Ui::TMainWnd
  {
    Q_OBJECT
  public:
    enum WalkPurpose
      {
      Invalid=-1,
      Correct=0,
      Report
      };
    Q_ENUM(WalkPurpose)

    TMainWnd(QWidget *parent=nullptr);

  private:
    TDataModule *m_dataModule;

    TCollegeModel *m_collegeModel;
    TSpecialityModel *m_specialityModel;
    TProfileModel *m_profileModel;
    TAttestationModel *m_attestationModel;
    TPersonModel *m_personModel;

    QSignalMapper *m_directoryMapper;

    LimeReport::ReportEngine *m_report;

    QProgressDialog *m_progressDlg;
    QAction *m_toOrderActn;
    QAction *m_fromOrderActn;
    QAction *m_showActn;
    QAction *m_editActn;

    QAction *m_emptyToOrderActn;

    QAction *m_expandAllActn;
    QAction *m_collapseAllActn;

    QAction *m_exportBranchActn;
    QAction *m_importBranchActn;

    TOrderModel *m_orderModel;
    TOrderDelegate *m_orderDelegate;
    LimeReport::ICallbackDatasource *m_orderDataSource;

    int m_count;
    int m_current;

    QString m_baseName;
    QString m_orderName;

    QVector <QVector <QVariant>> m_orderData;

  protected:
    void initCore();
    void initIface();

    void writeSettings();
    void readSettings();

    void closeEvent(QCloseEvent *event);
    void appendToOrder(TTableView*);
    void showInWindow();

    void setOrderData(int,QModelIndex,QVector <QVariant>);

    void walkTree(QModelIndex,int row=-1,WalkPurpose purpose=Invalid);//QString speciality=QString());
    //void walkTree(TOrderModel::ItemInfo *, QString speciality=QString(), int row=-1);

    void adjustIface();
    bool prepareReport();

    void moveToIndex(QModelIndex);

  public slots:
    void about();
    void aboutToQuit();

    void setCollegeInfo();

    void reportRenderStarted();
    void reportRenderFinished();
    void reportRenderPageFinished(int);

    void designReport();
    void previewReport();
    void printReport();
    void printReportToPdf();

    void directoryMapperMapped(QString);

    void createDatabase();
    void openDatabase();
    void saveDatabase();
    void removeDatabase();

    void newOrder();
    void openOrder();
    void saveOrder();
    void removeOrder();

    void specialitySelectionChanged(QItemSelection selected, QItemSelection deselected);

    void orderSelectionChanged(QItemSelection selected, QItemSelection deselected);
    //void tableSelectionChanged(QItemSelection selected, QItemSelection deselected);
    void getOrderCallbackData(LimeReport::CallbackInfo,QVariant &);
    void changeOrderPos(const LimeReport::CallbackInfo::ChangePosType,bool&);

    void tableContextMenuRequested(QPoint);
    void orderContextMenuRequested(QPoint);

    void tableDoubleClicked(QModelIndex);
    void treeDoubleClicked(QModelIndex);
    void appendToOrderActionTriggered();
    void appendEmptyToOrder();
    void showActionTriggered();
    void removeFromOrder();

    void editInWindow();
    void expandAll();
    void collapseAll();

    void exportBranch();
    void importBranch();

    //void correctOrder();
  };
