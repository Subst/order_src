﻿#include "TMainWnd.h"
#include "TSettings.h"
#include "TDirectoryDlg.h"
#include "TCollegeDlg.h"

#include "TCollegeModel.h"
#include "TSpecialityModel.h"
#include "TProfileModel.h"
#include "TAttestationModel.h"
#include "TPersonModel.h"

#include "TOrderModel.h"
#include "TOrderDelegate.h"

#include "TEditDlg.h"

#include <QMessageBox>
#include <QCloseEvent>
#include <QSignalMapper>
#include <QDir>
#include <QProgressDialog>
#include <QFileDialog>
#include <QInputDialog>
#include <QDebug>

#include <QCryptographicHash>
#include <QDate>

TMainWnd::TMainWnd(QWidget *parent) : QMainWindow(parent)
  {
  /*QVersionNumber remoteVersion=QVersionNumber::fromString(versionReply->readAll().simplified());
  QVersionNumber currentVersion=QVersionNumber::fromString(ORDER_VERSION);*/
  qDebug()<<QCryptographicHash::hash("Order",QCryptographicHash::Md5).toHex().toUpper();
  setupUi(this);
  qDebug()<<ORDER_VERSION;

  initCore();
  initIface();
  }
//
void TMainWnd::initCore()
  {
  m_dataModule=new TDataModule(this);

  TSettings settings;
  QString fileName=settings.getXmlValue("common/database","",qApp->applicationDirPath()+"/dbase/order.sqt").toString();
  QString dirName;

  if (QFile::exists(fileName)==false || m_dataModule->openBase(fileName)==false)
    {
    int answer=QMessageBox::critical(this,tr("Error"),tr("Can't open last working database.\nPress 'Open' to open existing database\nor 'Save' to create new one."),
                                     QMessageBox::Open | QMessageBox::Save);

    dirName=settings.getXmlValue("common/database_folder","",qApp->applicationDirPath()+"/dbase").toString();
    QDir dir(dirName);

    if (dir.exists()==false)
      dir.mkpath(dir.path());

    switch (answer)
      {
      case QMessageBox::Save:
        {
        fileName=QFileDialog::getSaveFileName(this,tr("Select new database"),dirName,tr("Database files (*.sqt)"));
        if (fileName.isEmpty())
          exit(0);

        if (QFile::exists(fileName))
          QFile::remove(fileName);

        if (QFile::copy(":database/order",fileName)==false)
          {
          QMessageBox::critical(this,tr("Error"),tr("Can't create database file.\n%1\nProgram now exit.").arg(fileName));
          exit(0);
          }
        QFile::setPermissions(fileName,QFile::permissions(fileName) | QFile::WriteOwner | QFile::WriteUser | QFile::WriteGroup | QFile::WriteOther);
        break;
        }
      case QMessageBox::Open:
        {
        fileName=QFileDialog::getOpenFileName(this,tr("Select existing database"),dirName,tr("Database files (*.sqt)"));
        if (fileName.isEmpty())
          exit(0);
        break;
        }
      default:
        {
        exit(0);
        break;
        }
      }
    }

  m_baseName=fileName;
  if (m_dataModule->database().isOpen()==false && m_dataModule->openBase(fileName)==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't open database file.\n%1\nProgram now exit.").arg(m_dataModule->baseName()));
    exit(0);
    }

  activateWindow();

  setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));

  m_collegeModel=qobject_cast <TCollegeModel*> (m_dataModule->tableModel("college"));

  m_specialityModel=qobject_cast <TSpecialityModel*> (m_dataModule->tableModel("specialities"));
  specialityTable->setModel(m_specialityModel);

  m_profileModel=qobject_cast <TProfileModel*> (m_dataModule->tableModel("profiles"));
  profileTable->setModel(m_profileModel);

  m_attestationModel=qobject_cast <TAttestationModel*> (m_dataModule->tableModel("attestations"));
  attestationTable->setModel(m_attestationModel);

  m_personModel=qobject_cast <TPersonModel*> (m_dataModule->tableModel("persons"));
  personTable->setModel(m_personModel);

  connect(specialityTable->selectionModel(),SIGNAL(selectionChanged(QItemSelection,QItemSelection)),this,SLOT(specialitySelectionChanged(QItemSelection,QItemSelection)));

  connect(specialityTable,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(tableContextMenuRequested(QPoint)));
  connect(profileTable,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(tableContextMenuRequested(QPoint)));
  connect(personTable,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(tableContextMenuRequested(QPoint)));
  connect(attestationTable,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(tableContextMenuRequested(QPoint)));

  connect(orderTree,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(orderContextMenuRequested(QPoint)));

  connect(specialityTable,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(tableDoubleClicked(QModelIndex)));
  connect(profileTable,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(tableDoubleClicked(QModelIndex)));
  connect(personTable,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(tableDoubleClicked(QModelIndex)));
  connect(attestationTable,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(tableDoubleClicked(QModelIndex)));

  connect(qApp,SIGNAL(aboutToQuit()),this,SLOT(aboutToQuit()));

  connect(createDatabaseActn,SIGNAL(triggered()),this,SLOT(createDatabase()));
  connect(openDatabaseActn,SIGNAL(triggered()),this,SLOT(openDatabase()));
  connect(saveDatabaseActn,SIGNAL(triggered()),this,SLOT(saveDatabase()));
  connect(removeDatabaseActn,SIGNAL(triggered()),this,SLOT(removeDatabase()));

  connect(newOrderActn,SIGNAL(triggered()),this,SLOT(newOrder()));
  connect(openOrderActn,SIGNAL(triggered()),this,SLOT(openOrder()));
  connect(saveOrderActn,SIGNAL(triggered()),this,SLOT(saveOrder()));
  connect(removeOrderActn,SIGNAL(triggered()),this,SLOT(removeOrder()));

  m_directoryMapper=new QSignalMapper(this);
  connect(m_directoryMapper,SIGNAL(mapped(QString)),this,SLOT(directoryMapperMapped(QString)));

  connect(profileActn,SIGNAL(triggered()),m_directoryMapper,SLOT(map()));
  m_directoryMapper->setMapping(profileActn,"profiles");

  connect(specialityActn,SIGNAL(triggered()),m_directoryMapper,SLOT(map()));
  m_directoryMapper->setMapping(specialityActn,"specialities");

  connect(attestationActn,SIGNAL(triggered()),m_directoryMapper,SLOT(map()));
  m_directoryMapper->setMapping(attestationActn,"attestations");

  connect(personActn,SIGNAL(triggered()),m_directoryMapper,SLOT(map()));
  m_directoryMapper->setMapping(personActn,"persons");

  connect(collegeActn,SIGNAL(triggered()),this,SLOT(setCollegeInfo()));
  connect(exitActn,SIGNAL(triggered()),this,SLOT(close()));
  connect(aboutQtActn,SIGNAL(triggered()),qApp,SLOT(aboutQt()));
  connect(aboutActn,SIGNAL(triggered()),this,SLOT(about()));

  //connect(correctOrderActn,SIGNAL(triggered()),this,SLOT(correctOrder()));
  // toolbars в меню
  connect(viewDatabaseActn,SIGNAL(triggered(bool)),databaseToolBar,SLOT(setVisible(bool)));
  connect(databaseToolBar,SIGNAL(visibilityChanged(bool)),viewDatabaseActn,SLOT(setChecked(bool)));

  connect(viewOrderActn,SIGNAL(triggered(bool)),orderToolBar,SLOT(setVisible(bool)));
  connect(orderToolBar,SIGNAL(visibilityChanged(bool)),viewOrderActn,SLOT(setChecked(bool)));

  connect(viewReportActn,SIGNAL(toggled(bool)),reportToolBar,SLOT(setVisible(bool)));
  connect(reportToolBar,SIGNAL(visibilityChanged(bool)),viewReportActn,SLOT(setChecked(bool)));

  connect(viewServiceActn,SIGNAL(triggered(bool)),serviceToolBar,SLOT(setVisible(bool)));
  connect(serviceToolBar,SIGNAL(visibilityChanged(bool)),viewServiceActn,SLOT(setChecked(bool)));

  // docks в меню
  connect(viewSpecialityActn,SIGNAL(triggered(bool)),specialityDock,SLOT(setVisible(bool)));
  connect(specialityDock,SIGNAL(visibilityChanged(bool)),viewSpecialityActn,SLOT(setChecked(bool)));

  connect(viewProfileActn,SIGNAL(triggered(bool)),profileDock,SLOT(setVisible(bool)));
  connect(profileDock,SIGNAL(visibilityChanged(bool)),viewProfileActn,SLOT(setChecked(bool)));

  connect(viewPersonActn,SIGNAL(triggered(bool)),personDock,SLOT(setVisible(bool)));
  connect(personDock,SIGNAL(visibilityChanged(bool)),viewPersonActn,SLOT(setChecked(bool)));

  connect(viewAttestationActn,SIGNAL(triggered(bool)),attestationDock,SLOT(setVisible(bool)));
  connect(attestationDock,SIGNAL(visibilityChanged(bool)),viewAttestationActn,SLOT(setChecked(bool)));


  m_toOrderActn=new QAction(QIcon(":images/next"),tr("Append to Order"),this);
  connect(m_toOrderActn,SIGNAL(triggered()),this,SLOT(appendToOrderActionTriggered()));

  m_fromOrderActn=new QAction(QIcon(":images/previous"),tr("Remove from Order"),this);
  connect(m_fromOrderActn,SIGNAL(triggered()),this,SLOT(removeFromOrder()));

  m_emptyToOrderActn=new QAction(QIcon(":images/next"),tr("Append empty record"),this);
  connect(m_emptyToOrderActn,SIGNAL(triggered()),this,SLOT(appendEmptyToOrder()));

  m_expandAllActn=new QAction(QIcon(":images/expand"),tr("Expand All"),this);
  connect(m_expandAllActn,SIGNAL(triggered()),this,SLOT(expandAll()));

  m_collapseAllActn=new QAction(QIcon(":images/collapse"),tr("Collapse All"),this);
  connect(m_collapseAllActn,SIGNAL(triggered()),this,SLOT(collapseAll()));

  m_showActn=new QAction(QIcon(":images/editraise"),tr("Show in window"),this);
  connect(m_showActn,SIGNAL(triggered()),this,SLOT(showActionTriggered()));

  m_editActn=new QAction(QIcon(":images/editraise"),tr("Edit in window"),this);
  connect(m_editActn,SIGNAL(triggered()),this,SLOT(editInWindow()));

  m_exportBranchActn=new QAction(QIcon(":images/export"),tr("Export Branch"),this);
  connect(m_exportBranchActn,SIGNAL(triggered()),this,SLOT(exportBranch()));

  m_importBranchActn=new QAction(QIcon(":images/import"),tr("Import Branch"),this);
  connect(m_importBranchActn,SIGNAL(triggered()),this,SLOT(importBranch()));

  connect(designReportActn,SIGNAL(triggered()),this,SLOT(designReport()));
  connect(previewReportActn,SIGNAL(triggered()),this,SLOT(previewReport()));
  connect(printReportActn,SIGNAL(triggered()),this,SLOT(printReport()));
  connect(printReportToPdfActn,SIGNAL(triggered()),this,SLOT(printReportToPdf()));

  m_orderModel=new TOrderModel(this);
  orderTree->setModel(m_orderModel);
  m_orderDelegate=new TOrderDelegate(this);
  orderTree->setItemDelegate(m_orderDelegate);

  connect(orderTree->selectionModel(),SIGNAL(selectionChanged(QItemSelection,QItemSelection)),this,SLOT(orderSelectionChanged(QItemSelection,QItemSelection)));

  QModelIndex rootIndex=m_orderModel->index(0,TOrderModel::Name);
  orderTree->setRootIndex(rootIndex);

  fileName=TSettings().getXmlValue("common/order","",qApp->applicationDirPath()+"/orders/order.xml").toString();
  if (fileName.isEmpty()==false && QFile::exists(fileName))
    {
    m_orderModel->presetAnswer(TOrderModel::NoToAllAnswer);
    QModelIndex actualIndex=m_orderModel->readXml(fileName,m_orderModel->rootIndex());
    if (actualIndex.isValid())
      {
      m_orderName=fileName;
      setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));
      orderTree->expandAll();
      orderTree->setCurrentIndex(actualIndex);
      if (m_orderModel->isChanged())
        QMessageBox::information(this,tr("Information"),tr("The Order has been automatically corrected\nto coply with new order format."),QMessageBox::Ok);
      }
    }

  m_report=new LimeReport::ReportEngine(this);
  m_report->setShowProgressDialog(true);
  m_report->setPreviewWindowIcon(QIcon(QPixmap(":/images/order")));

  m_report->dataManager()->addModel("college",m_collegeModel,true);
  m_orderDataSource=m_report->dataManager()->createCallbackDatasource("order");
  connect(m_orderDataSource,SIGNAL(getCallbackData(LimeReport::CallbackInfo,QVariant&)),this,SLOT(getOrderCallbackData(LimeReport::CallbackInfo,QVariant&)));
  connect(m_orderDataSource,SIGNAL(changePos(LimeReport::CallbackInfo::ChangePosType,bool&)),this,SLOT(changeOrderPos(LimeReport::CallbackInfo::ChangePosType,bool&)));

  connect(m_report,SIGNAL(renderStarted()),this,SLOT(reportRenderStarted()));
  connect(m_report,SIGNAL(renderFinished()),this,SLOT(reportRenderFinished()));
  connect(m_report,SIGNAL(renderPageFinished(int)),this,SLOT(reportRenderPageFinished(int)));
  }
//
void TMainWnd::initIface()
  {
  readSettings();

  specialityTable->setColumnHidden(m_specialityModel->fieldIndex("id"),true);
  profileTable->setColumnHidden(m_profileModel->fieldIndex("id"),true);
  profileTable->setColumnHidden(m_profileModel->fieldIndex("speciality_id"),true);
  attestationTable->setColumnHidden(m_attestationModel->fieldIndex("id"),true);
  personTable->setColumnHidden(m_personModel->fieldIndex("id"),true);

  specialityTable->setFocus();
  if (m_specialityModel->rowCount())
    specialityTable->selectRow(0);

  orderTree->header()->setSectionHidden(TOrderModel::Name,true);
  orderTree->header()->setSectionHidden(TOrderModel::Size,true);
  orderTree->header()->setSectionHidden(TOrderModel::ReadOnly,true);
  orderTree->expandAll();

  adjustIface();
  }
//
void TMainWnd::adjustIface()
  {
  QModelIndex itemIndex=orderTree->currentIndex();
  QString name=m_orderModel->index(itemIndex.row(),TOrderModel::Name,itemIndex.parent()).data().toString();

  saveOrderActn->setEnabled(m_orderModel->root()->children.size()>0);
  //correctOrderActn->setEnabled(m_orderModel->root()->children.size()>0);

  if (m_orderModel->root()->children.size()>0 && (name=="speciality" || name=="order"))
    {
    previewReportActn->setEnabled(true);
    printReportActn->setEnabled(true);
    printReportToPdfActn->setEnabled(true);
    }
  else
    {
    previewReportActn->setEnabled(false);
    printReportActn->setEnabled(false);
    printReportToPdfActn->setEnabled(false);
    }
  }
//
void TMainWnd::readSettings()
  {
  TSettings settings;
  restoreState(settings.getXmlValue("main_window/state","",0).toByteArray());
  restoreGeometry(settings.getXmlValue("main_window/geometry","",0).toByteArray());

  specialityTable->horizontalHeader()->restoreState(settings.getXmlValue("main_window/speciality_table","",0).toByteArray());
  profileTable->horizontalHeader()->restoreState(settings.getXmlValue("main_window/profile_table","",0).toByteArray());
  attestationTable->horizontalHeader()->restoreState(settings.getXmlValue("main_window/attestation_table","",0).toByteArray());
  personTable->horizontalHeader()->restoreState(settings.getXmlValue("main_window/person_table","",0).toByteArray());

  orderTree->header()->restoreState(settings.getXmlValue("main_window/order_tree","",0).toByteArray());
  }
//
void TMainWnd::writeSettings()
  {
  TSettings settings;
  settings.setXmlValue("common/database","",m_baseName);
  settings.setXmlValue("common/order","",m_orderName);

  settings.setXmlValue("main_window/state","",saveState());
  settings.setXmlValue("main_window/geometry","",saveGeometry());

  settings.setXmlValue("main_window/speciality_table","",specialityTable->horizontalHeader()->saveState());
  settings.setXmlValue("main_window/profile_table","",profileTable->horizontalHeader()->saveState());
  settings.setXmlValue("main_window/attestation_table","",attestationTable->horizontalHeader()->saveState());
  settings.setXmlValue("main_window/person_table","",personTable->horizontalHeader()->saveState());

  settings.setXmlValue("main_window/order_tree","",orderTree->header()->saveState());
  }
//
void TMainWnd::aboutToQuit()
  {
  writeSettings();
  m_dataModule->closeBase();
  delete m_dataModule;
  foreach (QString connection,QSqlDatabase::connectionNames())
    QSqlDatabase::removeDatabase(connection);
  }
//
void TMainWnd::about()
  {
  QMessageBox::information(this,tr("About program"),tr("Order. Bla-bla about...\nCopyrigh (c) Denis P.Classen, since 2017.\nVersion: %1").arg(ORDER_VERSION),QMessageBox::Ok);
  }
//
void TMainWnd::closeEvent(QCloseEvent *event)
  {
  if (QMessageBox::question(this,tr("Confirmation"),tr("Exit program?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    {
    if (m_orderModel->isChanged() &&
        QMessageBox::question(this,tr("Confirmation"),tr("Current Order has been changed.\nSave it before exit?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
      saveOrder();
    event->accept();
    }
  else
    event->ignore();
  }
//
void TMainWnd::setCollegeInfo()
  {
  TCollegeDlg *collegeDlg=new TCollegeDlg(this);
  collegeDlg->setModal(true);
  collegeDlg->exec();
  delete collegeDlg;
  }
//
void TMainWnd::directoryMapperMapped(QString table)
  {
  // при работе со словарем фильтр убираем
  QSqlTableModel *tableModel=m_dataModule->tableModel(table);
  QString filter=tableModel->filter();
  tableModel->setFilter(QString());

  TDirectoryDlg *directoryDlg=new TDirectoryDlg(this,table);
  directoryDlg->setModal(true);
  directoryDlg->exec();
  delete directoryDlg;
  // после работы со словарем фильтр восстанавливаем
  tableModel->setFilter(filter);
  }
//
void TMainWnd::createDatabase()
  {
  TSettings settings;
  QString dir=settings.getXmlValue("common/database_folder","",qApp->applicationDirPath()+"/dbase").toString();
  QString fileName=QFileDialog::getSaveFileName(this,tr("Create Database"),dir,tr("Database files (*.sqt)"));
  if (fileName.isEmpty())
    return;

  if (QFile::exists(fileName))
    QFile::remove(fileName);

  if (QFile::copy(":database/order",fileName)==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't create database file.\n%1.").arg(fileName));
    return;
    }

  writeSettings();
  QFile::setPermissions(fileName,QFile::permissions(fileName) | QFile::WriteOwner | QFile::WriteUser | QFile::WriteGroup | QFile::WriteOther);
  m_dataModule->closeBase();

  if (m_dataModule->openBase(fileName)==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't open database file.\n%1.").arg(m_dataModule->baseName()));
    return;
    }

  initIface();
  settings.setXmlValue("common/database_folder","",QFileInfo(fileName).absolutePath());
  m_baseName=fileName;
  setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));
  }
//
void TMainWnd::openDatabase()
  {
  TSettings settings;
  QString dir=settings.getXmlValue("common/database_folder","",qApp->applicationDirPath()+"/dbase").toString();
  QString fileName=QFileDialog::getOpenFileName(this,tr("Open Database"),dir,tr("Database files (*.sqt)"));
  if (fileName.isEmpty())
    return;

  writeSettings();
  m_dataModule->closeBase();
  if (m_dataModule->openBase(fileName)==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't open database file.\n%1.").arg(m_dataModule->baseName()));
    return;
    }

  initIface();
  settings.setXmlValue("common/database_folder","",QFileInfo(fileName).absolutePath());
  m_baseName=fileName;
  setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));
  }
//
void TMainWnd::saveDatabase()
  {
  TSettings settings;
  QString dir=settings.getXmlValue("common/database_folder","",qApp->applicationDirPath()+"/dbase").toString();

  QString fileName=QFileDialog::getSaveFileName(this,tr("Create Database"),dir,tr("Database files (*.sqt)"));
  if (fileName.isEmpty())
    return;

  if (QFile::exists(fileName))
    QFile::remove(fileName);

  if (QFile::copy(m_dataModule->baseName(),fileName)==false)
    QMessageBox::critical(this,tr("Error"),tr("Can't save database file.\n%1.").arg(m_dataModule->baseName()));

  settings.setXmlValue("common/database_folder","",QFileInfo(fileName).absolutePath());
  }
//
void TMainWnd::removeDatabase()
  {
  TSettings settings;
  QString dir=settings.getXmlValue("common/database_folder","",qApp->applicationDirPath()+"/dbase").toString();

  QString fileName=QFileDialog::getOpenFileName(this,tr("Remove Database"),dir,tr("Database files (*.sqt)"));
  if (fileName.isEmpty())
    return;

  if (QMessageBox::question(this,tr("Confirmation"),tr("Remove Database?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::No)
    return;

  if (fileName==m_baseName)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't remove current database file.\n%1.").arg(fileName));
    return;
    }

  settings.setXmlValue("common/database_folder","",QFileInfo(fileName).absolutePath());
  if (QFile::remove(fileName)==false)
    QMessageBox::critical(this,tr("Error"),tr("Can't remove database file.\n%1.").arg(fileName));
  }
//
void TMainWnd::newOrder()
  {
  if (m_orderModel->isChanged() &&
      QMessageBox::question(this,tr("Confirmation"),tr("Current Order has been changed.\nSave it before open new one?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    saveOrder();

  if (m_orderModel->readXml(":template/order",m_orderModel->rootIndex()).isValid())
    orderTree->expandAll();

  m_orderName=tr("untitled");
  setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));
  adjustIface();
  }
//
void TMainWnd::openOrder()
  {
  TSettings settings;
  if (m_orderModel->isChanged() && QMessageBox::question(this,tr("Confirmation"),tr("Current Order has been changed.\nSave it before open new one?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    saveOrder();

  QString dirName=settings.getXmlValue("common/order_folder","","").toString();
  if (dirName.isEmpty())
    dirName=qApp->applicationDirPath()+"/orders";

  QDir dir(dirName);
  if (dir.exists()==false)
    dir.mkpath(dir.path());

  QString fileName=QFileDialog::getOpenFileName(this,tr("Order file"),dirName,tr("XML files (*.xml)"));
  if (fileName.isEmpty())
    return;

  //qDebug()<<fileName;
  settings.setXmlValue("common/order_folder","",QFileInfo(fileName).absolutePath());

  m_orderModel->presetAnswer(TOrderModel::NoToAllAnswer);
  if (m_orderModel->readXml(fileName,m_orderModel->rootIndex()).isValid())
    {
    m_orderName=fileName;
    setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));
    orderTree->expandAll();
    }

  adjustIface();
  if (m_orderModel->isChanged())
    QMessageBox::information(this,tr("Information"),tr("The Order has been automatically corrected\nto coply with new order format."),QMessageBox::Ok);
  }
//
void TMainWnd::saveOrder()
  {
  TSettings settings;
  QString dirName=settings.getXmlValue("common/order_folder","","").toString();
  if (dirName.isEmpty())
    dirName=qApp->applicationDirPath()+"/orders";

  QDir dir(dirName);
  if (dir.exists()==false)
    dir.mkpath(dir.path());

  QString fileName=QFileDialog::getSaveFileName(this,tr("Order file"),dirName,tr("XML files (*.xml)"));
  if (fileName.isEmpty())
    return;

  if (m_orderModel->writeXml(fileName,m_orderModel->rootIndex()))
    {
    m_orderName=fileName;
    setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));
    }

  settings.setXmlValue("common/order_folder","",QFileInfo(fileName).absolutePath());
  }
//
void TMainWnd::removeOrder()
  {
  TSettings settings;
  QString dir=settings.getXmlValue("common/order_folder","",qApp->applicationDirPath()+"/orders").toString();

  QString fileName=QFileDialog::getOpenFileName(this,tr("Remove Order file"),dir,tr("XML files (*.xml)"));
  if (fileName.isEmpty())
    return;

  if (QMessageBox::question(this,tr("Confirmation"),tr("Remove Order?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::No)
    return;

  if (fileName==m_orderName)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't remove current order file.\n%1.").arg(fileName));
    return;
    }

  settings.setXmlValue("common/order_folder","",QFileInfo(fileName).absolutePath());
  if (QFile::remove(fileName)==false)
    QMessageBox::critical(this,tr("Error"),tr("Can't remove order file.\n%1.").arg(fileName));
  }
//
void TMainWnd::reportRenderStarted()
  {
  m_progressDlg=new QProgressDialog(this);
  connect(m_progressDlg,SIGNAL(canceled()),m_report,SLOT(cancelRender()));

  m_progressDlg->resize(300,m_progressDlg->height());
  m_progressDlg->setWindowTitle(tr("Report preparing"));
  m_progressDlg->setModal(true);
  m_progressDlg->setMaximum(m_count);

  m_progressDlg->show();
  qApp->processEvents();
  }
//
void TMainWnd::reportRenderFinished()
  {
  m_progressDlg->close();
  delete m_progressDlg;
  }
//
void TMainWnd::reportRenderPageFinished(int page)
  {
  m_progressDlg->setValue(m_current);
  qApp->processEvents();
  }
//
void TMainWnd::designReport()
  {
  QString report=qApp->applicationDirPath()+"/report/order.lrxml";
  QString name=tr("Order");

  if (report.isEmpty()==false)
    {
    m_report->loadFromFile(report);
    m_report->setReportName(name+" ["+report+"]");
    }
  else
    {
    m_report->loadFromByteArray(nullptr);
    m_report->setReportName(QString());
    }

  m_report->designReport();
  }
//
bool TMainWnd::prepareReport()
  {
  m_current=0;
  m_count=0;
  m_orderData.clear();

  QModelIndex index=orderTree->currentIndex();
  QString name=m_orderModel->index(index.row(),TOrderModel::Name,index.parent()).data().toString();
  //qDebug()<<name;
  if (name=="order")
    {
    m_report->dataManager()->setReportVariable("fullOrder",true);
    walkTree(m_orderModel->rootIndex(),-1,Report);
    }
  else
    {
    m_report->dataManager()->setReportVariable("fullOrder",false);
    walkTree(m_orderModel->rootIndex(),index.row(),Report);
    }
  return true;
  }
//
void TMainWnd::previewReport()
  {
  // Тут видимо проверка, что в приказе что-то накидано, количество направлений (специальностей) больше нуля
  QModelIndex root=m_orderModel->index(0,TOrderModel::Title,QModelIndex());
  if (m_orderModel->rowCount(root)<1 || prepareReport()==false)
    return;

  QString report=qApp->applicationDirPath()+"/report/order.lrxml";
  QString name=tr("Order");

  if (m_report->loadFromFile(report)==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't open report template file\n%1.").arg(report),QMessageBox::Ok);
    return;
    }

  m_report->setReportName(name+" ["+report+"]");
  m_report->setPreviewWindowTitle(tr("Preview - %1 [%2]").arg(name).arg(report));
  m_report->previewReport();
  }
//
void TMainWnd::printReport()
  {
  // Тут видимо проверка, что в приказе что-то накидано, количество направлений (специальностей) больше нуля
  QModelIndex root=m_orderModel->index(0,TOrderModel::Title,QModelIndex());
  if (m_orderModel->rowCount(root)<1 || prepareReport()==false)
    return;

  QString report=qApp->applicationDirPath()+"/report/order.lrxml";
  QString name=tr("Order");

  if (m_report->loadFromFile(report)==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't open report template file\n%1.").arg(report),QMessageBox::Ok);
    return;
    }

  m_report->setReportName(name+" ["+report+"]");
  m_report->printReport();
  }
//
void TMainWnd::printReportToPdf()
  {
  // Тут видимо проверка, что в приказе что-то накидано, количество направлений (специальностей) больше нуля
  QModelIndex root=m_orderModel->index(0,TOrderModel::Title,QModelIndex());
  if (m_orderModel->rowCount(root)<1 || prepareReport()==false)
    return;

  QString report=qApp->applicationDirPath()+"/report/order.lrxml";
  QString name=tr("Order");

  if (m_report->loadFromFile(report)==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't open report template file\n%1.").arg(report),QMessageBox::Ok);
    return;
    }

  m_report->setReportName(name+" ["+report+"]");

  QString pdfName=QFileDialog::getSaveFileName(this,tr("Save to PDF"),qApp->applicationDirPath(),tr("PDF files (*.pdf)"));
  if (pdfName.isEmpty())
    return;

  m_report->printToPDF(pdfName);
  }
//
void TMainWnd::specialitySelectionChanged(QItemSelection selected, QItemSelection deselected)
  {
  Q_UNUSED(selected)
  Q_UNUSED(deselected)

  QItemSelectionModel *selectionModel=qobject_cast <QItemSelectionModel*>(sender());
  int id=-1;
  QModelIndexList indexes=selectionModel->selectedRows();
  if (indexes.size())
    {
    id=m_specialityModel->index(indexes.at(0).row(),m_specialityModel->fieldIndex("id")).data().toInt();
    m_profileModel->setFilter("speciality_id="+QString::number(id));
    }
  else
    m_profileModel->setFilter("");
  }
//
void TMainWnd::tableContextMenuRequested(QPoint point)
  {
  TTableView *tableView=qobject_cast <TTableView*>(sender());
  TSqlTableModel *model=qobject_cast <TSqlTableModel*> (tableView->model());
  QString table=model->tableName();
  // property
  // m_toOrderActn->setData(table);
  m_toOrderActn->setData((quint64)tableView);

  QMenu menu;
  menu.addAction(m_toOrderActn);
  menu.addSeparator();
  menu.addAction(m_showActn);

  QModelIndex sourceIndex=tableView->indexAt(point);
  QModelIndex targetIndex=orderTree->currentIndex();

  QString source=model->nodeType();
  TOrderModel::NodeToNode nodeToNode=m_orderModel->getNodeToNode(targetIndex,source);

  if (sourceIndex.isValid())
    {
    m_toOrderActn->setEnabled(nodeToNode.type!=TOrderModel::InvalidIndex && nodeToNode.count<nodeToNode.size);
    m_showActn->setEnabled(true);
    m_showActn->setData(sourceIndex.data().toString());
    }
  else
    {
    m_toOrderActn->setEnabled(false);
    m_showActn->setEnabled(false);
    }

  menu.exec(QCursor::pos());
  }
//
void TMainWnd::orderContextMenuRequested(QPoint point)
  {
  QMenu menu;
  menu.addAction(m_emptyToOrderActn);
  menu.addAction(m_fromOrderActn);
  menu.addSeparator();
  menu.addAction(m_editActn);
  menu.addSeparator();
  menu.addAction(m_exportBranchActn);
  menu.addAction(m_importBranchActn);
  menu.addSeparator();
  menu.addAction(m_expandAllActn);
  menu.addAction(m_collapseAllActn);

  QModelIndex itemIndex=orderTree->indexAt(point);

  TOrderModel::NodeToNode nodeToNode=m_orderModel->getNodeToNode(itemIndex);
  m_emptyToOrderActn->setEnabled(orderTree->indexAt(point).isValid() && nodeToNode.type!=TOrderModel::InvalidIndex && nodeToNode.count<nodeToNode.size);
  m_fromOrderActn->setEnabled(itemIndex.isValid() && m_orderModel->data(m_orderModel->index(itemIndex.row(),TOrderModel::ReadOnly,itemIndex.parent())).toInt()==0);

  m_editActn->setEnabled(itemIndex.isValid());
  m_exportBranchActn->setEnabled(itemIndex.isValid());

  menu.exec(QCursor::pos());
  }
//
void TMainWnd::getOrderCallbackData(LimeReport::CallbackInfo info, QVariant &value)
  {
  switch (info.dataType)
    {
    case LimeReport::CallbackInfo::IsEmpty:
      {
      value=false;
      break;
      }
    case LimeReport::CallbackInfo::HasNext:
      {
      value=(m_current<m_count);
      break;
      }
    case LimeReport::CallbackInfo::RowCount:
      {
      value=m_count;
      break;
      }
    case LimeReport::CallbackInfo::ColumnCount:
      {
      value=m_orderModel->columnCount();
      break;
      }
    case LimeReport::CallbackInfo::ColumnHeaderData:
      {
      value=m_orderModel->headerData(info.index,Qt::Horizontal,Qt::UserRole);
      break;
      }
    case LimeReport::CallbackInfo::ColumnData:
      {
      value=m_orderData[m_current].at(m_orderModel->columnPosition(info.columnName)).toString();
      break;
      }
    }
  }
//
void TMainWnd::changeOrderPos(const LimeReport::CallbackInfo::ChangePosType type, bool &result)
  {
  switch (type)
    {
    case LimeReport::CallbackInfo::First:
      {
      result=(m_current==0);
      break;
      }

    case LimeReport::CallbackInfo::Next:
      {
      result=(m_current<m_count);
      m_current++;
      break;
      }
    }
  }
//
void TMainWnd::walkTree(QModelIndex index, int row, WalkPurpose purpose)
  {
  switch (purpose)
    {
    /*case Correct:
   {
   QString name=m_orderModel->index(index.row(),TOrderModel::Name,index.parent()).data().toString();
   // сформировать data и вызвать setOrderData ?
   // через m_nodeSize и m_nodeReadOnly работаю
   m_orderModel->setData(m_orderModel->index(index.row(),TOrderModel::Size,index.parent()),m_orderModel->m_nodeSize[name]);
   m_orderModel->setData(m_orderModel->index(index.row(),TOrderModel::ReadOnly,index.parent()),m_orderModel->m_nodeReadOnly[name]);
   break;
   }*/
    case Report:
      {
      if (m_orderModel->index(index.row(),TOrderModel::Name,index.parent()).data().toString()!=QString("speciality") || row==-1 || row==index.row())
        {
        QVector <QVariant> nodeData;
        nodeData<<m_orderModel->index(index.row(),TOrderModel::Title,index.parent()).data()
               <<m_orderModel->index(index.row(),TOrderModel::Description,index.parent()).data()
              <<m_orderModel->index(index.row(),TOrderModel::Name,index.parent()).data()
             <<m_orderModel->index(index.row(),TOrderModel::Size,index.parent()).data()
            <<m_orderModel->index(index.row(),TOrderModel::ReadOnly,index.parent()).data();
        m_orderData<<nodeData;
        m_count++;
        }
      break;
      }
    default:
      break;
    }

  for (int i=0;i<m_orderModel->rowCount(index);i++)
    {
    switch (purpose)
      {
      case Report:
        {
        if (m_orderModel->index(i,TOrderModel::Name,index).data()=="speciality" && row!=-1 && row!=i)
          continue;
        break;
        }
      default:
        break;
      }
    walkTree(m_orderModel->index(i,TOrderModel::Name,index),row,purpose);
    }
  }
//
/*void TMainWnd::walkTree(TOrderModel::ItemInfo *info, QString speciality, int row) // может, вместо itemINfo использовать modelIndex
 {
 QModelIndex index;
 // index->rowCount()
 m_count+=info->children.size();
 m_orderData<<info->data;
 //m_orderData<<m_order
 for (int i=0;i<info->children.size();i++)
  {
  if (speciality.isEmpty()==false && info->children.at(i)->data.at(TOrderModel::Name)=="speciality" && i!=row)
   {
   m_count--;
   continue;
   }//qDebug()<<info->data;
  //qDebug()<<m_orderModel->index(i,TOrderModel::Name,m_orderModel->rootIndex()).data()<<m_orderModel->index(i,TOrderModel::Title,m_orderModel->rootIndex()).data();
  walkTree(info->children.at(i),speciality,row);
  }
 }*/
//
void TMainWnd::tableDoubleClicked(QModelIndex index)
  {
  TTableView *tableView=qobject_cast <TTableView*>(sender());
  appendToOrder(tableView);
  //QSqlTableModel *model=qobject_cast <QSqlTableModel*> (tableView->model());
  //QString table=model->tableName();

  //appendToOrder(table);
  }
//
void TMainWnd::treeDoubleClicked(QModelIndex index)
  {
  if (index.isValid()==false || index.row()!=TOrderModel::Title)
    return;

  removeFromOrder();
  }
//
void TMainWnd::appendToOrderActionTriggered()
  {
  QAction *action=qobject_cast <QAction*>(sender());

  TTableView *tableView=(TTableView*)(action->data().toULongLong());
  appendToOrder(tableView);
  }
//
void TMainWnd::appendEmptyToOrder()
  {
  QModelIndex targetIndex=orderTree->currentIndex();
  QString name=m_orderModel->index(targetIndex.row(),TOrderModel::Name,targetIndex.parent()).data().toString();
  int size;

  bool found=false;
  foreach (QString key,m_orderModel->m_allowType[name].keys())
    {// строго ищем что тип разрешенной к вставке пустой записи соответсвует типу какой-то непустой и эта непустая и будет нужная
    if (key.isEmpty()==false && m_orderModel->m_allowType[name][key]==m_orderModel->m_allowType[name][""])
      {
      found=true;
      name=key;
      break;
      }
    }

  if (found==false)
    return;

  size=m_orderModel->m_nodeSize[name];

  TOrderModel::NodeToNode nodeToNode=m_orderModel->getNodeToNode(targetIndex);
  QModelIndex parent=(nodeToNode.type==TOrderModel::ChildIndex) ? targetIndex : targetIndex.parent();
  int count=m_orderModel->rowCount(parent);
  int position=(nodeToNode.type==TOrderModel::ChildIndex) ? count : targetIndex.row();

  m_orderModel->insertRow(position,parent);

  QVector <QVariant> data;
  data<<tr("New record").append(" %1").arg(count+1)<<QString()<<name<<size<<0;
  setOrderData(position,parent,data);
  }
//
void TMainWnd::appendToOrder(TTableView *tableView)
  {
  if (tableView->model()==nullptr)
    return;

  TSqlTableModel *model=qobject_cast<TSqlTableModel*>(tableView->model());

  QModelIndex targetIndex=orderTree->currentIndex();
  QString name=model->nodeType();
  TOrderModel::NodeToNode nodeToNode=m_orderModel->getNodeToNode(targetIndex,name);

  if (targetIndex.isValid()==false || nodeToNode.count>=nodeToNode.size)
    return;

  switch (nodeToNode.type)
    {
    case TOrderModel::ChildIndex:
      {
      break;
      }
    case TOrderModel::PeerIndex:
      {
      targetIndex=targetIndex.parent();
      break;
      }
    default:
      {
      QMessageBox::critical(this,tr("Error"),tr("You can't append this record here."),QMessageBox::Ok);
      return;
      }
    }
  //QString target=m_orderModel->index(targetIndex.row(),TOrderModel::Name,targetIndex.parent()).data().toString();
  // тут разобрался вроде, теперь дальше рефакторинг кода в этой функции сделать
  int position=(nodeToNode.type==TOrderModel::PeerIndex) ? orderTree->currentIndex().row() : nodeToNode.count;
  int maximum=nodeToNode.size;

  QModelIndexList indexes;
  QString title;
  QString description;
  int size=-1;

  QList <int> titleFields;
  QList <int> descriptionFields;

  // вот тут в первую очередь ориентироваться на данные из m_dataModule
  //name=m_dataModule->nodeType(table);
  indexes=tableView->selectionModel()->selectedRows();
  size=m_orderModel->m_nodeSize[name];
  titleFields<<model->fieldIndex(model->headerField());
  descriptionFields<<model->fieldIndex(model->descriptionField());
  //descriptionFields<<model->fieldIndex("description");

  while (indexes.size()>maximum)
    indexes.removeLast();
  if (indexes.size()==0)
    return;

  m_orderModel->presetAnswer(TOrderModel::NoAnswer);
  for (int i=0;i<indexes.size();++i)//i++)
    {
    title=QString();
    description=QString();
    for (int j=0;j<titleFields.size();j++)
      title.append(model->index(indexes.at(i).row(),titleFields.at(j)).data().toString()).append(" ");
    while (title.endsWith(" "))
      title.chop(1);

    for (int j=0;j<descriptionFields.size();j++)
      if (model->index(indexes.at(i).row(),descriptionFields.at(j)).data().toString().isEmpty()==false)
        description.append(model->index(indexes.at(i).row(),descriptionFields.at(j)).data().toString()).append(", ");

    while (description.endsWith(", "))
      description.resize(description.size()-2);

    QVector <QVariant> data;
    data<<title<<description<<name<<size<<0;

    QPair <bool,int> result=m_orderModel->findMatch(targetIndex,data,position+i);
    if (result.first)
      {
      indexes.removeAt(i);
      i--;
      setOrderData(result.second,targetIndex,data);
      }
    else
      {
      m_orderModel->insertRow(position+i,targetIndex);
      setOrderData(position+i,targetIndex,data);
      }
    }
  }
//
void TMainWnd::setOrderData(int position,QModelIndex parent,QVector <QVariant> data)
  {
  m_orderModel->setData(m_orderModel->index(position,TOrderModel::Title,parent),data.at(TOrderModel::Title).toString());
  m_orderModel->setData(m_orderModel->index(position,TOrderModel::Description,parent),data.at(TOrderModel::Description).toString());
  m_orderModel->setData(m_orderModel->index(position,TOrderModel::Name,parent),data.at(TOrderModel::Name).toString());
  m_orderModel->setData(m_orderModel->index(position,TOrderModel::Size,parent),data.at(TOrderModel::Size).toInt());
  m_orderModel->setData(m_orderModel->index(position,TOrderModel::ReadOnly,parent),data.at(TOrderModel::ReadOnly).toInt());

  if (data.at(TOrderModel::Name).toString()=="speciality")// && child.isValid())
    m_orderModel->readXml(":template/speciality",m_orderModel->index(position,TOrderModel::Name,parent),data.at(TOrderModel::Name).toString());
  //m_orderModel->readXml(":template/speciality",parent.child(position,TOrderModel::Name),data.at(TOrderModel::Name).toString());

  if (data.at(TOrderModel::Name).toString()=="attestation_item")// && child.isValid())
    m_orderModel->readXml(":template/attestation",m_orderModel->index(position,TOrderModel::Name,parent),data.at(TOrderModel::Name).toString());
  //m_orderModel->readXml(":template/attestation",parent.child(position,TOrderModel::Name),data.at(TOrderModel::Name).toString());

  moveToIndex(m_orderModel->index(position,TOrderModel::Title,parent));
  }
//
void TMainWnd::removeFromOrder()
  {
  QModelIndex itemIndex=orderTree->selectionModel()->currentIndex();
  if (itemIndex.isValid()==false || m_orderModel->index(itemIndex.row(),TOrderModel::ReadOnly,itemIndex.parent()).data().toBool()==1)
    return;

  QString arg=m_orderModel->rowCount(itemIndex)>0 ? tr(" (And all it's children)") : "";
  if (QMessageBox::question(this,tr("Confirmation"),tr("Remove the current Item%1 from Order?").arg(arg),QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    m_orderModel->removeRow(itemIndex.row(),itemIndex.parent());

  adjustIface();
  }
//
void TMainWnd::showActionTriggered()
  {
  showInWindow();
  }
//
void TMainWnd::showInWindow()
  {
  QString text=m_showActn->data().toString();
  TEditDlg *editDlg=new TEditDlg(this,text,true);
  editDlg->setWindowTitle(tr("Show text"));
  editDlg->exec();

  delete editDlg;
  }
//
void TMainWnd::editInWindow()
  {
  QModelIndex itemIndex=orderTree->selectionModel()->currentIndex();
  if (itemIndex.isValid()==false)
    return;

  TEditDlg *editDlg=new TEditDlg(this,m_orderModel->data(itemIndex).toString(),false);
  editDlg->setWindowTitle(tr("Edit text"));
  if (editDlg->exec()==QDialog::Accepted)
    m_orderModel->setData(itemIndex,editDlg->text());

  delete editDlg;
  }
//
void TMainWnd::expandAll()
  {
  QModelIndex index=orderTree->selectionModel()->currentIndex();
  orderTree->expandAll();
  if (index.isValid())
    orderTree->scrollTo(index,QAbstractItemView::PositionAtCenter);
  }
//
void TMainWnd::collapseAll()
  {
  orderTree->collapseAll();
  if (m_orderModel->rowCount()>0)
    {
    for (quint8 i=0;i<m_orderModel->columnCount();i++)
      orderTree->selectionModel()->select(m_orderModel->index(0,i,m_orderModel->index(0,TOrderModel::Title)),QItemSelectionModel::Select);
    orderTree->scrollTo(m_orderModel->index(0,TOrderModel::Title),QAbstractItemView::PositionAtCenter);
    }
  }
//
void TMainWnd::exportBranch()
  {
  TSettings settings;
  QModelIndex itemIndex=orderTree->selectionModel()->currentIndex();
  if (itemIndex.isValid()==false)
    return;

  QString dirName=settings.getXmlValue("common/order_folder","","").toString();
  if (dirName.isEmpty())
    dirName=qApp->applicationDirPath()+"/orders";

  QDir dir(dirName);
  if (dir.exists()==false)
    dir.mkpath(dir.path());

  QString fileName=QFileDialog::getSaveFileName(this,tr("Order file"),dirName,tr("XML files (*.xml)"));
  if (fileName.isEmpty())
    return;

  settings.setXmlValue("common/order_folder","",QFileInfo(fileName).absolutePath());
  m_orderModel->writeXml(fileName,itemIndex);
  }
//
void TMainWnd::importBranch()
  {
  TSettings settings;
  QModelIndex itemIndex=orderTree->selectionModel()->currentIndex();
  if (itemIndex.isValid()==false)
    return;

  QString dirName=settings.getXmlValue("common/order_folder","","").toString();
  if (dirName.isEmpty())
    dirName=qApp->applicationDirPath()+"/orders";

  QDir dir(dirName);
  if (dir.exists()==false)
    dir.mkpath(dir.path());

  QString fileName=QFileDialog::getOpenFileName(this,tr("Order file"),dirName,tr("XML files (*.xml)"));
  if (fileName.isEmpty())
    return;

  settings.setXmlValue("common/order_folder","",QFileInfo(fileName).absolutePath());

  m_orderModel->presetAnswer(TOrderModel::NoAnswer);
  QModelIndex actualIndex=m_orderModel->readXml(fileName,itemIndex);
  if (actualIndex.isValid())
    moveToIndex(actualIndex);
  else
    QMessageBox::critical(this,tr("Error"),tr("You can't import this branch here."),QMessageBox::Ok);
  }
//
void TMainWnd::orderSelectionChanged(QItemSelection selected, QItemSelection deselected)
  {
  adjustIface();
  }
//
void TMainWnd::moveToIndex(QModelIndex index)
  {
  if (index.isValid())
    {
    orderTree->expand(index);
    orderTree->setCurrentIndex(index);
    orderTree->scrollTo(index,QAbstractItemView::PositionAtCenter);
    }
  }
//
/*void TMainWnd::correctOrder()
 {
 if (QMessageBox::question(this,tr("Confirmation"),tr("The Order will be corrected to coply with new order format.\nAre you really want to check and correct Order?"),QMessageBox::Yes | QMessageBox::No)!=QMessageBox::Yes)
  return;

 qApp->setOverrideCursor(QCursor(Qt::WaitCursor));
 walkTree(m_orderModel->rootIndex(),0,Correct);
 qApp->restoreOverrideCursor();
 }*/
