#pragma once

#include "ui_TEditDlg.h"

class TEditDlg : public QDialog, private Ui::TEditDlg
  {
    Q_OBJECT

  public:
    explicit TEditDlg(QWidget *parent=nullptr,const QString &text=QString(),bool readOnly=false);
    QString text() const;

  protected:
    void initCore();
    void initIface();
    void readSettings();
    void writeSettings();

  public slots:
    void accept();
    void reject();
  };
