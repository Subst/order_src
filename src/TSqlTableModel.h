#pragma once

#include <QSqlTableModel>
#include "TSqlRelation.h"

class TSqlTableModel : public QSqlTableModel
  {
    Q_OBJECT
  public:
    TSqlTableModel(QObject *parent=nullptr, const QSqlDatabase &dataBase=QSqlDatabase());

    void setTitle(const QString &title);
    QString title() const;

    void setHeaderField(const QString &field);
    QString headerField() const;

    void setDescriptionField(const QString &field);
    QString descriptionField() const;

    void setNodeType(const QString &type);
    QString nodeType() const;

    void setRelations(const QList <TSqlRelation> &relations);
    void clearRelations();
    void appendRelation(const TSqlRelation &relation);
    void removeRelation(const TSqlRelation &relation);
    QList <TSqlRelation> relations() const;

    bool isValidRelation(const TSqlRelation &relation) const;
    virtual void setHeaderNames()=0;

    quint16 sortFieldIndex();

  private:
    QString m_title;
    QString m_headerField;
    QString m_descriptionField;
    QString m_nodeType;
    QList <TSqlRelation> m_relations;

    quint16 m_sortColumn;

  public slots:
    bool select() override;
    void setSort(int column, Qt::SortOrder order) override;
    void sort(int column, Qt::SortOrder order) override;

  signals:
    void titleChanged(const QString &title);
    void headerFieldChanged(const QString &field);
    void descriptionFieldChanged(const QString &field);
    void nodeTypeChanged(const QString &nodeType);
  };
