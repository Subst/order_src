﻿#pragma once

#include "ui_TMainWnd.h"

#include <LimeReport>
#include <LRCallbackDS>

#include <QNetworkRequest>
#include <QNetworkReply>

class TSqlTableModel;
class TDataModule;

class TOrderModel;
class TOrderDelegate;

class TSpecialityModel;
class TProfileModel;
class TAttestationModel;
class TPersonModel;

class QProgressDialog;

class QNetworkAccessManager;

class TMainWnd : public QMainWindow, private Ui::TMainWnd
  {
    Q_OBJECT
  public:
    TMainWnd(QWidget *parent=nullptr);

  private:
    TDataModule *m_dataModule;

    TSpecialityModel *m_specialityModel;
    TProfileModel *m_profileModel;
    TAttestationModel *m_attestationModel;
    TPersonModel *m_personModel;

    QAction *m_toOrderActn;
    QAction *m_fromOrderActn;
    QAction *m_viewActn;
    QAction *m_editActn;

    QAction *m_emptyToOrderActn;

    QAction *m_expandAllActn;
    QAction *m_collapseAllActn;

    QAction *m_exportBranchActn;
    QAction *m_importBranchActn;

    TOrderModel *m_orderModel;
    TOrderDelegate *m_orderDelegate;

    LimeReport::ReportEngine *m_report;
    LimeReport::ICallbackDatasource *m_orderDataSource;
    QProgressDialog *m_progressDlg;

    int m_count;
    int m_current;

    QString m_baseName;
    QString m_orderName;

    QVector <QVector <QVariant>> m_orderData;

    QMap <QAction*,TSqlTableModel*> m_directoryMap;
    QMap <QCheckBox*,TTableView*> m_sortMap;
    QNetworkAccessManager *m_manager;

  protected:
    void initCore();
    void initIface();

    void writeSettings();
    void readSettings();

    void closeEvent(QCloseEvent *event);
    bool appendToOrder(TSqlTableModel *model, const QModelIndex &index);

    void setOrderData(quint16 position,const QModelIndex &parent,const QVector <QVariant> &data);
    void walkTree(const QModelIndex &index, int row=-1);

    void adjustIface();
    bool prepareReport();

    void moveToIndex(const QModelIndex &index);
    void clearInstallers();

  protected slots:
    void about();
    void aboutToQuit();

    void setCollegeInfo();

    void reportRenderStarted();
    void reportRenderFinished();
    void reportRenderPageFinished(int page);

    void designReport();
    void previewReport();
    void printReport();
    void printReportToPdf();

    void editDirectory();

    void createDatabase();
    void openDatabase();
    void saveDatabase();
    void removeDatabase();

    void createOrder();
    void openOrder();
    void saveOrder();
    void removeOrder();

    void specialitySelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
    void orderSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

    void getOrderCallbackData(const LimeReport::CallbackInfo &info,QVariant &value);
    void changeOrderPos(const LimeReport::CallbackInfo::ChangePosType &type,bool &result);

    void tableContextMenuRequested(const QPoint &point);
    void orderContextMenuRequested(const QPoint &point);

    void tableActivated(const QModelIndex &index);
    void treeDoubleClicked(const QModelIndex &index);

    void appendToOrder();
    void appendEmptyToOrder();

    void aboutShowInWindow();
    void viewInWindow(const QModelIndex &index=QModelIndex());
    void removeFromOrder();

    void editInWindow();
    void expandAll();
    void collapseAll();

    void exportBranch();
    void importBranch();

    void sortDictionary(bool checked);
    void dockVisibilityChanged();

    QNetworkRequest createRequest(const QString &fileName);
    void checkUpdate();
    void checkVersion();
    void updateProgress(qint64 bytesReceived, qint64 bytesTotal);
    void updateFinished();
  };
