#pragma once

#include "TSqlTableModel.h"

class TProfileModel : public TSqlTableModel
  {
    Q_OBJECT
  public:
    TProfileModel(QObject *parent=nullptr, const QSqlDatabase &dataBase=QSqlDatabase());
    void setHeaderNames();

  protected:
    QString selectStatement() const;
  };
