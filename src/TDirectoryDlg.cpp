#include "TDirectoryDlg.h"
#include "TSettings.h"

#include "TDataModule.h"
#include "TSqlTableModel.h"

#include "TItemDelegate.h"
#include "TEditDlg.h"

#include <QToolBar>
#include <QAction>
#include <QDebug>

#include <QSqlRecord>
#include <QSqlField>
#include <QSqlError>
#include <QSqlQuery>

#include <QMessageBox>
#include <QFileDialog>
#include <QScrollBar>

TDirectoryDlg::TDirectoryDlg(QWidget *parent, TSqlTableModel *tableModel) : QDialog(parent),
  m_dataModule(TDataModule::instanse()),
  m_tableModel(tableModel)
  {
  setupUi(this);

  initCore();
  initIface();
  }
//
void TDirectoryDlg::initCore()
  {
  quint16 fieldIndex=m_tableModel->fieldIndex("id");
  sortListActn->setChecked(fieldIndex != m_tableModel->sortFieldIndex());

  tableView->setModel(m_tableModel);
  m_tableDelegate=new TItemDelegate(this);
  tableView->setItemDelegate(m_tableDelegate);

  connect(okBtn,&QPushButton::clicked,this,&TDirectoryDlg::reject);

  connect(m_tableModel,&TSqlTableModel::dataChanged,this,&TDirectoryDlg::dataChanged);
  connect(tableView->selectionModel(),&QItemSelectionModel::currentChanged,this,&TDirectoryDlg::tableCurrentChanged);
  connect(tableView->selectionModel(),&QItemSelectionModel::selectionChanged,this,&TDirectoryDlg::tableSelectionChanged);

  connect(appendRecordActn,&QAction::triggered,this,&TDirectoryDlg::appendRecord);
  connect(editRecordActn,&QAction::triggered,this,&TDirectoryDlg::editRecord);
  connect(editInWindowActn,&QAction::triggered,this,&TDirectoryDlg::editInWindow);
  connect(removeRecordActn,&QAction::triggered,this,&TDirectoryDlg::removeRecord);
  connect(downRecordActn,&QAction::triggered,this,&TDirectoryDlg::downRecord);
  connect(upRecordActn,&QAction::triggered,this,&TDirectoryDlg::upRecord);
  connect(loadListActn,&QAction::triggered,this,&TDirectoryDlg::loadList);
  connect(clearListActn,&QAction::triggered,this,&TDirectoryDlg::clearList);
  connect(sortListActn,qOverload<bool>(&QAction::triggered),this,&TDirectoryDlg::sortList);
  connect(sortListActn,qOverload<bool>(&QAction::triggered),this,&TDirectoryDlg::sortChanged);

  connect(tableView,&TTableView::activated,this,&TDirectoryDlg::editRecord);

  toolBar=new QToolBar(QString(),this);
  toolBar->setIconSize(QSize(16,16));
  mainLayout->insertWidget(0,toolBar);

  mainLayout->addWidget(buttonFrame);

  toolBar->addAction(appendRecordActn);
  toolBar->addSeparator();
  toolBar->addAction(editRecordActn);
  toolBar->addSeparator();
  toolBar->addAction(editInWindowActn);
  toolBar->addSeparator();
  toolBar->addAction(removeRecordActn);
  toolBar->addSeparator();
  toolBar->addAction(loadListActn);
  toolBar->addSeparator();
  toolBar->addAction(clearListActn);
  toolBar->addSeparator();
  toolBar->addAction(downRecordActn);
  toolBar->addSeparator();
  toolBar->addAction(upRecordActn);
  toolBar->addSeparator();
  toolBar->addAction(sortListActn);

  QAction *separator;

  tableView->addAction(appendRecordActn);
  tableView->addAction(editRecordActn);
  tableView->addAction(editInWindowActn);
  tableView->addAction(removeRecordActn);
  separator=new QAction(this);
  separator->setSeparator(true);
  tableView->addAction(separator);
  tableView->addAction(loadListActn);
  tableView->addAction(clearListActn);
  separator=new QAction(this);
  separator->setSeparator(true);
  tableView->addAction(separator);
  tableView->addAction(downRecordActn);
  tableView->addAction(upRecordActn);
  separator=new QAction(this);
  separator->setSeparator(true);
  tableView->addAction(separator);
  tableView->addAction(sortListActn);
  }
//
void TDirectoryDlg::initIface()
  {
  readSettings();

  setWindowTitle(m_tableModel->title());
  tableView->setColumnHidden(m_tableModel->fieldIndex("id"),true);

  QSqlRecord record=m_tableModel->record();
  foreach (const TSqlRelation &relation,m_tableModel->relations())
    {
    if (record.contains(relation.field()))
      tableView->setColumnHidden(m_tableModel->fieldIndex(relation.field()),true);
    }

  tableView->horizontalHeader()->setSectionsMovable(true);
  tableView->horizontalHeader()->setSectionsClickable(false);
  tableView->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);

  tableView->verticalHeader()->setDefaultAlignment(Qt::AlignVCenter | Qt::AlignRight);

  adjustActions();
  }
//
void TDirectoryDlg::readSettings()
  {
  TSettings settings;
  restoreGeometry(settings.getXmlValue("directory_dialog/"+m_tableModel->tableName()+"/geometry","",0).toByteArray());
  tableView->horizontalHeader()->restoreState(settings.getXmlValue("directory_dialog/"+m_tableModel->tableName()+"/directory_table","",0).toByteArray());
  }
//
void TDirectoryDlg::writeSettings()
  {
  TSettings settings;
  settings.setXmlValue("directory_dialog/"+m_tableModel->tableName()+"/geometry","",saveGeometry());
  settings.setXmlValue("directory_dialog/"+m_tableModel->tableName()+"/directory_table","",tableView->horizontalHeader()->saveState());
  }
//
void TDirectoryDlg::reject()
  {
  writeSettings();
  QDialog::reject();
  }
//
void TDirectoryDlg::appendRecord()
  {
  disconnect(m_tableModel,&TSqlTableModel::dataChanged,this,&TDirectoryDlg::dataChanged);

  QString tableName=m_tableModel->tableName();
  quint16 position=m_dataModule->fieldIndex(tableName); // это оригинальный индекс поля в таблице
  QString value=tr("New %1").arg(m_tableModel->headerData(m_tableModel->record().indexOf(m_tableModel->headerField()),Qt::Horizontal).toString());

  QSqlRecord record=m_dataModule->tableRecord(m_tableModel->tableName()); // Это оргинальная запись из таблицы, как она в базе
  record.setValue(position,value);
  m_tableModel->insertRecord(-1,record);

  connect(m_tableModel,&TSqlTableModel::dataChanged,this,&TDirectoryDlg::dataChanged);

  position=m_tableModel->record().indexOf(m_tableModel->headerField()); // это индекс поля в таблице, как я вытащил с помощью select
  QModelIndexList items=m_tableModel->match(m_tableModel->index(0,position),Qt::DisplayRole,value,1,Qt::MatchExactly);
  if (items.size())
    {
    QModelIndex item=items.first();
    setupTableIndex(item,-1);
    tableView->edit(item);
    }
  }
//
void TDirectoryDlg::editRecord()
  {
  QModelIndex item=tableView->currentIndex();
  if (item.isValid()==false)
    return;

  setupTableIndex(item,tableView->verticalScrollBar()->value());
  tableView->edit(item);
  }
//
void TDirectoryDlg::removeRecord()
  {
  QModelIndexList indexes=tableView->selectionModel()->selectedRows();
  if (indexes.size()==false)
    return;

  quint16 row=indexes.last().row();
  QString string=(indexes.size()>1) ? tr("selected records") : tr("selected record");
  if (QMessageBox::question(this,tr("Confirm removing"),tr("Remove %1?").arg(string),QMessageBox::Yes | QMessageBox::No)==QMessageBox::No)
    return;

  // сделаю сортировку в обратном порядке, и удалю сначала последний,в конце первый, чтобы не побились индексы
  std::sort(indexes.begin(),indexes.end(),[this](const QModelIndex &first, const QModelIndex &second) {
    return first.row() > second.row();
    });

  foreach (QModelIndex index,indexes)
    m_tableModel->removeRow(index.row());

  if (m_tableModel->rowCount())
    {
    if (m_tableModel->index(row,1).isValid())
      setupTableIndex(m_tableModel->index(row,1),tableView->verticalScrollBar()->value());
    else
      setupTableIndex(m_tableModel->index(m_tableModel->rowCount()-1,1),-1);
    return;
    }

  adjustActions();
  }
//
void TDirectoryDlg::downRecord()
  {
  QModelIndex index=tableView->currentIndex();
  if (index.isValid()==false || index.row()==m_tableModel->rowCount()-1)
    return;

  disconnect(m_tableModel,&TSqlTableModel::dataChanged,this,&TDirectoryDlg::dataChanged);

  QSqlRecord record_1=m_tableModel->record(index.row());
  QSqlRecord record_2=m_tableModel->record(index.row()+1);

  QSqlRecord original_1=m_dataModule->tableRecord(m_tableModel->tableName());
  QSqlRecord original_2=original_1;

  m_tableModel->setRecord(index.row()+1,original_2);

  QString field;
  for (int i=0;i<original_1.count();i++)
    {
    field=original_1.fieldName(i);
    original_1.setValue(field,record_2.value(field));
    original_2.setValue(field,record_1.value(field));
    }

  m_tableModel->setRecord(index.row(),original_1);
  m_tableModel->setRecord(index.row()+1,original_2);

  connect(m_tableModel,&TSqlTableModel::dataChanged,this,&TDirectoryDlg::dataChanged);

  setupTableIndex(m_tableModel->index(index.row()+1,0),-1);
  }
//
void TDirectoryDlg::upRecord()
  {
  QModelIndex index=tableView->currentIndex();
  if (index.isValid()==false || index.row()==0)
    return;

  disconnect(m_tableModel,&TSqlTableModel::dataChanged,this,&TDirectoryDlg::dataChanged);

  QSqlRecord record_1=m_tableModel->record(index.row());
  QSqlRecord record_2=m_tableModel->record(index.row()-1);

  QSqlRecord original_1=m_dataModule->tableRecord(m_tableModel->tableName());
  QSqlRecord original_2=original_1;

  m_tableModel->setRecord(index.row()-1,original_2);

  QString field;
  for (int i=0;i<original_1.count();i++)
    {
    field=original_1.fieldName(i);
    original_1.setValue(field,record_2.value(field));
    original_2.setValue(field,record_1.value(field));
    }

  m_tableModel->setRecord(index.row(),original_1);
  m_tableModel->setRecord(index.row()-1,original_2);

  connect(m_tableModel,&TSqlTableModel::dataChanged,this,&TDirectoryDlg::dataChanged);;

  setupTableIndex(m_tableModel->index(index.row()-1,0),-1);
  }
//
void TDirectoryDlg::dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
  {
  QModelIndex index=tableView->currentIndex();
  int id=-1;

  if (topLeft==bottomRight)
    {
    id=m_tableModel->data(m_tableModel->index(index.row(),m_tableModel->fieldIndex("id"))).toInt();// record(index.row()).value("id").toInt();
    if (m_tableModel->rowCount()==0)
      return;

    QModelIndexList indexes=m_tableModel->match(m_tableModel->index(0,0),Qt::DisplayRole,id,1,Qt::MatchExactly);
    if (indexes.size())
      index=m_tableModel->index(indexes.first().row(),index.column());
    }

  int position=tableView->verticalScrollBar()->value();
  if (m_tableModel->submit()==false)
    m_tableModel->revert();
  m_tableModel->select();

  if (topLeft==bottomRight)
    setupTableIndex(index,position);
  }
//
void TDirectoryDlg::loadList()
  {
  TSettings settings;
  QString dir=settings.getXmlValue("common/files_folder","",qApp->applicationDirPath()+"/files").toString();
  QString fileName=QFileDialog::getOpenFileName(this,tr("Open input file '%1'").arg(m_tableModel->title(),dir,tr("Text files (*.txt *.csv)")));
  if (fileName.isEmpty())
    return;

  settings.setXmlValue("common/files_folder","",QFileInfo(fileName).absolutePath().remove(qApp->applicationDirPath()+"/"));
  QFile file(fileName);
  if (file.open(QIODevice::Text | QIODevice::ReadOnly)==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't open file \n%1").arg(fileName));
    return;
    }

  QCursor::setPos(pos()+tableView->pos()+QPoint(40,40));
  qApp->setOverrideCursor(QCursor(Qt::WaitCursor));
  qApp->processEvents();

  QSqlRecord record=m_dataModule->tableRecord(m_tableModel->tableName());
  QStringList fields, params;
  for (int i=0;i<record.count();i++)
    {
    fields<<record.fieldName(i);
    params<<"?";
    }
  QString queryText="insert or ignore into "+m_tableModel->tableName()+"("+fields.join(",")+") values ("+params.join(",")+")";

  QVector <QVariantList> values;
  for (int i=0;i<record.count();i++)
    {
    QVariantList value;
    values.append(value);
    }

  while (file.atEnd()==false)
    {
    QString string=QString::fromLocal8Bit(file.readLine().trimmed());
    if (string.startsWith('#'))
      continue;

    QStringList strings=string.split(";");
    for (int i=0;i<record.count();i++)
      {
      if (i<strings.size() && strings.at(i).size())
        values[i].append(strings.at(i)[0].toUpper()+strings.at(i).mid(1));
      else
        values[i].append(QVariant());
      }
    }
  file.close();

  QSqlQuery query(m_dataModule->database());
  query.prepare(queryText);

  for (int i=0;i<record.count();i++)
    query.addBindValue(values.at(i));

  query.execBatch();
  query.clear();
  m_tableModel->select();

  qApp->restoreOverrideCursor();

  if (m_tableModel->rowCount())
    setupTableIndex(m_tableModel->index(0,1),tableView->verticalScrollBar()->value());
  }
//
void TDirectoryDlg::clearList()
  {
  if (QMessageBox::question(this,tr("Confirm clearing"),tr("Clear List?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::No)
    return;

  QCursor::setPos(pos()+tableView->pos()+QPoint(40,40));
  qApp->setOverrideCursor(QCursor(Qt::WaitCursor));
  qApp->processEvents();

  QSqlQuery query(m_dataModule->database());
  QString queryText=QString("delete from %1").arg(m_tableModel->tableName());
  if (m_tableModel->filter().isEmpty()==false)
    queryText+=QString(" where %1").arg(m_tableModel->filter());

  query.exec(queryText);
  query.clear();
  m_tableModel->select();

  qApp->restoreOverrideCursor();
  adjustActions();
  }
//
void TDirectoryDlg::sortList(bool checked)
  {
  quint16 fieldIndex=m_tableModel->fieldIndex("id");
  if (checked==true)
    {
    QString fieldName=m_tableModel->headerField();
    fieldIndex=m_tableModel->fieldIndex(fieldName);
    foreach (const TSqlRelation &relation, m_tableModel->relations())
      {
      if (fieldName!=relation.visibleField())
        continue;

      fieldName=relation.field();
      fieldIndex=m_tableModel->fieldIndex(fieldName);
      break;
      }
    }

  QVariant value=m_tableModel->index(tableView->currentIndex().row(),fieldIndex).data();
  m_tableModel->sort(fieldIndex,Qt::AscendingOrder);

  if (value.isValid()==false)
    return;

  QModelIndexList indexes=m_tableModel->match(m_tableModel->index(0,fieldIndex),Qt::DisplayRole,value,1,Qt::MatchExactly);
  if (indexes.size()>0)
    setupTableIndex(indexes.first(),-1);
  }
//
void TDirectoryDlg::tableCurrentChanged(const QModelIndex &current, const QModelIndex &previous)
  {
  adjustActions();
  }
//
void TDirectoryDlg::tableSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
  {
  adjustActions();
  }
//
void TDirectoryDlg::adjustActions()
  {
  QModelIndexList selected=tableView->selectionModel()->selectedRows();
  QModelIndex index=tableView->currentIndex();

  appendRecordActn->setEnabled(true);
  editRecordActn->setEnabled(selected.size() && index.isValid());
  editInWindowActn->setEnabled(selected.size() && index.isValid() && m_tableModel->record().field(index.column()).type()==QVariant::String);

  foreach (const TSqlRelation &relation, m_tableModel->relations())
    {
    if (index.column()==m_tableModel->fieldIndex(relation.visibleField()))
      {
      editInWindowActn->setEnabled(false);
      break;
      }
    }

  removeRecordActn->setEnabled(selected.size() && index.isValid());
  upRecordActn->setEnabled(selected.size() && index.isValid() && index.row()>0 && sortListActn->isChecked()==false);
  downRecordActn->setEnabled(selected.size() && index.isValid() && index.row()<m_tableModel->rowCount()-1 && sortListActn->isChecked()==false);
  loadListActn->setEnabled(true);
  clearListActn->setEnabled(m_tableModel->rowCount());
  sortListActn->setEnabled(m_tableModel->rowCount());
  }
//
void TDirectoryDlg::setupTableIndex(const QModelIndex &index, int position)
  {
  tableView->verticalHeader()->reset();
  tableView->setFocus();
  tableView->selectRow(index.row());
  tableView->setCurrentIndex(index);
  if (position==-1)
    tableView->scrollTo(index,QAbstractItemView::EnsureVisible);
  else
    tableView->verticalScrollBar()->setValue(position);
  }
//
void TDirectoryDlg::editInWindow()
  {
  QModelIndex itemIndex=tableView->currentIndex();
  if (itemIndex.isValid()==false)
    return;

  TEditDlg *editDlg=new TEditDlg(this,m_tableModel->data(itemIndex).toString(),false);
  editDlg->setWindowTitle(tr("Edit text"));
  if (editDlg->exec()==QDialog::Accepted)
    m_tableModel->setData(itemIndex,editDlg->text());

  delete editDlg;
  }
