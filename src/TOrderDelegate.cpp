#include "TOrderDelegate.h"
#include "TOrderModel.h"

#include <QApplication>
#include <QDebug>

TOrderDelegate::TOrderDelegate(QObject *parent) : QStyledItemDelegate(parent)
  {

  }
//
void TOrderDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
  {
  const TOrderModel *model=qobject_cast <const TOrderModel*>(index.model());
  QString name=model->data(model->index(index.row(),TOrderModel::Name,index.parent())).toString();

  QStyleOptionViewItem itemOption=modifyOption(option,name);
  itemOption.text=index.data().toString();
  qApp->style()->drawControl(QStyle::CE_ItemViewItem,&itemOption,painter);
  }
//
QWidget *TOrderDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
  {
  const TOrderModel *model=qobject_cast <const TOrderModel*>(index.model());
  QString name=model->data(model->index(index.row(),TOrderModel::Name,index.parent())).toString();

  QStyleOptionViewItem itemOption=modifyOption(option,name);

  QWidget *editor=QStyledItemDelegate::createEditor(parent,itemOption,index);
  editor->setFont(itemOption.font);
  return editor;
  }
//
QStyleOptionViewItem TOrderDelegate::modifyOption(const QStyleOptionViewItem &option, const QString &name) const
  {
  QStyleOptionViewItem itemOption(option);
  if (name=="order" || name=="paragraph" || name=="reason")
    {
    itemOption.palette.setColor(QPalette::Text,Qt::darkRed);
    itemOption.font.setBold(true);
    return itemOption;
    }

  if (name=="rector" || name=="management" || name=="performer")
    {
    itemOption.palette.setColor(QPalette::Text,Qt::darkRed);
    return itemOption;
    }

  if (name=="section" || name=="speciality")
    {
    itemOption.palette.setColor(QPalette::Text,Qt::darkBlue);
    itemOption.font.setBold(true);
    return itemOption;
    }

  if (name=="profile" || name=="chairman" || name=="secretary" || name=="attestation")
    {
    itemOption.palette.setColor(QPalette::Text,Qt::darkBlue);
    return itemOption;
    }

  if (name=="attestation_item")
    {
    itemOption.palette.setColor(QPalette::Text,QColor::fromRgb(0,64,0));
    itemOption.font.setBold(true);
    return itemOption;
    }

  if (name=="member")
    {
    itemOption.palette.setColor(QPalette::Text,QColor::fromRgb(0,64,0));
    return itemOption;
    }

  itemOption.palette.setColor(QPalette::Text,Qt::black);
  return itemOption;
  }
