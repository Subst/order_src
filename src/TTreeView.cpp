#include "TTreeView.h"

#include <QKeyEvent>
#include <QDebug>

TTreeView::TTreeView(QWidget *parent) : QTreeView(parent)
  {

  }
//
void TTreeView::keyPressEvent(QKeyEvent *event)
  {
  // без Ctrl пока не интересны
  if (event->modifiers().testFlag(Qt::ControlModifier)==false)
    {
    QTreeView::keyPressEvent(event);
    return;
    }

  // Ctrl + Enter
  if (event->key()==Qt::Key_Enter || event->key()==Qt::Key_Return)
    {
    emit ctrlEnterPressed(currentIndex());
    return;
    }
  // Ctrl + Delete
  if (event->key()==Qt::Key_Delete)
    {
    emit ctrlDeletePressed(currentIndex());
    return;
    }
  // Ctrl + Insert
  if (event->key()==Qt::Key_Insert)
    {
    emit ctrlInsertPressed(currentIndex());
    return;
    }
  // Ctrl + PageDown
  if (event->key()==Qt::Key_PageDown)
    {
    emit ctrlPageDownPressed(currentIndex());
    return;
    }
  // Ctrl + PageUp
  if (event->key()==Qt::Key_PageUp)
    {
    emit ctrlPageUpPressed(currentIndex());
    return;
    }
  // Ctrl + End
  if (event->key()==Qt::Key_End)
    {
    emit ctrlEndPressed(currentIndex());
    return;
    }
  // Ctrl + Home
  if (event->key()==Qt::Key_Home)
    {
    emit ctrlHomePressed(currentIndex());
    return;
    }

  // просто Enter(Return)
  // TODO: если description, то сразу в окне делать (или если текст длинный, но насколько длинный? - QFonMetrics)?
  /*if ((event->key()==Qt::Key_Enter || event->key()==Qt::Key_Return) && state()!=QAbstractItemView::EditingState &&
      editTriggers().testFlag(QAbstractItemView::EditKeyPressed)==true)
    {
    edit(currentIndex());
    return;
    }*/

  QTreeView::keyPressEvent(event);
  }
//
