#include "TCollegeDlg.h"

#include "TSettings.h"
#include "TDataModule.h"
#include "TCollegeModel.h"

#include <QDebug>

TCollegeDlg::TCollegeDlg(QWidget *parent) : QDialog(parent)
  {
  setupUi(this);
  initCore();
  initIface();
  }
//
void TCollegeDlg::initCore()
  {
  m_collegeModel=qobject_cast <TCollegeModel*>(TDataModule::instanse()->tableModel("college"));

  connect(okBtn,&QPushButton::clicked,this,&TCollegeDlg::accept);
  connect(cancelBtn,&QPushButton::clicked,this,&TCollegeDlg::reject);

  QString title=QString();
  QString city=QString();
  QString region=QString();
  QString rector=QString();

  if (m_collegeModel->rowCount())
    {
    QSqlRecord record=m_collegeModel->record(0);
    title=record.value("title").toString();
    city=record.value("city").toString();
    region=record.value("region").toString();
    rector=record.value("rector").toString();
    }

  titleEdit->setPlainText(title);
  cityEdit->setText(city);
  regionEdit->setText(region);
  rectorEdit->setText(rector);
  }
//
void TCollegeDlg::initIface()
  {
  titleEdit->setWordWrapMode(QTextOption::WordWrap);
  readSettings();
  }
//
void TCollegeDlg::readSettings()
  {
  restoreGeometry(TSettings().getXmlValue("college_dialog/geometry","",0).toByteArray());
  }
//
void TCollegeDlg::writeSettings()
  {
  TSettings().setXmlValue("college_dialog/geometry","",saveGeometry());
  }
//
void TCollegeDlg::accept()
  {
  QString title=titleEdit->toPlainText();
  QString city=cityEdit->text();
  QString region=regionEdit->text();
  QString rector=rectorEdit->text();

  QSqlRecord record=TDataModule::instanse()->tableRecord("college");
  record.setValue("title",title);
  record.setValue("city",city);
  record.setValue("region",region);
  record.setValue("rector",rector);

  if (m_collegeModel->rowCount())
    m_collegeModel->setRecord(0,record);
  else
    m_collegeModel->insertRecord(-1,record);

  m_collegeModel->submit();

  writeSettings();
  QDialog::accept();
  }
//
void TCollegeDlg::reject()
  {
  writeSettings();
  QDialog::reject();
  }
