#include "TProfileModel.h"

#include <QSqlRecord>

TProfileModel::TProfileModel(QObject *parent, const QSqlDatabase &dataBase) : TSqlTableModel(parent, dataBase)
  {
  setTable("profiles");
  setTitle(tr("Profiles"));

  setHeaderField("profile");
  setNodeType("profile_item");

  appendRelation(TSqlRelation("speciality_id","speciality","specialities","id","speciality"));

  setSort(fieldIndex("id"),Qt::AscendingOrder);
  setEditStrategy(QSqlTableModel::OnFieldChange);
  }
//
void TProfileModel::setHeaderNames()
  {
  setHeaderData(fieldIndex("id"),Qt::Horizontal,tr("ID"),Qt::DisplayRole);
  setHeaderData(fieldIndex("profile"),Qt::Horizontal,tr("Profile"),Qt::DisplayRole);
  setHeaderData(fieldIndex("speciality_id"),Qt::Horizontal,tr("Speciality ID"),Qt::DisplayRole);
  setHeaderData(fieldIndex("speciality"),Qt::Horizontal,tr("Speciality"),Qt::DisplayRole);

  for (int i=0;i<record().count();i++)
    setHeaderData(i,Qt::Horizontal,record().fieldName(i),Qt::UserRole);
  }
//
QString TProfileModel::selectStatement() const
  {
  QString statement="select profiles.id, profiles.profile, profiles.speciality_id, specialities.speciality "
                    "from profiles "
                    "left join specialities on profiles.speciality_id=specialities.id";

  if (filter().isEmpty()==false)
    statement.append(" where "+filter());

  statement.append(" "+orderByClause());
  return statement;
  }
//
