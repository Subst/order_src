#pragma once

#include <QTableView>

class TTableView : public QTableView
  {
    Q_OBJECT
  public:
    TTableView(QWidget *parent=nullptr);

  protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    //bool edit(const QModelIndex &,EditTrigger,QEvent*);

  signals:
    void ctrlEnterPressed(const QModelIndex &index);
  };
