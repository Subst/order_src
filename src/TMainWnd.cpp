﻿#include "TMainWnd.h"
#include "TSettings.h"

#include "TDataModule.h"

#include "TDirectoryDlg.h"
#include "TCollegeDlg.h"

#include "TSpecialityModel.h"
#include "TProfileModel.h"
#include "TAttestationModel.h"
#include "TPersonModel.h"

#include "TOrderModel.h"
#include "TOrderDelegate.h"

#include "TEditDlg.h"

#include "QAesEncryption.h"

#include <QMessageBox>
#include <QCloseEvent>
#include <QDir>
#include <QProgressDialog>
#include <QFileDialog>
#include <QInputDialog>
#include <QDebug>

#include <QCryptographicHash>

#include <QNetworkAccessManager>
#include <QVersionNumber>
#include <QUrlQuery>
#include <QStandardPaths>
#include <QProcess>

TMainWnd::TMainWnd(QWidget *parent) : QMainWindow(parent),
  m_manager(new QNetworkAccessManager(this))
  {
  /*QVersionNumber remoteVersion=QVersionNumber::fromString(versionReply->readAll().simplified());
  QVersionNumber currentVersion=QVersionNumber::fromString(ORDER_VERSION);*/
  qDebug()<<QCryptographicHash::hash("Order",QCryptographicHash::Md5).toHex().toUpper();
  setupUi(this);
  qDebug()<<ORDER_VERSION;

  m_manager->setTransferTimeout(30000);
  m_manager->setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
  m_manager->setAutoDeleteReplies(true);

  QString fromFile=":templates/config.xml";
  QString toFile=qApp->applicationDirPath()+"/order.xml";
  if (QFile::exists(toFile)==false)
    QFile::copy(fromFile,toFile);

  QFile::setPermissions(toFile,QFile::permissions(toFile) | QFile::WriteOwner | QFile::WriteUser | QFile::WriteGroup | QFile::WriteOther);

  clearInstallers();
  initCore();
  initIface();
  }
//
void TMainWnd::initCore()
  {
  m_dataModule=new TDataModule(this);

  TSettings settings;
  QString fileName=settings.getXmlValue("common/database","",qApp->applicationDirPath()+"/dbase/order.sqt").toString();
  if (QFile::exists(fileName)==false || m_dataModule->openBase(fileName)==false)
    {
    int answer=QMessageBox::critical(this,tr("Error"),tr("Can't open last working database.\nPress 'Open' to open existing database\nor 'Save' to create new one."),
                                     QMessageBox::Open | QMessageBox::Save);

    QString dirName=settings.getXmlValue("common/database_folder","",qApp->applicationDirPath()+"/dbase").toString();
    QDir dir(dirName);

    if (dir.exists()==false)
      dir.mkpath(dir.path());

    switch (answer)
      {
      case QMessageBox::Save:
        {
        fileName=QFileDialog::getSaveFileName(this,tr("Select new database"),dirName,tr("Database files (*.sqt)"));
        if (fileName.isEmpty())
          exit(0);

        if (QFile::exists(fileName))
          QFile::remove(fileName);

        if (QFile::copy(":database/order.sqt",fileName)==false)
          {
          QMessageBox::critical(this,tr("Error"),tr("Can't create database file.\n%1\nProgram now exit.").arg(fileName));
          exit(0);
          }
        QFile::setPermissions(fileName,QFile::permissions(fileName) | QFile::WriteOwner | QFile::WriteUser | QFile::WriteGroup | QFile::WriteOther);
        break;
        }
      case QMessageBox::Open:
        {
        fileName=QFileDialog::getOpenFileName(this,tr("Select existing database"),dirName,tr("Database files (*.sqt)"));
        if (fileName.isEmpty())
          exit(0);
        break;
        }
      default:
        {
        exit(0);
        break;
        }
      }
    }

  m_baseName=fileName;
  if (m_dataModule->database().isOpen()==false && m_dataModule->openBase(fileName)==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't open database file.\n%1\nProgram now exit.").arg(m_dataModule->baseName()));
    exit(0);
    }

  activateWindow();

  setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));

  m_specialityModel=qobject_cast <TSpecialityModel*> (m_dataModule->tableModel("specialities"));
  specialityTable->setModel(m_specialityModel);

  m_profileModel=qobject_cast <TProfileModel*> (m_dataModule->tableModel("profiles"));
  profileTable->setModel(m_profileModel);

  m_attestationModel=qobject_cast <TAttestationModel*> (m_dataModule->tableModel("attestations"));
  attestationTable->setModel(m_attestationModel);

  m_personModel=qobject_cast <TPersonModel*> (m_dataModule->tableModel("persons"));
  personTable->setModel(m_personModel);

  connect(specialityTable->selectionModel(),&QItemSelectionModel::selectionChanged,this,&TMainWnd::specialitySelectionChanged);

  QVector <TTableView*> tables={specialityTable, profileTable, personTable, attestationTable};
  QVector <QCheckBox*> checkBoxes={specialitySortCheck, profileSortCheck, personSortCheck, attestationSortCheck};
  QVector <QAction*> actions={specialityActn, profileActn, personActn, attestationActn};

  Q_ASSERT(tables.size()==checkBoxes.size() && tables.size()==actions.size());
  for (quint8 i=0;i<tables.size();i++)
    {
    TTableView *table=tables.at(i);
    connect(table,&TTableView::customContextMenuRequested,this,&TMainWnd::tableContextMenuRequested);
    connect(table,&TTableView::ctrlEnterPressed,this,&TMainWnd::viewInWindow);
    connect(table,&TTableView::activated,this,&TMainWnd::tableActivated);

    table->horizontalHeader()->setSectionsMovable(true);
    table->horizontalHeader()->setSectionsClickable(false);

    QCheckBox *checkBox=checkBoxes.at(i);
    connect(checkBox,&QCheckBox::clicked,this,&TMainWnd::sortDictionary);
    m_sortMap.insert(checkBox,table);

    QAction *action=actions.at(i);
    connect(action,&QAction::triggered,this,&TMainWnd::editDirectory);

    TSqlTableModel *model=qobject_cast<TSqlTableModel*>(table->model());
    m_directoryMap.insert(action,model);
    }

  connect(orderTree,&TTreeView::customContextMenuRequested,this,&TMainWnd::orderContextMenuRequested);
  connect(orderTree,&TTreeView::ctrlEnterPressed,this,&TMainWnd::editInWindow);
  connect(orderTree,&TTreeView::ctrlDeletePressed,this,&TMainWnd::removeFromOrder);
  connect(orderTree,&TTreeView::ctrlInsertPressed,this,&TMainWnd::appendEmptyToOrder);
  connect(orderTree,&TTreeView::ctrlPageDownPressed,this,&TMainWnd::exportBranch);
  connect(orderTree,&TTreeView::ctrlPageUpPressed,this,&TMainWnd::importBranch);
  connect(orderTree,&TTreeView::ctrlEndPressed,this,&TMainWnd::expandAll);
  connect(orderTree,&TTreeView::ctrlHomePressed,this,&TMainWnd::collapseAll);

  connect(orderTree,&QAbstractItemView::activated,orderTree,qOverload<const QModelIndex&>(&TTreeView::edit));

  connect(qApp,&QApplication::aboutToQuit,this,&TMainWnd::aboutToQuit);

  connect(createDatabaseActn,&QAction::triggered,this,&TMainWnd::createDatabase);
  connect(openDatabaseActn,&QAction::triggered,this,&TMainWnd::openDatabase);
  connect(saveDatabaseActn,&QAction::triggered,this,&TMainWnd::saveDatabase);
  connect(removeDatabaseActn,&QAction::triggered,this,&TMainWnd::removeDatabase);

  connect(newOrderActn,&QAction::triggered,this,&TMainWnd::createOrder);
  connect(openOrderActn,&QAction::triggered,this,&TMainWnd::openOrder);
  connect(saveOrderActn,&QAction::triggered,this,&TMainWnd::saveOrder);
  connect(removeOrderActn,&QAction::triggered,this,&TMainWnd::removeOrder);

  connect(collegeActn,&QAction::triggered,this,&TMainWnd::setCollegeInfo);
  connect(exitActn,&QAction::triggered,this,&TMainWnd::close);
  connect(aboutQtActn,&QAction::triggered,qApp,&QApplication::aboutQt);
  connect(aboutActn,&QAction::triggered,this,&TMainWnd::about);

  connect(updateActn,&QAction::triggered,this,&TMainWnd::checkUpdate);

  // toolbars в меню
  connect(viewDatabaseActn,&QAction::triggered,databaseToolBar,&QToolBar::setVisible);
  connect(databaseToolBar,&QToolBar::visibilityChanged,viewDatabaseActn,&QAction::setChecked);

  connect(viewOrderActn,&QAction::triggered,orderToolBar,&QToolBar::setVisible);
  connect(orderToolBar,&QToolBar::visibilityChanged,viewOrderActn,&QAction::setChecked);

  connect(viewReportActn,&QAction::triggered,reportToolBar,&QToolBar::setVisible);
  connect(reportToolBar,&QToolBar::visibilityChanged,viewReportActn,&QAction::setChecked);

  connect(viewServiceActn,&QAction::triggered,serviceToolBar,&QToolBar::setVisible);
  connect(serviceToolBar,&QToolBar::visibilityChanged,viewServiceActn,&QAction::setChecked);

  // docks в меню
  connect(viewSpecialityActn,&QAction::triggered,specialityDock,&QDockWidget::setVisible);
  connect(viewProfileActn,&QAction::triggered,profileDock,&QDockWidget::setVisible);
  connect(viewPersonActn,&QAction::triggered,personDock,&QDockWidget::setVisible);
  connect(viewAttestationActn,&QAction::triggered,attestationDock,&QDockWidget::setVisible);

  // просто QDockWidget::visibilitiChaned(bool) робит не так, как надо.
  // DockWidget может быть во вкладках, но не на переднем плане, visibilitiChaned(bool) передаст false в параметре
  QVector <QDockWidget*> widgets={specialityDock,profileDock,personDock,attestationDock};
  foreach (QDockWidget *widget, widgets)
    connect(widget,&QDockWidget::visibilityChanged,this,&TMainWnd::dockVisibilityChanged);

  m_toOrderActn=new QAction(QIcon(":images/next"),tr("Append to Order"),this);
  m_toOrderActn->setShortcut(QKeySequence(Qt::Key_Enter));
  connect(m_toOrderActn,&QAction::triggered,this,qOverload<>(&TMainWnd::appendToOrder));

  m_fromOrderActn=new QAction(QIcon(":images/previous"),tr("Remove from Order"),this);
  m_fromOrderActn->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Delete));
  connect(m_fromOrderActn,&QAction::triggered,this,&TMainWnd::removeFromOrder);

  m_emptyToOrderActn=new QAction(QIcon(":images/next"),tr("Append empty record"),this);
  m_emptyToOrderActn->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Insert));
  connect(m_emptyToOrderActn,&QAction::triggered,this,&TMainWnd::appendEmptyToOrder);

  m_expandAllActn=new QAction(QIcon(":images/expand"),tr("Expand All"),this);
  m_expandAllActn->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_End));
  connect(m_expandAllActn,&QAction::triggered,this,&TMainWnd::expandAll);

  m_collapseAllActn=new QAction(QIcon(":images/collapse"),tr("Collapse All"),this);
  m_collapseAllActn->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Home));
  connect(m_collapseAllActn,&QAction::triggered,this,&TMainWnd::collapseAll);

  m_viewActn=new QAction(QIcon(":images/editraise"),tr("View in window"),this);
  m_viewActn->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Enter));
  connect(m_viewActn,&QAction::triggered,this,&TMainWnd::aboutShowInWindow);

  m_editActn=new QAction(QIcon(":images/editraise"),tr("Edit in window"),this);
  m_editActn->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Enter));
  connect(m_editActn,&QAction::triggered,this,&TMainWnd::editInWindow);

  m_exportBranchActn=new QAction(QIcon(":images/export"),tr("Export Branch"),this);
  m_exportBranchActn->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_PageDown));
  connect(m_exportBranchActn,&QAction::triggered,this,&TMainWnd::exportBranch);

  m_importBranchActn=new QAction(QIcon(":images/import"),tr("Import Branch"),this);
  m_importBranchActn->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_PageUp));
  connect(m_importBranchActn,&QAction::triggered,this,&TMainWnd::importBranch);

  connect(designReportActn,&QAction::triggered,this,&TMainWnd::designReport);
  connect(previewReportActn,&QAction::triggered,this,&TMainWnd::previewReport);
  connect(printReportActn,&QAction::triggered,this,&TMainWnd::printReport);
  connect(printReportToPdfActn,&QAction::triggered,this,&TMainWnd::printReportToPdf);

  m_orderModel=new TOrderModel(this);
  orderTree->setModel(m_orderModel);
  m_orderDelegate=new TOrderDelegate(this);
  orderTree->setItemDelegate(m_orderDelegate);

  connect(orderTree->selectionModel(),&QItemSelectionModel::selectionChanged,this,&TMainWnd::orderSelectionChanged);

  QModelIndex rootIndex=m_orderModel->index(0,TOrderModel::Name);
  orderTree->setRootIndex(rootIndex);

  fileName=TSettings().getXmlValue("common/order","",qApp->applicationDirPath()+"/orders/order.xml").toString();
  if (fileName.isEmpty()==false && QFile::exists(fileName))
    {
    m_orderModel->presetAnswer(TOrderModel::AppendToAllAnswer);
    QModelIndex actualIndex=m_orderModel->readXml(fileName,m_orderModel->rootIndex());
    if (actualIndex.isValid())
      {
      m_orderName=fileName;
      setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));
      orderTree->expandAll();
      orderTree->setCurrentIndex(actualIndex);
      if (m_orderModel->isChanged())
        QMessageBox::information(this,tr("Information"),tr("The Order has been automatically corrected\nto coply with new order format."),QMessageBox::Ok);
      }
    }

  m_report=new LimeReport::ReportEngine(this);
  m_report->setShowProgressDialog(true);
  m_report->setPreviewWindowIcon(QIcon(QPixmap(":/images/order")));

  connect(m_report,&LimeReport::ReportEngine::renderStarted,this,&TMainWnd::reportRenderStarted);
  connect(m_report,&LimeReport::ReportEngine::renderFinished,this,&TMainWnd::reportRenderFinished);
  connect(m_report,&LimeReport::ReportEngine::renderPageFinished,this,&TMainWnd::reportRenderPageFinished);

  m_report->dataManager()->addModel("college",m_dataModule->tableModel("college"),true);
  m_orderDataSource=m_report->dataManager()->createCallbackDatasource("order");

  connect(m_orderDataSource,&LimeReport::ICallbackDatasource::getCallbackData,this,&TMainWnd::getOrderCallbackData);
  connect(m_orderDataSource,&LimeReport::ICallbackDatasource::changePos,this,&TMainWnd::changeOrderPos);
  }
//
void TMainWnd::initIface()
  {
  readSettings();

  specialityTable->setColumnHidden(m_specialityModel->fieldIndex("id"),true);
  profileTable->setColumnHidden(m_profileModel->fieldIndex("id"),true);
  profileTable->setColumnHidden(m_profileModel->fieldIndex("speciality_id"),true);
  attestationTable->setColumnHidden(m_attestationModel->fieldIndex("id"),true);
  personTable->setColumnHidden(m_personModel->fieldIndex("id"),true);

  specialityTable->setFocus();
  if (m_specialityModel->rowCount())
    specialityTable->selectRow(0);

  orderTree->header()->setSectionHidden(TOrderModel::Name,true);
  orderTree->header()->setSectionHidden(TOrderModel::Size,true);
  orderTree->header()->setSectionHidden(TOrderModel::Permanent,true);
  orderTree->expandAll();

  adjustIface();
  }
//
void TMainWnd::adjustIface()
  {
  QModelIndex itemIndex=orderTree->currentIndex();
  QString name=m_orderModel->index(itemIndex.row(),TOrderModel::Name,itemIndex.parent()).data().toString();

  saveOrderActn->setEnabled(m_orderModel->root()->children.size()>0);
  if (m_orderModel->root()->children.size()>0 && (name=="speciality" || name=="order"))
    {
    previewReportActn->setEnabled(true);
    printReportActn->setEnabled(true);
    printReportToPdfActn->setEnabled(true);
    return;
    }

  previewReportActn->setEnabled(false);
  printReportActn->setEnabled(false);
  printReportToPdfActn->setEnabled(false);
  }
//
void TMainWnd::readSettings()
  {
  TSettings settings;
  restoreState(settings.getXmlValue("main_window/state","",0).toByteArray());
  restoreGeometry(settings.getXmlValue("main_window/geometry","",0).toByteArray());

  specialityTable->horizontalHeader()->restoreState(settings.getXmlValue("main_window/speciality_table","",0).toByteArray());
  profileTable->horizontalHeader()->restoreState(settings.getXmlValue("main_window/profile_table","",0).toByteArray());
  attestationTable->horizontalHeader()->restoreState(settings.getXmlValue("main_window/attestation_table","",0).toByteArray());
  personTable->horizontalHeader()->restoreState(settings.getXmlValue("main_window/person_table","",0).toByteArray());

  orderTree->header()->restoreState(settings.getXmlValue("main_window/order_tree","",0).toByteArray());
  }
//
void TMainWnd::writeSettings()
  {
  TSettings settings;
  settings.setXmlValue("common/database","",m_baseName);
  settings.setXmlValue("common/order","",m_orderName);

  settings.setXmlValue("main_window/state","",saveState());
  settings.setXmlValue("main_window/geometry","",saveGeometry());

  settings.setXmlValue("main_window/speciality_table","",specialityTable->horizontalHeader()->saveState());
  settings.setXmlValue("main_window/profile_table","",profileTable->horizontalHeader()->saveState());
  settings.setXmlValue("main_window/attestation_table","",attestationTable->horizontalHeader()->saveState());
  settings.setXmlValue("main_window/person_table","",personTable->horizontalHeader()->saveState());

  settings.setXmlValue("main_window/order_tree","",orderTree->header()->saveState());
  }
//
void TMainWnd::aboutToQuit()
  {
  writeSettings();
  m_dataModule->closeBase();
  delete m_dataModule;
  //delete m_orderDataSource;
  foreach (const QString &connection,QSqlDatabase::connectionNames())
    QSqlDatabase::removeDatabase(connection);

  TSettings settings;
  QString fileName=QDir::toNativeSeparators(settings.getXmlValue("common/update_path","","none").toString());
  settings.removeXmlEntry("common/update_path");

  if (QFile::exists(fileName)==false) // нету такого файла
    return;

#ifdef Q_OS_WIN
  QString msiPath=QDir::toNativeSeparators(QStandardPaths::findExecutable("msiexec.exe"));
  if (msiPath.isEmpty())
    return;
  QStringList arguments={"/package",fileName};
  if (fileName.isEmpty()==false)
    QProcess::startDetached(msiPath,arguments);
#endif
  }
//
void TMainWnd::about()
  {
  QMessageBox::information(this,tr("About program"),tr("Order. Bla-bla about...\nCopyrigh (c) Denis P.Classen, since 2017.\nVersion: %1").arg(ORDER_VERSION),QMessageBox::Ok);
  }
//
void TMainWnd::closeEvent(QCloseEvent *event)
  {
  if (QMessageBox::question(this,tr("Confirmation"),tr("Exit program?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::No)
    {
    event->ignore();
    return;
    }

  if (m_orderModel->isChanged()==false)
    {
    event->accept();
    return;
    }

  if (QMessageBox::question(this,tr("Confirmation"),tr("Current Order has been changed.\nSave it before exit?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    saveOrder();

  event->accept();
  }
//
void TMainWnd::setCollegeInfo()
  {
  TCollegeDlg *collegeDlg=new TCollegeDlg(this);
  collegeDlg->exec();
  delete collegeDlg;
  }
//
void TMainWnd::editDirectory()
  {
  QAction *action=qobject_cast<QAction*>(sender());
  TSqlTableModel *tableModel=m_directoryMap.value(action);
  if (tableModel==nullptr)
    return;
  // при работе со словарем фильтр убираем
  QString filter=tableModel->filter();
  if (filter.isEmpty()==false)
    tableModel->setFilter(QString());

  TDirectoryDlg *directoryDlg=new TDirectoryDlg(this, tableModel);
  QVector <TTableView *> tables={specialityTable, profileTable, personTable, attestationTable};
  foreach (TTableView *tableView,tables)
    {
    if (tableView->model()!=tableModel)
      continue;

    QCheckBox *checkBox=m_sortMap.key(tableView);
    if (checkBox==nullptr)
      continue;

    connect(directoryDlg,&TDirectoryDlg::sortChanged,checkBox,&QCheckBox::setChecked);
    }

  directoryDlg->exec();
  delete directoryDlg;
  // после работы со словарем фильтр восстанавливаем
  if (filter.isEmpty()==false)
    tableModel->setFilter(filter);
  }
//
void TMainWnd::createDatabase()
  {
  TSettings settings;
  QString dir=settings.getXmlValue("common/database_folder","",qApp->applicationDirPath()+"/dbase").toString();
  QString fileName=QFileDialog::getSaveFileName(this,tr("Create Database"),dir,tr("Database files (*.sqt)"));
  if (fileName.isEmpty())
    return;

  if (QFile::exists(fileName))
    QFile::remove(fileName);

  if (QFile::copy(":database/order.sqt",fileName)==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't create database file.\n%1.").arg(fileName));
    return;
    }

  writeSettings();
  QFile::setPermissions(fileName,QFile::permissions(fileName) | QFile::WriteOwner | QFile::WriteUser | QFile::WriteGroup | QFile::WriteOther);
  m_dataModule->closeBase();

  if (m_dataModule->openBase(fileName)==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't open database file.\n%1.").arg(m_dataModule->baseName()));
    return;
    }

  initIface();
  settings.setXmlValue("common/database_folder","",QFileInfo(fileName).absolutePath());
  m_baseName=fileName;
  setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));
  }
//
void TMainWnd::openDatabase()
  {
  TSettings settings;
  QString dir=settings.getXmlValue("common/database_folder","",qApp->applicationDirPath()+"/dbase").toString();
  QString fileName=QFileDialog::getOpenFileName(this,tr("Open Database"),dir,tr("Database files (*.sqt)"));
  if (fileName.isEmpty())
    return;

  writeSettings();
  m_dataModule->closeBase();
  if (m_dataModule->openBase(fileName)==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't open database file.\n%1.").arg(m_dataModule->baseName()));
    return;
    }

  initIface();
  settings.setXmlValue("common/database_folder","",QFileInfo(fileName).absolutePath());
  m_baseName=fileName;
  setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));
  }
//
void TMainWnd::saveDatabase()
  {
  TSettings settings;
  QString dir=settings.getXmlValue("common/database_folder","",qApp->applicationDirPath()+"/dbase").toString();

  QString fileName=QFileDialog::getSaveFileName(this,tr("Create Database"),dir,tr("Database files (*.sqt)"));
  if (fileName.isEmpty())
    return;

  if (QFile::exists(fileName))
    QFile::remove(fileName);

  if (QFile::copy(m_dataModule->baseName(),fileName)==false)
    QMessageBox::critical(this,tr("Error"),tr("Can't save database file.\n%1.").arg(m_dataModule->baseName()));

  settings.setXmlValue("common/database_folder","",QFileInfo(fileName).absolutePath());
  }
//
void TMainWnd::removeDatabase()
  {
  TSettings settings;
  QString dir=settings.getXmlValue("common/database_folder","",qApp->applicationDirPath()+"/dbase").toString();

  QString fileName=QFileDialog::getOpenFileName(this,tr("Remove Database"),dir,tr("Database files (*.sqt)"));
  if (fileName.isEmpty())
    return;

  if (QMessageBox::question(this,tr("Confirmation"),tr("Remove Database?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::No)
    return;

  if (fileName==m_baseName)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't remove current database file.\n%1.").arg(fileName));
    return;
    }

  if (QFile::remove(fileName)==false)
    QMessageBox::critical(this,tr("Error"),tr("Can't remove database file.\n%1.").arg(fileName));

  settings.setXmlValue("common/database_folder","",QFileInfo(fileName).absolutePath());
  }
//
void TMainWnd::createOrder()
  {
  if (m_orderModel->isChanged()==true && QMessageBox::question(this,tr("Confirmation"),tr("Current Order has been changed.\nSave it before open new one?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    saveOrder();

  if (m_orderModel->readXml(":template/order.xml",m_orderModel->rootIndex()).isValid())
    orderTree->expandAll();

  m_orderName=tr("untitled");
  setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));
  adjustIface();
  }
//
void TMainWnd::openOrder()
  {
  TSettings settings;
  if (m_orderModel->isChanged()==true && QMessageBox::question(this,tr("Confirmation"),tr("Current Order has been changed.\nSave it before open new one?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    saveOrder();

  QString dirName=settings.getXmlValue("common/order_folder","","").toString();
  if (dirName.isEmpty())
    dirName=qApp->applicationDirPath()+"/orders";

  QDir dir(dirName);
  if (dir.exists()==false)
    dir.mkpath(dir.path());

  QString fileName=QFileDialog::getOpenFileName(this,tr("Order file"),dirName,tr("XML files (*.xml)"));
  if (fileName.isEmpty())
    return;

  settings.setXmlValue("common/order_folder","",QFileInfo(fileName).absolutePath());

  m_orderModel->presetAnswer(TOrderModel::AppendToAllAnswer);
  if (m_orderModel->readXml(fileName,m_orderModel->rootIndex()).isValid())
    {
    m_orderName=fileName;
    setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));
    orderTree->expandAll();
    }

  adjustIface();
  if (m_orderModel->isChanged()==true)
    QMessageBox::information(this,tr("Information"),tr("The Order has been automatically corrected\nto coply with new order format."),QMessageBox::Ok);
  }
//
void TMainWnd::saveOrder()
  {
  TSettings settings;
  QString dirName=settings.getXmlValue("common/order_folder","","").toString();
  if (dirName.isEmpty())
    dirName=qApp->applicationDirPath()+"/orders";

  QDir dir(dirName);
  if (dir.exists()==false)
    dir.mkpath(dir.path());

  QString fileName=QFileDialog::getSaveFileName(this,tr("Order file"),dirName,tr("XML files (*.xml)"));
  if (fileName.isEmpty())
    return;

  settings.setXmlValue("common/order_folder","",QFileInfo(fileName).absolutePath());
  if (m_orderModel->writeXml(fileName,m_orderModel->rootIndex()))
    {
    m_orderName=fileName;
    setWindowTitle(tr("Order [%1] , [%2]").arg(m_baseName).arg(m_orderName));
    }
  }
//
void TMainWnd::removeOrder()
  {
  TSettings settings;
  QString dir=settings.getXmlValue("common/order_folder","",qApp->applicationDirPath()+"/orders").toString();

  QString fileName=QFileDialog::getOpenFileName(this,tr("Remove Order file"),dir,tr("XML files (*.xml)"));
  if (fileName.isEmpty())
    return;

  if (QMessageBox::question(this,tr("Confirmation"),tr("Remove Order?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::No)
    return;

  if (fileName==m_orderName)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't remove current order file.\n%1.").arg(fileName));
    return;
    }

  settings.setXmlValue("common/order_folder","",QFileInfo(fileName).absolutePath());
  if (QFile::remove(fileName)==false)
    QMessageBox::critical(this,tr("Error"),tr("Can't remove order file.\n%1.").arg(fileName));
  }
//
void TMainWnd::reportRenderStarted()
  {
  m_progressDlg=new QProgressDialog(this);
  connect(m_progressDlg,&QProgressDialog::canceled,m_report,&LimeReport::ReportEngine::cancelRender);

  m_progressDlg->resize(300,m_progressDlg->height());
  m_progressDlg->setWindowTitle(tr("Report preparing"));
  m_progressDlg->setModal(true);
  m_progressDlg->setMaximum(m_count);

  m_progressDlg->show();
  qApp->processEvents();
  }
//
void TMainWnd::reportRenderFinished()
  {
  m_progressDlg->close();
  delete m_progressDlg;
  }
//
void TMainWnd::reportRenderPageFinished(int page)
  {
  m_progressDlg->setValue(m_current);
  qApp->processEvents();
  }
//
void TMainWnd::designReport()
  {
  QString report=qApp->applicationDirPath()+"/report/order.lrxml";
  QString name=tr("Order");

  if (report.isEmpty()==false)
    {
    m_report->loadFromFile(report);
    m_report->setReportName(name+" ["+report+"]");
    }
  else
    {
    m_report->loadFromByteArray(nullptr);
    m_report->setReportName(QString());
    }

  m_report->designReport();
  }
//
bool TMainWnd::prepareReport()
  {
  m_current=0;
  m_count=0;
  m_orderData.clear();

  QModelIndex index=orderTree->currentIndex();
  QString name=m_orderModel->index(index.row(),TOrderModel::Name,index.parent()).data().toString();

  QString report=qApp->applicationDirPath()+"/report/order.lrxml";
  if (m_report->loadFromFile(report)==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Can't open report template file\n%1.").arg(report),QMessageBox::Ok);
    return false;
    }

  m_report->setReportName(tr("Order")+" ["+report+"]");
  // готовим весь приказ
  if (name=="order")
    {
    m_report->dataManager()->setReportVariable("fullOrder",true);
    walkTree(m_orderModel->rootIndex(),-1);
    return true;
    }
  // готовим только часть, какой-то раздел
  m_report->dataManager()->setReportVariable("fullOrder",false);
  walkTree(m_orderModel->rootIndex(),index.row());
  return true;
  }
//
void TMainWnd::previewReport()
  {
  // проверим, что приказ не пустой
  QModelIndex root=m_orderModel->index(0,TOrderModel::Title,QModelIndex());
  if (m_orderModel->rowCount(root)<1 || prepareReport()==false)
    return;

  m_report->setPreviewWindowTitle(tr("Preview - %1").arg(m_report->reportName()));
  m_report->previewReport();
  }
//
void TMainWnd::printReport()
  {
  // проверим, что приказ не пустой
  QModelIndex root=m_orderModel->index(0,TOrderModel::Title,QModelIndex());
  if (m_orderModel->rowCount(root)<1 || prepareReport()==false)
    return;

  m_report->printReport();
  }
//
void TMainWnd::printReportToPdf()
  {
  // проверим, что приказ не пустой
  QModelIndex root=m_orderModel->index(0,TOrderModel::Title,QModelIndex());
  if (m_orderModel->rowCount(root)<1 || prepareReport()==false)
    return;

  QString pdfName=QFileDialog::getSaveFileName(this,tr("Save to PDF"),qApp->applicationDirPath(),tr("PDF files (*.pdf)"));
  if (pdfName.isEmpty())
    return;

  m_report->printToPDF(pdfName);
  }
//
void TMainWnd::specialitySelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
  {
  Q_UNUSED(selected)
  Q_UNUSED(deselected)

  QItemSelectionModel *selectionModel=qobject_cast <QItemSelectionModel*>(sender());
  QModelIndexList indexes=selectionModel->selectedRows();
  if (indexes.size()==0)
    {
    m_profileModel->setFilter("");
    return;
    }

  quint16 id=m_specialityModel->index(indexes.at(0).row(),m_specialityModel->fieldIndex("id")).data().toInt();
  m_profileModel->setFilter("speciality_id="+QString::number(id));
  }
//
void TMainWnd::tableContextMenuRequested(const QPoint &point)
  {
  TTableView *tableView=qobject_cast <TTableView*>(sender());
  TSqlTableModel *model=qobject_cast <TSqlTableModel*> (tableView->model());
  if (tableView==nullptr || model==nullptr)
    return;

  QMenu menu;
  menu.addAction(m_toOrderActn);
  menu.addSeparator();
  menu.addAction(m_viewActn);

  QModelIndex sourceIndex=tableView->indexAt(point);
  if (sourceIndex.isValid()==false)
    {
    m_toOrderActn->setEnabled(false);
    m_viewActn->setEnabled(false);
    menu.exec(QCursor::pos());
    return;
    }

  // можно ли добавлять в m_orderModel currentIndex() данные именно из этой таблицы
  QModelIndex targetIndex=orderTree->currentIndex();
  QString source=model->nodeType();
  TOrderModel::AppendResponse response=m_orderModel->appendRequest(targetIndex,source);
  // запомнить в Action, из какой вьюхи был вызван
  m_toOrderActn->setData((quintptr)tableView);
  m_toOrderActn->setEnabled(response.index.isValid());
  m_viewActn->setEnabled(true);
  m_viewActn->setData((quintptr)tableView);

  menu.exec(QCursor::pos());
  }
//
void TMainWnd::orderContextMenuRequested(const QPoint &point)
  {
  QMenu menu;
  menu.addAction(m_emptyToOrderActn);
  menu.addAction(m_fromOrderActn);
  menu.addSeparator();
  menu.addAction(m_editActn);
  menu.addSeparator();
  menu.addAction(m_exportBranchActn);
  menu.addAction(m_importBranchActn);
  menu.addSeparator();
  menu.addAction(m_expandAllActn);
  menu.addAction(m_collapseAllActn);

  QModelIndex targetIndex=orderTree->indexAt(point);
  if (targetIndex.isValid()==false)
    {
    m_emptyToOrderActn->setEnabled(false);
    m_fromOrderActn->setEnabled(false);
    m_editActn->setEnabled(false);
    m_exportBranchActn->setEnabled(false);
    menu.exec(QCursor::pos());
    return;
    }

  TOrderModel::AppendResponse response=m_orderModel->appendRequest(targetIndex,QString());
  m_emptyToOrderActn->setEnabled(response.index.isValid());
  quint8 permanent=m_orderModel->index(targetIndex.row(),TOrderModel::Permanent,targetIndex.parent()).data().toInt();
  m_fromOrderActn->setEnabled(permanent==0);

  m_editActn->setEnabled(true);
  m_exportBranchActn->setEnabled(true);

  menu.exec(QCursor::pos());
  }
//
void TMainWnd::getOrderCallbackData(const LimeReport::CallbackInfo &info, QVariant &value)
  {
  switch (info.dataType)
    {
    case LimeReport::CallbackInfo::IsEmpty:
      {
      value=false;
      break;
      }
    case LimeReport::CallbackInfo::HasNext:
      {
      value=(m_current<m_count);
      break;
      }
    case LimeReport::CallbackInfo::RowCount:
      {
      value=m_count;
      break;
      }
    case LimeReport::CallbackInfo::ColumnCount:
      {
      value=m_orderModel->columnCount();
      break;
      }
    case LimeReport::CallbackInfo::ColumnHeaderData:
      {
      value=m_orderModel->headerData(info.index,Qt::Horizontal,Qt::UserRole);
      break;
      }
    case LimeReport::CallbackInfo::ColumnData:
      {
      value=m_orderData[m_current].at(m_orderModel->columnPosition(info.columnName)).toString();
      break;
      }
    }
  }
//
void TMainWnd::changeOrderPos(const LimeReport::CallbackInfo::ChangePosType &type, bool &result)
  {
  switch (type)
    {
    case LimeReport::CallbackInfo::First:
      {
      result=(m_current==0);
      break;
      }

    case LimeReport::CallbackInfo::Next:
      {
      result=(m_current<m_count);
      m_current++;
      break;
      }
    }
  }
//
void TMainWnd::walkTree(const QModelIndex &index, int row)
  {
  if (m_orderModel->index(index.row(),TOrderModel::Name,index.parent()).data().toString()!=QString("speciality") || row==-1 || row==index.row())
    {
    QVector <QVariant> nodeData{m_orderModel->index(index.row(),TOrderModel::Title,index.parent()).data(),
                                m_orderModel->index(index.row(),TOrderModel::Description,index.parent()).data(),
                                m_orderModel->index(index.row(),TOrderModel::Name,index.parent()).data(),
                                m_orderModel->index(index.row(),TOrderModel::Size,index.parent()).data(),
                                m_orderModel->index(index.row(),TOrderModel::Permanent,index.parent()).data()};
    m_orderData<<nodeData;
    m_count++;
    }

  for (int i=0;i<m_orderModel->rowCount(index);i++)
    {
    if (m_orderModel->index(i,TOrderModel::Name,index).data()=="speciality" && row!=-1 && row!=i)
      continue;
    walkTree(m_orderModel->index(i,TOrderModel::Name,index),row);
    }
  }
//
void TMainWnd::tableActivated(const QModelIndex &index)
  {
  if (index.isValid()==false)
    return;

  TTableView *tableView=qobject_cast <TTableView*>(sender());
  TSqlTableModel *model=qobject_cast<TSqlTableModel*>(tableView->model());
  if (tableView==nullptr || model==nullptr)
    return;

  appendToOrder(model,index);
  orderTree->setFocus();
  }
//
void TMainWnd::appendToOrder()
  {
  QAction *action=qobject_cast <QAction*>(sender());
  TTableView *tableView=(TTableView*)(action->data().value<quintptr>());
  TSqlTableModel *model=qobject_cast<TSqlTableModel*>(tableView->model());
  if (tableView==nullptr || model==nullptr)
    return;

  QModelIndexList indexes=tableView->selectionModel()->selectedRows();
  std::sort(indexes.begin(),indexes.end(),[this](const QModelIndex &first, const QModelIndex &second) {
    return first.row() < second.row();
    });
  // сбросим "ответ" диалога на "нет ответа"
  m_orderModel->presetAnswer(TOrderModel::NoAnswer);
  foreach (const QModelIndex &index, indexes)
    {
    if (appendToOrder(model,index)==false)
      break;
    }
    orderTree->setFocus();
  }
//
bool TMainWnd::appendToOrder(TSqlTableModel *model, const QModelIndex &index)
  {
  if (model==nullptr || index.isValid()==false)
    return false;

  quint8 titleIndex=model->fieldIndex(model->headerField());
  quint8 descriptionIndex=model->fieldIndex(model->descriptionField());

  QString title=model->index(index.row(),titleIndex).data().toString();
  QString description=model->index(index.row(),descriptionIndex).data().toString();

  QModelIndex targetIndex=orderTree->currentIndex();
  QString targetTitle=m_orderModel->index(targetIndex.row(),TOrderModel::Title,targetIndex.parent()).data().toString();

  QString source=model->nodeType();
  TOrderModel::AppendResponse response=m_orderModel->appendRequest(targetIndex,source);
  if (response.index.isValid()==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("Inpossible to append record\n'%1'\nto section\n'%2'.").arg(title,targetTitle),QMessageBox::Ok);
    return false;
    }

  QString name=m_orderModel->index(response.index.row(),TOrderModel::Name,response.index.parent()).data().toString();
  TNodeInfo nodeInfo=m_orderModel->nodeInfo(name);
  // TODO: в data последний параметр - permanent, может его достать из реальной инфы о ноде? хотя, оно и будет равно 0
  QVector <QVariant> data={title, description, source, nodeInfo.size(), 0};
  // поискать такую запись
  QPair <bool,qint16> result=m_orderModel->findMatch(response.index,data,response.row);
  if (result.second==INT16_MIN)
    return true;
  // меняем найденную
  if (result.first==true)
    {
    setOrderData(result.second,targetIndex,data);
    return true;
    }
  // вставляем новую
  m_orderModel->insertRow(response.row,response.index);
  // если вставили новую запись ПЕРЕД текущей, то row новой записи это row текущей, а row текущей увеличился на 1
  quint16 row=(result.second==-1 ? m_orderModel->rowCount(response.index)-1 : targetIndex.row());
  setOrderData(row,response.index,data);
  return true;
  }
//
void TMainWnd::appendEmptyToOrder()
  {
  QModelIndex targetIndex=orderTree->currentIndex();
  TOrderModel::AppendResponse response=m_orderModel->appendRequest(targetIndex,QString());
  if (response.index.isValid()==false)
    return;

  m_orderModel->insertRow(response.row,response.index);

  TNodeInfo nodeInfo=m_orderModel->nodeInfo(response.name);
  quint16 row=(response.relation==TNodeInfo::ChildRelation ? m_orderModel->rowCount(response.index)-1 : response.index.row()-1);
  QVector <QVariant> data={nodeInfo.title(),nodeInfo.description(),response.name,nodeInfo.size(),nodeInfo.permanent()};

  setOrderData(row,response.index,data);
  orderTree->setFocus();
  }
//
void TMainWnd::setOrderData(quint16 position, const QModelIndex &parent, const QVector<QVariant> &data)
  {
  m_orderModel->setData(m_orderModel->index(position,TOrderModel::Title,parent),data.at(TOrderModel::Title).toString());
  m_orderModel->setData(m_orderModel->index(position,TOrderModel::Description,parent),data.at(TOrderModel::Description).toString());
  m_orderModel->setData(m_orderModel->index(position,TOrderModel::Name,parent),data.at(TOrderModel::Name).toString());
  m_orderModel->setData(m_orderModel->index(position,TOrderModel::Size,parent),data.at(TOrderModel::Size).toInt());
  m_orderModel->setData(m_orderModel->index(position,TOrderModel::Permanent,parent),data.at(TOrderModel::Permanent).toInt());

  QModelIndex targetIndex=m_orderModel->index(position,TOrderModel::Name,parent);
  QString fromName=data.at(TOrderModel::Name).toString();

  m_orderModel->readXml(":template/order.xml",targetIndex,fromName);
  moveToIndex(m_orderModel->index(position,TOrderModel::Title,parent));
  }
//
void TMainWnd::treeDoubleClicked(const QModelIndex &index)
  {
  if (index.isValid()==false || index.row()!=TOrderModel::Title)
    return;

  removeFromOrder();
  }
//
void TMainWnd::removeFromOrder()
  {
  QModelIndex itemIndex=orderTree->selectionModel()->currentIndex();
  if (itemIndex.isValid()==false || m_orderModel->index(itemIndex.row(),TOrderModel::Permanent,itemIndex.parent()).data().toBool()==1)
    return;

  QString title=m_orderModel->fullPath(itemIndex).join(" -> ");
  if (QMessageBox::question(this,tr("Confirmation"),tr("Remove the following Item from Order?\n'%1'").arg(title),QMessageBox::Yes | QMessageBox::No)==QMessageBox::No)
    return;

  m_orderModel->removeRow(itemIndex.row(),itemIndex.parent());
  adjustIface();
  }
//
void TMainWnd::aboutShowInWindow()
  {
  QAction *action=qobject_cast <QAction*>(sender());
  TTableView *tableView=(TTableView*)(action->data().value<quintptr>());
  if (tableView==nullptr)
    return;

  viewInWindow(tableView->currentIndex());
  }
//
void TMainWnd::viewInWindow(const QModelIndex &index)
  {
  if (index.isValid()==false)
    return;
  // данные в самом index есть
  TEditDlg *editDlg=new TEditDlg(this,index.data().toString(),true);
  editDlg->setWindowTitle(tr("Show text"));
  editDlg->exec();

  delete editDlg;
  }
//
void TMainWnd::editInWindow()
  {
  QModelIndex index=orderTree->selectionModel()->currentIndex();
  if (index.isValid()==false)
    return;
  // данные в самом index есть
  TEditDlg *editDlg=new TEditDlg(this,index.data().toString(),false);
  editDlg->setWindowTitle(tr("Edit text"));
  if (editDlg->exec()==QDialog::Accepted)
    m_orderModel->setData(index,editDlg->text());

  delete editDlg;
  }
//
void TMainWnd::expandAll()
  {
  QModelIndex index=orderTree->selectionModel()->currentIndex();
  orderTree->expandAll();
  if (index.isValid())
    orderTree->scrollTo(index,QAbstractItemView::PositionAtCenter);
  }
//
void TMainWnd::collapseAll()
  {
  orderTree->collapseAll();
  if (m_orderModel->rowCount()>0)
    {
    for (quint8 i=0;i<m_orderModel->columnCount();i++)
      orderTree->selectionModel()->select(m_orderModel->index(0,i,m_orderModel->index(0,TOrderModel::Title)),QItemSelectionModel::Select);
    orderTree->scrollTo(m_orderModel->index(0,TOrderModel::Title),QAbstractItemView::PositionAtCenter);
    }
  }
//
void TMainWnd::exportBranch()
  {
  TSettings settings;
  QModelIndex itemIndex=orderTree->selectionModel()->currentIndex();
  if (itemIndex.isValid()==false)
    return;

  QString dirName=settings.getXmlValue("common/order_folder","","").toString();
  if (dirName.isEmpty())
    dirName=qApp->applicationDirPath()+"/orders";

  QDir dir(dirName);
  if (dir.exists()==false)
    dir.mkpath(dir.path());

  QString fileName=QFileDialog::getSaveFileName(this,tr("Order file to Export"),dirName,tr("XML files (*.xml)"));
  if (fileName.isEmpty())
    return;

  settings.setXmlValue("common/order_folder","",QFileInfo(fileName).absolutePath());
  m_orderModel->writeXml(fileName,itemIndex);
  }
//
void TMainWnd::importBranch()
  {
  TSettings settings;
  QModelIndex itemIndex=orderTree->selectionModel()->currentIndex();
  if (itemIndex.isValid()==false)
    return;

  QString dirName=settings.getXmlValue("common/order_folder","","").toString();
  if (dirName.isEmpty())
    dirName=qApp->applicationDirPath()+"/orders";

  QDir dir(dirName);
  if (dir.exists()==false)
    dir.mkpath(dir.path());

  QString fileName=QFileDialog::getOpenFileName(this,tr("Order file to Import"),dirName,tr("XML files (*.xml)"));
  if (fileName.isEmpty())
    return;

  settings.setXmlValue("common/order_folder","",QFileInfo(fileName).absolutePath());

  m_orderModel->presetAnswer(TOrderModel::NoAnswer);
  QModelIndex actualIndex=m_orderModel->readXml(fileName,itemIndex);
  if (actualIndex.isValid()==false)
    {
    QMessageBox::critical(this,tr("Error"),tr("You can't import this branch here."),QMessageBox::Ok);
    return;
    }

  moveToIndex(actualIndex);
  }
//
void TMainWnd::orderSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
  {
  adjustIface();
  }
//
void TMainWnd::moveToIndex(const QModelIndex &index)
  {
  if (index.isValid()==false)
    return;

  orderTree->expand(index);
  orderTree->setCurrentIndex(index);
  orderTree->scrollTo(index,QAbstractItemView::PositionAtCenter);
  }
//
void TMainWnd::sortDictionary(bool checked)
  {
  QCheckBox *checkBox=qobject_cast<QCheckBox*>(sender());
  if (checkBox==nullptr)
    return;

  TTableView *tableView=m_sortMap.value(checkBox);
  if (tableView==nullptr)
    return;

  TSqlTableModel *tableModel=qobject_cast<TSqlTableModel*>(tableView->model());
  quint16 fieldIndex=tableModel->fieldIndex("id");
  if (checked==true)
    {
    QString fieldName=tableModel->headerField();
    fieldIndex=tableModel->fieldIndex(fieldName);
    foreach (const TSqlRelation &relation, tableModel->relations())
      {
      if (fieldName!=relation.visibleField())
        continue;

      fieldName=relation.field();
      fieldIndex=tableModel->fieldIndex(fieldName);
      break;
      }
    }

  QVariant value=tableModel->index(tableView->currentIndex().row(),fieldIndex).data();
  tableModel->sort(fieldIndex,Qt::AscendingOrder);

  if (value.isValid()==false)
    return;

  QModelIndexList indexes=tableModel->match(tableModel->index(0,fieldIndex),Qt::DisplayRole,value,1,Qt::MatchExactly);
  if (indexes.size()==0)
    return;

  QModelIndex index=indexes.first();
  tableView->verticalHeader()->reset();
  tableView->setFocus();
  tableView->selectRow(index.row());
  tableView->setCurrentIndex(index);
  tableView->scrollTo(index,QAbstractItemView::EnsureVisible);
  }

void TMainWnd::dockVisibilityChanged()
  {
  viewSpecialityActn->setChecked(specialityDock->isVisible());
  viewProfileActn->setChecked(profileDock->isVisible());
  viewPersonActn->setChecked(personDock->isVisible());
  viewAttestationActn->setChecked(attestationDock->isVisible());
  }
  //
QNetworkRequest TMainWnd::createRequest(const QString &fileName)
  {
  QAESEncryption encryption(QAESEncryption::AES_256, QAESEncryption::ECB);
  QByteArray message=QByteArrayLiteral("TSPU_Order");
  message=QCryptographicHash::hash(message, QCryptographicHash::Sha256);

  TSettings settings;

  QString remotePath=settings.getXmlValue(QStringLiteral("updates/remote_path"),"","").toString();
  QByteArray answer=QByteArray::fromHex(remotePath.toUtf8());
  answer=encryption.decode(answer, message);
  answer=encryption.removePadding(answer);
  remotePath=QString::fromUtf8(answer)+QString("/%1/raw").arg(fileName);

  QString token=settings.getXmlValue(QStringLiteral("updates/token"),"","").toString();
  answer=QByteArray::fromHex(token.toUtf8());
  answer=encryption.decode(answer, message);
  answer=encryption.removePadding(answer);
  token=QString::fromUtf8(answer);

  QUrlQuery query;
  query.addQueryItem("ref","master");

  QUrl url(remotePath);
  url.setQuery(query);

  QNetworkRequest request(url);
  request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::AlwaysNetwork);
  request.setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute,true);
  request.setAttribute(QNetworkRequest::User,fileName); // потом также достанем, при получении
  request.setRawHeader("Private-Token",token.toUtf8());

  return request;
  }
//
void TMainWnd::checkUpdate()
  {
  TSettings settings;
  QString fileName=settings.getXmlValue("common/update_path","","none").toString();
  if (QFile::exists(fileName)) // не надо щас апдейтиться или новый .msi уже есть
    {
    mainStatus->showMessage(tr("New 'Order' version is ready to install. It's will be installed when exiting the program."));
    QString message=tr("New 'Order' version is ready to install.\nIt's will be installed when exiting the program.\nExit program now?");
    if (QMessageBox::question(this,tr("Confirmation"),message,QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
      qApp->quit();
    return;
    }
  settings.removeXmlEntry("common/update_path");

  QNetworkRequest request=createRequest("version");
  QNetworkReply *reply=m_manager->get(request);
  connect(reply,&QNetworkReply::finished,this,&TMainWnd::checkVersion);
  }
//
void TMainWnd::checkVersion()
  {
  QNetworkReply *versionReply=qobject_cast<QNetworkReply*>(sender());
  QNetworkReply::NetworkError error=versionReply->error();
  QString errorString=versionReply->errorString();
  if (versionReply==nullptr || error!=QNetworkReply::NoError)
    {
    mainStatus->showMessage(tr("Update error:\n%1.").arg(versionReply->errorString()),3000);
     QList<QNetworkReply::NetworkError> accessErrors{QNetworkReply::ProtocolInvalidOperationError, QNetworkReply::AuthenticationRequiredError,
                                                    QNetworkReply::SslHandshakeFailedError, QNetworkReply::ContentAccessDenied};
    if (accessErrors.contains(error)==false)
      {
      QMessageBox::critical(this,tr("Error"),tr("Update error:\n%1.").arg(errorString),QMessageBox::Ok);
      return;
      }

    QString token=QInputDialog::getText(this,tr("Error"),tr("Update error. Invalid or expired access token.\n"
                                                            "If you have valid access token, enter it below."));
    if (token.isEmpty()==true)
      return;

    TSettings().setXmlValue(QStringLiteral("updates/token"),"",token);
    QTimer::singleShot(300,this,&TMainWnd::checkUpdate);
    return;
    }

  QVersionNumber remoteVersion=QVersionNumber::fromString(versionReply->readAll().simplified());
  QVersionNumber currentVersion=QVersionNumber::fromString(ORDER_VERSION);
  qDebug()<<"current version:"<<currentVersion<<"remote version:"<<remoteVersion;

  if (currentVersion>=remoteVersion) // или если просто currentVersion != remoteVersion ?
    {
    mainStatus->showMessage(tr("Actual 'Order' version installed."));
    QMessageBox::information(this,tr("Information"),tr("Actual 'Order' version installed."),QMessageBox::Ok);
    clearInstallers();
    return;
    }

  clearInstallers();
  // теперь просим сам файл .msi
  QString bitness=sizeof(quintptr)==8 ? "x64" : "x86";
  QString fileName=QString("order_%1_%2.msi").arg(bitness,remoteVersion.toString());
  QString localPath=QStandardPaths::writableLocation(QStandardPaths::TempLocation)+"/"+fileName;
  if (QFile::exists(localPath)) // там уже лежит этот файл
    {
    TSettings().setXmlValue("common/update_path","",localPath);
    mainStatus->showMessage(tr("New 'Order' version is ready to install. It's will be installed when exiting the program."));
    QString message=tr("New 'Order' version is ready to install.\nIt's will be installed when exiting the program.\nExit program now?");
    if (QMessageBox::question(this,tr("Confirmation"),message,QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
      qApp->quit();
    return;
    }

  m_progressDlg=new QProgressDialog(this);
  connect(m_progressDlg,&QProgressDialog::canceled,m_report,&LimeReport::ReportEngine::cancelRender);

  m_progressDlg->setModal(true);
  m_progressDlg->setRange(0,0);
  m_progressDlg->setWindowTitle(tr("Downloading progress"));
  m_progressDlg->setLabelText(tr("Downloaded %1 percents.").arg(0));
  m_progressDlg->resize(300,m_progressDlg->height());
  m_progressDlg->show();
  qApp->processEvents();

  mainStatus->showMessage(tr("New 'Order' version download process: %1 % completed.").arg(0));
  QNetworkRequest request=createRequest(fileName);
  QNetworkReply *reply=m_manager->get(request);
  connect(reply,&QNetworkReply::downloadProgress,this,&TMainWnd::updateProgress);
  connect(reply,&QNetworkReply::finished,this,&TMainWnd::updateFinished);
  }
//
void TMainWnd::updateProgress(qint64 bytesReceived, qint64 bytesTotal)
  {
  quint8 percent=qRound(double(bytesReceived)/bytesTotal*100);
  m_progressDlg->setRange(0,bytesTotal);
  m_progressDlg->setValue(bytesReceived);
  m_progressDlg->setLabelText(tr("Downloaded %1 percents.").arg(percent));

  mainStatus->showMessage(tr("New 'Order' version download process: %1 % completed.").arg(percent));
  }
//
void TMainWnd::updateFinished()
  {
  if (m_progressDlg==nullptr)
    return;
  m_progressDlg->close();
  delete m_progressDlg;
  m_progressDlg=nullptr;

  QNetworkReply *fileReply=qobject_cast<QNetworkReply*>(sender());
  if (fileReply==nullptr || fileReply->error()!=QNetworkReply::NoError)
    {
    mainStatus->showMessage(tr("Update error:\n%1.").arg(fileReply->errorString()),3000);
    QMessageBox::critical(this,tr("Error"),tr("Update error:\n%1.").arg(fileReply->errorString()),QMessageBox::Ok);
    return;
    }

  QString tempPath=QStandardPaths::writableLocation(QStandardPaths::TempLocation);
  QString localPath=tempPath+"/"+fileReply->request().attribute(QNetworkRequest::User).toString();
  QByteArray data=fileReply->readAll();

  QFile file(localPath);
  if (file.open(QIODevice::WriteOnly | QIODevice::Truncate)==false)
    {
    mainStatus->showMessage(tr("Update error:\n%1.").arg(tr("Can't save downloaded file")),3000);
    QMessageBox::critical(this,tr("Error"),tr("Update error:\n%1.").arg(tr("Can't save downloaded file")),QMessageBox::Ok);
    return;
    }

  file.write(data);
  file.close();
  mainStatus->showMessage(tr("New 'Order' version download completed."),3000);

  TSettings().setXmlValue("common/update_path","",QDir::toNativeSeparators(localPath));
  mainStatus->showMessage(tr("New 'Order' version is ready to install. It's will be installed when exiting the program."));
  QString message=tr("New 'Order' version is ready to install.\nIt's will be installed when exiting the program.\nExit program now?");
  if (QMessageBox::question(this,tr("Confirmation"),message,QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    qApp->quit();
  }
//
void TMainWnd::clearInstallers()
  {
  // удалить все наши .msi там, где могут быть
  QStringList paths;
  paths << QStandardPaths::writableLocation(QStandardPaths::TempLocation) << QStandardPaths::writableLocation(QStandardPaths::DownloadLocation)
        << QStringLiteral("c:/distrib");

  QDir directory;
  directory.setNameFilters(QStringList()<<"order_*.msi");
  directory.setFilter(QDir::Files);
  foreach (const QString &path,paths)
    {
    directory.setPath(path);
    foreach (const QString &installer, directory.entryList())
      directory.remove(installer);
    }
  }
//
