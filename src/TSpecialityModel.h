#pragma once

#include "TSqlTableModel.h"

class TSpecialityModel : public TSqlTableModel
  {
    Q_OBJECT
  public:
    TSpecialityModel(QObject *parent=nullptr, const QSqlDatabase &dataBase=QSqlDatabase());
    void setHeaderNames();

  protected:
    QString selectStatement() const;
  };
