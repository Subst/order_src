#pragma once

#include <QStyledItemDelegate>

class TOrderDelegate : public QStyledItemDelegate
  {
    Q_OBJECT
  public:
    explicit TOrderDelegate(QObject *parent=nullptr);

  protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    QStyleOptionViewItem modifyOption(const QStyleOptionViewItem &option, const QString &name) const;
  };
