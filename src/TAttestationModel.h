#pragma once

#include "TSqlTableModel.h"

class TAttestationModel : public TSqlTableModel
  {
    Q_OBJECT
  public:
    TAttestationModel(QObject *parent=nullptr, const QSqlDatabase &dataBase=QSqlDatabase());
    void setHeaderNames();

  protected:
    QString selectStatement() const;
  };
