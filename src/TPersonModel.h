#pragma once

#include "TSqlTableModel.h"

class TPersonModel : public TSqlTableModel
  {
    Q_OBJECT
  public:
    TPersonModel(QObject *parent=nullptr, const QSqlDatabase &dataBase=QSqlDatabase());
    void setHeaderNames();

  protected:
    QString selectStatement() const;
  };
