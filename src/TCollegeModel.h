#pragma once

#include "TSqlTableModel.h"

class TCollegeModel : public TSqlTableModel
  {
    Q_OBJECT
  public:
    TCollegeModel(QObject *parent=nullptr, const QSqlDatabase &dataBase=QSqlDatabase());
    void setHeaderNames();

  protected:
    QString selectStatement() const;
  };
