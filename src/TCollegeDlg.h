#pragma once

#include "ui_TCollegeDlg.h"

class TCollegeModel;

class TCollegeDlg : public QDialog, private Ui::TCollegeInfoDlg
  {
    Q_OBJECT

  public:
    explicit TCollegeDlg(QWidget *parent=nullptr);

  protected:
    void initCore();
    void initIface();
    void readSettings();
    void writeSettings();

  private:
    TCollegeModel *m_collegeModel;

  public slots:
    void accept();
    void reject();
  };
