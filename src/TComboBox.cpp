#include "TComboBox.h"
#include "TSqlTableModel.h"

#include <QAbstractItemView>
#include <QFontMetrics>
#include <QDebug>
#include <QScrollBar>
#include <QKeyEvent>

#include <QSqlRecord>

TComboBox::TComboBox(QWidget *parent) : QComboBox(parent)
  {
  //lineEdit()->setReadOnly(true);
  }
//
void TComboBox::showPopup()
  {
  QComboBox::showPopup();
  int maxWidth=0;
  int width;

  QFontMetrics metrics(font());

  for (int i=0;i<count();i++)
    {
    width=metrics.horizontalAdvance(itemText(i));
    if (width>maxWidth)
      maxWidth=width;
    }
  QFrame *popupFrame=dynamic_cast<QFrame*>(view()->parent());
  QRect frameRect=popupFrame->geometry();
  if (frameRect.width()<maxWidth+view()->horizontalScrollBar()->height())
    frameRect.setWidth(maxWidth+view()->horizontalScrollBar()->height());

  popupFrame->setGeometry(frameRect);
  }
//
void TComboBox::keyReleaseEvent(QKeyEvent *event)
  {
  if (event->key()==Qt::Key_Escape)
    setCurrentIndex(-1);
  else
    QComboBox::keyReleaseEvent(event);
  }
//
void TComboBox::setModel(TSqlTableModel *model, const QStringList &valueFields, const QString &indexField, const QString &filter)
  {
  clear();
  if (filter.isEmpty()==false)
    model->setFilter(filter);

  for (int i=0;i<model->rowCount();i++)
    {
    QSqlRecord record=model->record(i);

    QStringList value;
    for (int i=0;i<valueFields.size();i++)
      value << record.value(valueFields.at(i)).toString();
    addItem(value.join(": "),record.value(indexField));
    }
  model->setFilter(QString());
  }
