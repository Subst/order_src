#pragma once

#include <QAbstractItemModel>

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

/*
 * инфа заполняется из шаблона (файл в ресурсах template/order.xml),
 * size - максимальное кол-во дочерних элементов;
 * permanent - если true, то всегда есть, нельзя удалять;
 * relatives - возможные "родственники",в какие ноды и как именно можно принципиально добавить запись из этого нода,
 * в работе поможет, например, чтобы девочки не добавили в Вид аттестации какую-то Персону и т.д.
 * по месту еще будет проверка: например, сколько уже есть дочерних нодов и сколько можно всего (size)
 */
class TNodeInfo
  {
    Q_GADGET
  public:
    enum Relation
      {
      InvalidRelation=0,
      ChildRelation,
      PeerRelation
      };
    Q_ENUM(Relation)

    TNodeInfo(const QString &title=QString(), const QString &description=QString(), qint16 size=-1, qint8 permanent=-1);

    void appendRelative(const QString &name, TNodeInfo::Relation relation);
    bool isValid();
    QString title() const ;
    QString description() const;
    quint16 size();
    quint8 permanent();
    QMap <QString, TNodeInfo::Relation> relatives() const;

  private:
    QString m_title;
    QString m_description;
    qint16 m_size;
    qint8 m_permanent;
    QMap <QString, TNodeInfo::Relation> m_relatives;
  };

class TOrderModel : public QAbstractItemModel
  {
    Q_OBJECT
  public:
    struct ItemInfo
      {
        ItemInfo(): parent(nullptr),children(QVector <struct ItemInfo*>()),data(QVector <QVariant>()){}
        struct ItemInfo *parent;
        QVector <struct ItemInfo*> children;
        QVector <QVariant> data;
      };

    enum Column
      {
      Title,
      Description,
      Name,
      Size,
      Permanent
      };
    Q_ENUM(Column)

    // ответ на запрос о добавлении записи: куда, в какую строку, под каким именем (если не знаем сами)
    struct AppendResponse
      {
        AppendResponse(const QModelIndex &index=QModelIndex(), qint16 row=INT16_MIN, const QString &name=QString(),
                       TNodeInfo::Relation relation=TNodeInfo::InvalidRelation)
          {
          this->index=index;
          this->row=row;
          this->name=name;
          this->relation=relation;
          }
        QModelIndex index;
        qint16 row;
        QString name;
        TNodeInfo::Relation relation;
      };

    enum Answer
      {
      NoAnswer=-1,
      ReplaceToAllAnswer=0,
      AppendToAllAnswer,
      SkipToAllAnswer
      };
    Q_ENUM(Answer)

    explicit TOrderModel(QObject *parent=nullptr);
    ~TOrderModel();

    QVariant data(const QModelIndex &index,int role=Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index,const QVariant &value,int role=Qt::EditRole) override;

    QVariant headerData(int section,Qt::Orientation orientation,int role = Qt::DisplayRole) const override;
    bool setHeaderData(int section,Qt::Orientation orientation,const QVariant &value,int role=Qt::EditRole) override;

    QModelIndex index(int row,int column,const QModelIndex &parent=QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent=QModelIndex()) const override;
    int columnCount(const QModelIndex &parent=QModelIndex()) const override;

    int columnPosition(const QString &name);

    Qt::ItemFlags flags(const QModelIndex &index) const override;

    bool insertColumns(int position,int columns,const QModelIndex &parent=QModelIndex()) override;
    bool removeColumns(int position,int columns,const QModelIndex &parent=QModelIndex()) override;

    bool insertRows(int position,int rows,const QModelIndex &parent=QModelIndex()) override;
    bool removeRows(int position,int rows,const QModelIndex &parent = QModelIndex()) override;

    QModelIndex readXml(const QString &fileName, QModelIndex target, const QString &fromName=QString());
    bool writeXml(const QString &fileName, const QModelIndex &source);

    void clear();
    bool isChanged();

    TOrderModel::ItemInfo *root();
    QModelIndex rootIndex() const;

    TNodeInfo nodeInfo(const QString &name) const;
    TOrderModel::AppendResponse appendRequest(const QModelIndex &targetIndex, const QString source);

    QStringList fullPath(QModelIndex current, QStringList path=QStringList());
    QPair <bool, qint16> findMatch(const QModelIndex &parent, const QVector<QVariant> &data, qint16 row);

    void presetAnswer(Answer answer);

  protected:
    QModelIndex readTree(QXmlStreamReader*, QXmlStreamReader::TokenType, QString, QModelIndex parent=QModelIndex(),int row=-1);
    void writeTree(QXmlStreamWriter *stream, const QModelIndex &source);

  private:
    ItemInfo m_root;
    QModelIndex m_rootIndex;

    bool m_changed;
    bool m_corrected;

    QVector <QString> m_headers;
    QVector <QString> m_fields;

    QHash <QString, TNodeInfo> m_nodeInfoMap;

    //QHash <QString,QHash <QString, IndexType>> m_allowType;

    Answer m_answer;

  public slots:
    void dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles=QVector<int>());
  };
