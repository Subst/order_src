#include "TPersonModel.h"

#include <QSqlRecord>

TPersonModel::TPersonModel(QObject *parent, const QSqlDatabase &dataBase) : TSqlTableModel(parent, dataBase)
  {
  setTable("persons");
  setTitle(tr("Persons"));

  setHeaderField("person");
  setDescriptionField("description");
  setNodeType("person");

  setSort(fieldIndex("id"),Qt::AscendingOrder);
  setEditStrategy(QSqlTableModel::OnFieldChange);
  }
//
void TPersonModel::setHeaderNames()
  {
  setHeaderData(fieldIndex("id"),Qt::Horizontal,tr("ID"),Qt::DisplayRole);
  setHeaderData(fieldIndex("person"),Qt::Horizontal,tr("Person"),Qt::DisplayRole);
  setHeaderData(fieldIndex("description"),Qt::Horizontal,tr("Description"),Qt::DisplayRole);

  for (int i=0;i<record().count();i++)
    setHeaderData(i,Qt::Horizontal,record().fieldName(i),Qt::UserRole);
  }
//
QString TPersonModel::selectStatement() const
  {
  QString statement="select persons.id, persons.person, persons.description from persons";

  if (filter().isEmpty()==false)
    statement.append(" where "+filter());

  statement.append(" "+orderByClause());
  return statement;
  }
//
