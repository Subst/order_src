#include "TOrderModel.h"

#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QApplication>
#include <QCheckBox>

/* TNodeInfo */
TNodeInfo::TNodeInfo(const QString &title, const QString &description, qint16 size, qint8 permanent) :
                     m_title(title), m_description(description), m_size(size), m_permanent(permanent), m_relatives(QMap<QString,TNodeInfo::Relation>())
  {

  }
//
void TNodeInfo::appendRelative(const QString &name, TNodeInfo::Relation relation)
  {
  m_relatives.insert(name,relation);
  }
//
bool TNodeInfo::isValid()
  {
  return (m_size>-1 && m_permanent>-1);
  }
//
QString TNodeInfo::title() const
  {
  return m_title;
  }
//
QString TNodeInfo::description() const
  {
  return m_description;
  }
//
quint16 TNodeInfo::size()
  {
  return m_size;
  }
//
quint8 TNodeInfo::permanent()
  {
  return m_permanent;
  }
//
QMap <QString, TNodeInfo::Relation> TNodeInfo::relatives() const
  {
  return m_relatives;
  }

/* TOrderModel */

/* Для контроля вставки, удаления записей и т.п. можно было бы шаблон использовать,
 * но каждый раз при операциях юзать QXmlSchemaValidator - дорого, тяжелый для вычислений класс,
 * еще и падает иногда по непонятной причине.
 * Потому я со старта создам из шаблонного файла (в ресурсах есть) QMap <QString, TNodeInfo>,
 * он поможет контролировать процесс */
TOrderModel::TOrderModel(QObject *parent) : QAbstractItemModel(parent)
  {
  QString fileName=":template/order.xml";
  QFile file(fileName);
  if (file.open(QIODevice::ReadOnly)==false)
    return;

  QStringList names={"root"}; // для контроля, в какой ветке нахожусь
  QXmlStreamReader stream(&file);
  while (stream.atEnd()==false)
    {
    QXmlStreamReader::TokenType token=stream.readNext();
    QString tag=stream.name().toString();
    // пустые отсекаем сразу, на всякий случай, по-хорошему их не должно быть
    if (tag.isEmpty()==true)
      continue;

    if (token==QXmlStreamReader::EndElement)
       names.removeLast();

    // могут быть всякие комменты и пр. - тоже отсекаем
    if (token!=QXmlStreamReader::StartElement)
     continue;

    QXmlStreamAttributes attributes=stream.attributes();
    QString title=attributes.value("title").toString();
    QString description=attributes.value("description").toString();
    quint16 size=attributes.value("size").toInt();
    // переобуваться на лету, при сохранении потом уже в permanent сохранять
    quint8 permanent=(attributes.hasAttribute("permanent") ? attributes.value("permanent").toInt() : attributes.value("read_only").toInt());
    if (attributes.hasAttribute("permanent")==false)
      m_corrected=true;

    if (m_nodeInfoMap.contains(tag)==false)
      m_nodeInfoMap.insert(tag,TNodeInfo(title,description,size,permanent));

    // что куда добавлять можно, в работе потом поможет, ну напр. чтобы девочки не добавили Специальности к Членам комиссии
    m_nodeInfoMap[names.last()].appendRelative(tag,TNodeInfo::ChildRelation);
    m_nodeInfoMap[tag].appendRelative(tag,TNodeInfo::PeerRelation);

    names.append(tag);
    }
  file.close();

  m_fields<<"title"<<"description"<<"name"<<"size"<<"permanent";
  m_headers<<tr("Title")<<tr("Description")<<tr("Name")<<tr("Size")<<tr("Permanent");

  m_root.parent=nullptr;
  m_root.children=QVector <ItemInfo*>();
  m_root.data<<tr("Root")<<QString("")<<QString("root")<<32767<<1;

  m_rootIndex=index(0,Title,QModelIndex());
  m_changed=false;

  connect(this,&QAbstractItemModel::dataChanged,this,&TOrderModel::dataChanged);
  }
//
TOrderModel::~TOrderModel()
  {
  clear();
  }
//
QModelIndex TOrderModel::index(int row, int column, const QModelIndex &parent) const
  {
  if (hasIndex(row,column,parent)==false)
    return QModelIndex();

  if (parent.isValid()==false)
    return createIndex(row,column,const_cast <ItemInfo*>(&m_root));

  ItemInfo *parentInfo=static_cast <ItemInfo*>(parent.internalPointer());

  if (parentInfo->children.size()>row)
    return createIndex(row,column,parentInfo->children[row]);

  return QModelIndex();
  }
//
QModelIndex TOrderModel::parent(const QModelIndex &index) const
  {
  if (index.isValid()==false)
    return QModelIndex();

  ItemInfo *itemInfo=static_cast <ItemInfo*>(index.internalPointer());
  ItemInfo *parentInfo=itemInfo->parent;

  if (parentInfo==nullptr)
    return QModelIndex();

  if (parentInfo->parent==nullptr)
    return createIndex(0,0,parentInfo);

  QVector <ItemInfo*> parentInfoChildren=parentInfo->parent->children;
  return createIndex(parentInfoChildren.indexOf(parentInfo),0,parentInfo);
  }
//
int TOrderModel::rowCount(const QModelIndex &parent) const
  {
  if (parent.isValid()==false)
    return 1;

  ItemInfo *parentInfo=static_cast <ItemInfo*>(parent.internalPointer());
  return parentInfo->children.size();
  }
//
int TOrderModel::columnCount(const QModelIndex &parent) const
  {
  Q_UNUSED(parent);
  return m_root.data.size();
  }
//
QVariant TOrderModel::data(const QModelIndex &index,int role) const
  {
  if (index.isValid()==false)
    return QVariant();

  switch (role)
    {
    case Qt::DisplayRole:
    case Qt::EditRole:
      {
      ItemInfo *itemInfo=static_cast <ItemInfo*>(index.internalPointer());
      QVector <QVariant> data=itemInfo->data;
      return data.at(index.column());
      }
    case Qt::SizeHintRole:
      return QSize(0,20);
    default:
      return QVariant();
    }
  }
//
bool TOrderModel::setData(const QModelIndex &index,const QVariant &value,int role)
  {
  if ((index.isValid() && role==Qt::EditRole)==false)
    return false;

  ItemInfo *itemInfo=static_cast <ItemInfo*> (index.internalPointer());
  itemInfo->data[index.column()]=value;

  emit dataChanged(index,index.sibling(index.row(),columnCount()));
  return true;
  }
//
Qt::ItemFlags TOrderModel::flags(const QModelIndex &index) const
  {
  Qt::ItemFlags flags=QAbstractItemModel::flags(index);
  if (index.column()==Title || index.column()==Description)
    flags |= Qt::ItemIsEditable;

  return flags;
  }
//
QVariant TOrderModel::headerData(int section,Qt::Orientation orientation,int role) const
  {
  if (orientation!=Qt::Horizontal || section>=m_fields.size())
    return QVariant();

  switch (role)
    {
    case Qt::DisplayRole:
      return m_headers.at(section);
    case Qt::UserRole:
      return m_fields.at(section);
    default:
      return QVariant();
    }
  }
//
bool TOrderModel::setHeaderData(int section,Qt::Orientation orientation,const QVariant &value,int role)
  {
  if (orientation!=Qt::Horizontal || section>=m_root.data.size())
    return false;

  switch (role)
    {
    case Qt::DisplayRole:
      {
      m_headers[section]=value.toString();
      break;
      }
    case Qt::UserRole:
      {
      m_fields[section]=value.toString();
      break;
      }
    default:
      return false;
    }

  emit headerDataChanged(orientation,section,section);
  return true;
  }
//
int TOrderModel::columnPosition(const QString &name)
  {
  return m_fields.indexOf(name);
  }
//
bool TOrderModel::insertColumns(int position,int columns,const QModelIndex &parent)
  {
  /*bool success;
  beginInsertColumns(parent,position,position+columns-1);
  success=m_root->insertColumns(position,columns);
  endInsertColumns();*/

  return QAbstractItemModel::insertColumns(position,columns,parent);
  }
//
bool TOrderModel::removeColumns(int position,int columns,const QModelIndex &parent)
  {
  /*bool success;
  beginRemoveColumns(parent,position,position+columns-1);
  success=m_root->removeColumns(position,columns);
  endRemoveColumns();

  if (m_root->columnCount()==0)
   removeRows(0,rowCount());*/

  return QAbstractItemModel::removeColumns(position,columns,parent);
  }
//
bool TOrderModel::insertRows(int position,int rows,const QModelIndex &parent)
  {
  ItemInfo *parentInfo;
  if (parent.isValid())
    parentInfo=static_cast<ItemInfo*>(parent.internalPointer());
  else
    parentInfo=&m_root;

  if (position==-1 || position>parentInfo->children.size())//-1)
    position=parentInfo->children.size();

  beginInsertRows(parent,position,position+rows-1);
  parentInfo->children.reserve(parentInfo->children.size()+rows);
  for (qint16 i=position;i<position+rows;i++)
    {
    ItemInfo *itemInfo=new ItemInfo();
    for (quint16 j=0;j<m_root.data.size();j++)
      itemInfo->data<<QVariant();

    itemInfo->parent=parentInfo;
    parentInfo->children.insert(position,itemInfo);
    }
  endInsertRows();

  emit dataChanged(index(position,0,parent),index(position+rows-1,columnCount(),parent));
  return true;
  }
//
bool TOrderModel::removeRows(int position,int rows,const QModelIndex &parent)
  {
  ItemInfo *parentInfo;
  if (parent.isValid())
    parentInfo=static_cast<ItemInfo*>(parent.internalPointer());
  else
    parentInfo=&m_root;

  if (position==-1 || rows==0 || position+rows>parentInfo->children.size())
    return false;

  //Мне нужно пройти по всему дереву и грохнуть все дочерние и дочернии дочерних и т.д.
  QVector <ItemInfo*> childInfo=parentInfo->children;
  for (qint16 i=position+rows-1;i>=position;i--)
    {
    QModelIndex childIndex=index(i,0,parent);
    ItemInfo *itemInfo=childInfo.at(i);
    quint16 childSize=itemInfo->children.size();
    if (childSize>0)
      removeRows(0,childSize,childIndex);

    beginRemoveRows(parent,i,i);
    parentInfo->children.remove(i);
    delete itemInfo;
    endRemoveRows();
    }

  emit dataChanged(index(position,0,parent),index(position+rows-1,columnCount(),parent));
  return true;
  }
//
void TOrderModel::clear()
  {
  removeRows(0,m_root.children.size(),m_rootIndex);
  }
//
bool TOrderModel::isChanged()
  {
  return m_changed;
  }
//
void TOrderModel::dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
  {
  m_changed=true;
  }
//
// TODO: может, во fromOrder как-то полный путь учитывать попробовать?
QModelIndex TOrderModel::readXml(const QString &fileName, QModelIndex targetIndex, const QString &fromName)
  {
  m_corrected=false;
  QModelIndex actualIndex;

  QFile file(fileName);
  if (file.open(QIODevice::ReadOnly)==false)
    return QModelIndex();

  disconnect(this,&QAbstractItemModel::dataChanged,this,&TOrderModel::dataChanged);

  if (targetIndex==m_rootIndex)
    clear();

  qint16 row=-1;
  QXmlStreamReader stream(&file);
  while (stream.atEnd()==false)
    {
    QXmlStreamReader::TokenType token=stream.readNext();
    QString tag=stream.name().toString();
    if (token!=QXmlStreamReader::StartElement || tag==fromName)
     continue;

    TOrderModel::AppendResponse response=appendRequest(targetIndex,tag);
    if (response.index.isValid()==false)
      continue;

    if (response.relation==TNodeInfo::ChildRelation)
      {
      actualIndex=readTree(&stream,token,tag,targetIndex,row);
      continue;
      }

    if (targetIndex.parent().isValid()==false)
      return QModelIndex();

    row=targetIndex.row();
    targetIndex=targetIndex.parent();
    }
  file.close();

  connect(this,&QAbstractItemModel::dataChanged,this,&TOrderModel::dataChanged);

  m_changed=m_corrected || (m_changed && (targetIndex!=m_rootIndex));
  return actualIndex;
  }
//
QModelIndex TOrderModel::readTree(QXmlStreamReader *stream, QXmlStreamReader::TokenType token, QString tag, QModelIndex parent, int row)
  {
  QXmlStreamAttributes attributes=stream->attributes();
  quint8 permanent=(attributes.hasAttribute("permanent") ? attributes.value("permanent").toInt() : attributes.value("read_only").toInt());
  if (attributes.hasAttribute("permanent")==false)
    m_corrected=true;

  QVector <QVariant> data{attributes.value("title").toString(), attributes.value("description").toString(), tag, attributes.value("size").toInt(), permanent};
  QPair <bool,int> found=findMatch(parent,data,row);
  row=found.second;

  quint16 actualRow=0;
  QModelIndex actualIndex=QModelIndex();
  if (row != INT16_MIN)
    {
    if (found.first==false) // не надо заменять, надо добавить
      insertRow(row,parent);

    actualRow=(row==-1) ? rowCount(parent)-1 : row;
    actualIndex=index(actualRow,Title,parent);

    TNodeInfo nodeInfo=m_nodeInfoMap.value(tag);
    // size и permanent ставлю не из данных, а из шаблона на всякий случай
    quint16 size=nodeInfo.size();
    quint8 permanent=nodeInfo.permanent();

    setData(index(actualRow,Title,parent),attributes.value("title").toString());
    setData(index(actualRow,Description,parent),attributes.value("description").toString());
    setData(index(actualRow,Name,parent),tag);
    setData(index(actualRow,Size,parent),size);
    setData(index(actualRow,Permanent,parent),permanent);
    }
  else
    {
    actualRow=rowCount(parent)-1;
    actualIndex=index(actualRow,Title,parent);
    }

  while (token!=QXmlStreamReader::EndElement)// || current!=tag)
    {
    token=stream->readNext();
    tag=stream->name().toString();

    AppendResponse response=appendRequest(actualIndex,tag);
    if (token==QXmlStreamReader::StartElement && response.index.isValid())
      readTree(stream,token,tag,actualIndex);
    }

  return actualIndex;
  }
//
bool TOrderModel::writeXml(const QString &fileName, const QModelIndex &source)
  {
  QFile file(fileName);
  if (file.open(QIODevice::WriteOnly | QIODevice::Truncate)==false)
    return false;

  QXmlStreamWriter stream(&file);
  stream.setAutoFormatting(true);

  stream.writeStartDocument();
  stream.writeComment("Order Model File");
  writeTree(&stream,source);
  stream.writeEndDocument();
  file.close();

  m_changed=false;
  return true;
  }
//
void TOrderModel::writeTree(QXmlStreamWriter *stream,const QModelIndex &source)
  {
  if (source!=rootIndex())
    {
    if (index(source.row(),Name,source.parent()).data().toString().isEmpty())
      return;

    stream->writeStartElement(index(source.row(),Name,source.parent()).data().toString());
    stream->writeAttribute("title",index(source.row(),Title,source.parent()).data().toString());
    stream->writeAttribute("description",index(source.row(),Description,source.parent()).data().toString());
    stream->writeAttribute("size",index(source.row(),Size,source.parent()).data().toString());
    stream->writeAttribute("permanent",index(source.row(),Permanent,source.parent()).data().toString());
    }

  for (int i=0;i<rowCount(source);i++)
    writeTree(stream,index(i,Title,source));

  stream->writeEndElement();
  }
//
TOrderModel::ItemInfo *TOrderModel::root()
  {
  return &m_root;
  }
//
QModelIndex TOrderModel::rootIndex() const
  {
  return m_rootIndex;
  }

TNodeInfo TOrderModel::nodeInfo(const QString &name) const
  {
  if (m_nodeInfoMap.contains(name))
    return m_nodeInfoMap.value(name);

  return TNodeInfo();
  }
//
// вренуть надо AppendResponse - куда, каким по счету, Child или Peer и с каким именем, можно вставить
TOrderModel::AppendResponse TOrderModel::appendRequest(const QModelIndex &targetIndex, const QString source)
  {
  if (targetIndex.isValid()==false)
    return TOrderModel::AppendResponse();

  QString name=index(targetIndex.row(),Name,targetIndex.parent()).data().toString();
  TNodeInfo nodeInfo=m_nodeInfoMap.value(name);
  QMap <QString, TNodeInfo::Relation> relatives=nodeInfo.relatives();

  // если спрашиваем, что вот сюда то (targetIndex) хотим вот то-то (source) добавить
  if (source.isEmpty()==false)
    {
    if (relatives.contains(source)==false || relatives.value(source)==TNodeInfo::InvalidRelation)
      return TOrderModel::AppendResponse();

    TNodeInfo::Relation relation=relatives.value(source);
    QModelIndex actualIndex=(relation==TNodeInfo::ChildRelation ? targetIndex : targetIndex.parent());
    quint16 size=index(actualIndex.row(),Size,actualIndex.parent()).data().toInt();
    quint16 count=rowCount(actualIndex);

    if (count>=size)
      return TOrderModel::AppendResponse();

    qint16 row=(relation==TNodeInfo::ChildRelation ? -1 : targetIndex.row());
    return TOrderModel::AppendResponse(actualIndex,row,source,relation);
    }

  // если тут, то просто пустую запись хотим вставить
  QList <TNodeInfo::Relation> relations=relatives.values();
  relations.removeAll(TNodeInfo::InvalidRelation);
  if (relations.isEmpty())
    return TOrderModel::AppendResponse();
  // в общем случае может быть и ChildRelation и PeerRelation, ChildRelation имеет приоритетет, мне нужна сортировка
  std::sort(relations.begin(),relations.end(),[this](TNodeInfo::Relation first, TNodeInfo::Relation second) {
    return first < second;
    });

  foreach (const TNodeInfo::Relation &relation, relations)
    {
    QModelIndex actualIndex=(relation==TNodeInfo::ChildRelation ? targetIndex : targetIndex.parent());
    quint16 size=index(actualIndex.row(),Size,actualIndex.parent()).data().toInt();
    if (size > rowCount(actualIndex))
      {
      // определить, кого же мы добавляем
      qint16 row=(relation==TNodeInfo::ChildRelation ? -1 : targetIndex.row());
      name=relatives.key(relation);
      return TOrderModel::AppendResponse(actualIndex,row,name,relation);
      }
    }

  return TOrderModel::AppendResponse();
  }
//
QStringList TOrderModel::fullPath(QModelIndex current, QStringList path)
  {
  path.prepend(index(current.row(),Title,current.parent()).data().toString());//+QString(" -> "));
  current=current.parent();
  if (index(current.row(),Title,current.parent())!=rootIndex())
    path=fullPath(current,path);

  return path;
  }
//
QPair <bool,qint16> TOrderModel::findMatch(const QModelIndex &parent, const QVector <QVariant> &data, qint16 row)
  {
  quint16 size=index(parent.row(),Size,parent.parent()).data().toUInt();
  quint16 count=rowCount(parent);

  QModelIndexList foundNames=match(index(0,Name,parent),Qt::DisplayRole,data.at(Name).toString(),-1,Qt::MatchExactly); // найти data.Name
  //qDebug()<<foundNames.size()<<data.at(Name)<<data.at(Title)<<count<<size;
  // может, и не было ни одного совпадения по Name, тогда смело добавляем
  if (foundNames.size()==0)
    return QPair <bool,int>(false,row);
  // надо еще и Title посмотреть на совпадение
  foreach (const QModelIndex found, foundNames)
    {
    if (index(found.row(),Title,found.parent()).data().toString()!=data.at(Title).toString())
      continue;

    // нашли совпадение и по Title тоже; и, в общем случае, можно как заменить так и добавить новую запись
    // если ранее было выбрано "Всё добавить"
    if (m_answer==AppendToAllAnswer)
      return QPair <bool,qint16>(false,found.row());
    // если ранее было выбрано "Все пропустить"
    if (m_answer==SkipToAllAnswer)
      return QPair <bool,qint16>(false,INT16_MIN);
    // если ранее было выбрано "Всё заменить"
    if (m_answer==ReplaceToAllAnswer)
      return QPair <bool,qint16>(true,found.row());
    // если нельзя добавить больше, то заменить принудительно
    if (count>=size)
      return QPair <bool,qint16>(true,row);

    // если parent - QWidget, то мы можем показать диалог, тогда меняем сами просто
    QWidget *widget=qobject_cast <QWidget*>(QObject::parent());
    if (widget==nullptr)
      return QPair <bool,qint16>(true,found.row());

    QString path=fullPath(index(found.row(),Title,parent)).join(" -> ");
    QMessageBox *messageBox=new QMessageBox(QMessageBox::Question,tr("Confirmation"),tr("The following Item allready exists in Order.\n'%1'\n\nWhat need to do?").arg(path),QMessageBox::NoButton,widget);
    messageBox->setWindowModality(Qt::ApplicationModal);

    QCheckBox *checkBox=new QCheckBox(tr("Remember the choice."),messageBox);
    messageBox->setCheckBox(checkBox);

    messageBox->addButton(tr("Replace"),QMessageBox::YesRole);
    messageBox->addButton(tr("Append"),QMessageBox::AcceptRole);
    messageBox->addButton(tr("Skip"),QMessageBox::RejectRole);

    messageBox->adjustSize();

    messageBox->exec();
    QMessageBox::ButtonRole button=messageBox->buttonRole(messageBox->clickedButton());
    bool chaecked=messageBox->checkBox()->isChecked();
    delete messageBox;

    switch (button)
      {
      case QMessageBox::YesRole:
        {
        if (chaecked==true)
          m_answer=ReplaceToAllAnswer;
        return QPair <bool,qint16>(true,found.row());
        }
      case QMessageBox::AcceptRole:
        {
        if (chaecked==true)
          m_answer=AppendToAllAnswer;
        return QPair <bool,qint16>(false,found.row());
        }
      case QMessageBox::RejectRole:
      default:
        {
        m_answer=SkipToAllAnswer;
        return QPair <bool,qint16>(false,INT16_MIN);
        }
      }
    }

  // оказались тут, значит не нашли такого же, если можно добавить - то добавляем
  if (count<size)
    return QPair <bool,qint16>(false,row);

  // если нельзя добавить, и нельзя спросить у юзера, то пропускаем, что делать
  QWidget *widget=qobject_cast <QWidget*>(QObject::parent());
  if (widget==nullptr)
    return QPair <bool,qint16>(false,INT16_MIN);

  // если нельзя добавить и можно спросить, то спросим, надо ли заменить первый из существующих на
  if (QMessageBox::question(widget,tr("Confirmation"),tr("Not found the record with same Title,\nand can't append another.\nReplace first existing record?"))==QMessageBox::Yes)
    return QPair <bool,qint16>(true,foundNames.at(0).row());

  return QPair <bool,qint16>(false,INT16_MIN);
  }
//
void TOrderModel::presetAnswer(Answer answer)
  {
  m_answer=answer;
  }
