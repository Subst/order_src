#include "TSqlTableModel.h"

#include <QSqlRecord>

TSqlTableModel::TSqlTableModel(QObject *parent, const QSqlDatabase &dataBase) : QSqlTableModel(parent,dataBase), m_relations(QList <TSqlRelation>())
  {

  }
//
bool TSqlTableModel::select()
  {
  bool success=QSqlTableModel::select();
  while (canFetchMore())
    fetchMore();

  return success;
  }

void TSqlTableModel::setSort(int column, Qt::SortOrder order)
  {
  m_sortColumn=column;
  QSqlTableModel::setSort(column, order);
  }
//
void TSqlTableModel::sort(int column, Qt::SortOrder order)
  {
  m_sortColumn=column;
  QSqlTableModel::sort(column,order);
  }
//
quint16 TSqlTableModel::sortFieldIndex()
  {
  return m_sortColumn;
  }
//
void TSqlTableModel::setTitle(const QString &title)
  {
  if (m_title!=title)
    {
    m_title=title;
    emit titleChanged(m_title);
    }
  }
//
QString TSqlTableModel::title() const
  {
  return m_title;
  }
//
void TSqlTableModel::setHeaderField(const QString &field)
  {
  if (record().contains(field) && m_headerField!=field)
    {
    m_headerField=field;
    emit headerFieldChanged(m_headerField);
    }
  }
//
QString TSqlTableModel::headerField() const
  {
  return m_headerField;
  }
//
void TSqlTableModel::setDescriptionField(const QString &field)
  {
  if (record().contains(field) && m_descriptionField!=field)
    {
    m_descriptionField=field;
    emit descriptionFieldChanged(m_descriptionField);
    }
  }
//
QString TSqlTableModel::descriptionField() const
  {
  return m_descriptionField;
  }
//
void TSqlTableModel::setNodeType(const QString &type)
  {
  if (m_nodeType!=type)
    {
    m_nodeType=type;
    emit nodeTypeChanged(m_nodeType);
    }
  }
//
QString TSqlTableModel::nodeType() const
  {
  return m_nodeType;
  }
//
void TSqlTableModel::setRelations(const QList<TSqlRelation> &relations)
  {
  bool valid=true;
  foreach (TSqlRelation relation,relations)
    valid &= isValidRelation(relation);

  if (valid)
    m_relations=relations;
  }
//
void TSqlTableModel::clearRelations()
  {
  m_relations.clear();
  }
//
void TSqlTableModel::appendRelation(const TSqlRelation &relation)
  {
  if (isValidRelation(relation) && m_relations.contains(relation)==false)
    m_relations.append(relation);
  }
//
void TSqlTableModel::removeRelation(const TSqlRelation &relation)
  {
  if (m_relations.contains(relation))
    m_relations.removeAll(relation);
  }
//
QList <TSqlRelation> TSqlTableModel::relations() const
  {
  return m_relations;
  }
//
bool TSqlTableModel::isValidRelation(const TSqlRelation &relation) const
  {
  QString relatedTable=relation.relatedTable();
  if (record().contains(relation.field())==false || database().tables().contains(relatedTable)==false)
    return false;

  QSqlRecord record=database().record(relatedTable);
  return record.contains(relation.indexField()) && record.contains(relation.relatedField());
  }
