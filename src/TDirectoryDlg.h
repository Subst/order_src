#pragma once

#include "ui_TDirectoryDlg.h"

class TDataModule;
class TSqlTableModel;
class QToolBar;
class TItemDelegate;

class TDirectoryDlg : public QDialog, private Ui::TDirectoryDlg
  {
    Q_OBJECT

  public:
    explicit TDirectoryDlg(QWidget *parent, TSqlTableModel *tableModel);

  protected:
    void initCore();
    void initIface();
    void readSettings();
    void writeSettings();

    void setupTableIndex(const QModelIndex &index, int position);
    void adjustActions();

  private:
    TDataModule *m_dataModule;
    TSqlTableModel *m_tableModel;
    TItemDelegate *m_tableDelegate;

    QToolBar *toolBar;

  protected slots:
    void reject() override;

    void appendRecord();
    void editRecord();
    void editInWindow();
    void removeRecord();
    void downRecord();
    void upRecord();
    void loadList();
    void clearList();
    void sortList(bool);

    void dataChanged(const QModelIndex &topLeft,const QModelIndex &bottomRight,const QVector<int> &roles=QVector<int> ());
    void tableCurrentChanged(const QModelIndex &current,const QModelIndex &previous);
    void tableSelectionChanged(const QItemSelection &selected,const QItemSelection &deselected);

  signals:
    sortChanged(bool checked);
  };
