#include "TCollegeModel.h"

#include <QSqlRecord>

TCollegeModel::TCollegeModel(QObject *parent, const QSqlDatabase &dataBase) : TSqlTableModel(parent, dataBase)
  {
  setTable("college");
  setTitle(tr("College"));

  setHeaderField("title");

  setEditStrategy(QSqlTableModel::OnFieldChange);
  }
//
void TCollegeModel::setHeaderNames()
  {
  setHeaderData(fieldIndex("title"),Qt::Horizontal,tr("Title"),Qt::DisplayRole);
  setHeaderData(fieldIndex("city"),Qt::Horizontal,tr("City"),Qt::DisplayRole);
  setHeaderData(fieldIndex("region"),Qt::Horizontal,tr("Region"),Qt::DisplayRole);
  setHeaderData(fieldIndex("rector"),Qt::Horizontal,tr("Rector"),Qt::DisplayRole);

  for (int i=0;i<record().count();i++)
    setHeaderData(i,Qt::Horizontal,record().fieldName(i),Qt::UserRole);
  }
//
QString TCollegeModel::selectStatement() const
  {
  QString statement="select college.title, college.city, college.region, college.rector from college";

  if (filter().isEmpty()==false)
    statement.append(" where "+filter());

  return statement;
  }
