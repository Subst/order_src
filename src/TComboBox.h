#pragma once

#include <QComboBox>

class TSqlTableModel;

class TComboBox : public QComboBox
  {
    Q_OBJECT
  public:
    explicit TComboBox(QWidget *parent=nullptr);
    void showPopup();
    void setModel(TSqlTableModel *model, const QStringList &valueFields, const QString &indexField, const QString &filter=QString());

  protected:
    void keyReleaseEvent(QKeyEvent*);
  };
