﻿#include <QApplication>
#include <QTranslator>
#include <QDebug>

#include "QStyleFactory"
#include "TMainWnd.h"

int main(int argc, char *argv[])
  {
  QApplication App(argc, argv);
  App.setStyle(QStyleFactory::create("Windows"));

  QTranslator mainTranslator;
  if (mainTranslator.load(qApp->applicationDirPath()+"/translations/order_"+QLocale::system().name()))
    App.installTranslator(&mainTranslator);

  QTranslator reportTranslator;
  if (reportTranslator.load(qApp->applicationDirPath()+"/translations/limereport_"+QLocale::system().name()))
    App.installTranslator(&reportTranslator);

  QTranslator baseTranslator;
  if (baseTranslator.load(qApp->applicationDirPath()+"/translations/qtbase_"+QLocale::system().name()))
    App.installTranslator(&baseTranslator);

  QTranslator designerTranslator;
  if (designerTranslator.load(qApp->applicationDirPath()+"/translations/designer_"+QLocale::system().name()))
    App.installTranslator(&designerTranslator);

  TMainWnd MainWnd;
  MainWnd.show();
  return App.exec();
  }
