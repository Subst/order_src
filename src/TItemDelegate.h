#pragma once

#include <QStyledItemDelegate>

class TItemDelegate : public QStyledItemDelegate
  {
    Q_OBJECT
  public:
    TItemDelegate(QObject *parent=nullptr);

  protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QWidget *createEditor(QWidget *owner, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
  };
