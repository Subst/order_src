#include "TDataModule.h"

#include "TProfileModel.h"
#include "TSpecialityModel.h"
#include "TAttestationModel.h"
#include "TPersonModel.h"

#include "TCollegeModel.h"

#include <QSqlField>
#include <QDebug>

TDataModule *TDataModule::m_instance=nullptr;

TDataModule::TDataModule(QObject *parent) : QObject(parent)
  {
  m_instance=this; // static var

  m_profileModel=nullptr;
  m_specialityModel=nullptr;
  m_attestationModel=nullptr;
  m_personModel=nullptr;

  m_collegeModel=nullptr;
  }
//
TDataModule *TDataModule::instanse()
  {
  return m_instance;
  }
//
bool TDataModule::openBase(QString baseName)
  {
  if (m_orderBase.isOpen())
    m_orderBase.close();

  if (baseName.isEmpty()==false)
    {
    if (QSqlDatabase::contains("order_base")==false)
      m_orderBase=QSqlDatabase::addDatabase("QSQLITE","order_base");
    m_orderBase.setDatabaseName(baseName);
    if (m_orderBase.open()==false || m_orderBase.isValid()==false || m_orderBase.tables().contains("persons")==false)
      {
      if (m_orderBase.isOpen())
        m_orderBase.close();
      return false;
      }
    }

  if (m_collegeModel==nullptr)
    m_collegeModel=new TCollegeModel(this,m_orderBase);
  m_tableHash["college"]=m_collegeModel;

  if (m_profileModel==nullptr)
    m_profileModel=new TProfileModel(this,m_orderBase);
  m_tableHash["profiles"]=m_profileModel;

  if (m_specialityModel==nullptr)
    m_specialityModel=new TSpecialityModel(this,m_orderBase);
  m_tableHash["specialities"]=m_specialityModel;

  if (m_attestationModel==nullptr)
    m_attestationModel=new TAttestationModel(this,m_orderBase);
  m_tableHash["attestations"]=m_attestationModel;

  if (m_personModel==nullptr)
    m_personModel=new TPersonModel(this,m_orderBase);
   m_tableHash["persons"]=m_personModel;

  foreach (TSqlTableModel *model,m_tableHash.values())
    {
    model->select();
    model->setHeaderNames();
    }

  m_orderBase.rollback();
  return true;
  }
//
void TDataModule::closeBase()
  {
  m_orderBase.commit();

  if (m_orderBase.isOpen())
    m_orderBase.close();
  }
//
QSqlDatabase TDataModule::database() const
  {
  return m_orderBase;
  }
//
QString TDataModule::baseName() const
  {
  return m_orderBase.databaseName();
  }
//
TSqlTableModel *TDataModule::tableModel(const QString &table)
  {
  Q_ASSERT_X(m_tableHash.contains(table),"TDataModule::tableModel",QString("Table doesn't exists: '%1'").arg(table).toLocal8Bit().data());
  return m_tableHash.value(table);
  }
//
QSqlRecord TDataModule::tableRecord(const QString &table) const
  {
  QSqlRecord record=m_orderBase.record(table);
  for (int i=record.count()-1;i>-1;i--)
    {
    if (record.field(i).isAutoValue())
      record.remove(i);
    }
  return record;
  }
//
quint8 TDataModule::fieldIndex(const QString &table, const QString &field)
  {
  TSqlTableModel *model=tableModel(table);
  if (model==nullptr)
    return -1;

  QSqlRecord record=tableRecord(table);
  if (field=="header_field")
    return record.indexOf(model->headerField());

  return record.indexOf(field);
  }
//
void TDataModule::updateModel(TSqlTableModel *model)
  {
  if (model==nullptr)
    return;

  model->select();
  while (model->canFetchMore())
    model->fetchMore();
  }
//
void TDataModule::updateModel(const QString &table)
  {
  updateModel(tableModel(table));
  }
//
