#include "TItemDelegate.h"
#include "TComboBox.h"

#include <QSpinBox>
#include <QSqlTableModel>
#include <QSqlRecord>
#include <QSqlField>
#include <QDebug>
#include <QCheckBox>
#include <QFileDialog>
#include <QLineEdit>
#include <QPlainTextEdit>

#include "TDataModule.h"
#include "TSqlRelation.h"
#include "TSqlTableModel.h"

TItemDelegate::TItemDelegate(QObject *parent) : QStyledItemDelegate(parent)
  {
  // Этот делегат универсальный, используется для отображения данных многих моделей.
  // Модель, задающая данные в конкретный момент времени, определяется по индексу, когда она (модель) необходима
  }
//
void TItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
  {
  QStyleOptionViewItem itemOption=option;

  const QSqlTableModel *tableModel=qobject_cast<const TSqlTableModel*>(index.model());
  QVariant::Type type=tableModel->record().field(index.column()).type();

  if (itemOption.state & QStyle::State_HasFocus)
    {
    if (itemOption.state & QStyle::State_Selected)
      {
      itemOption.state ^= QStyle::State_Selected;
      itemOption.backgroundBrush=qApp->palette().mid();
      }
    else
      itemOption.backgroundBrush=qApp->palette().window();
    }

  if (type==QVariant::Bool)
    {
    itemOption.text=index.data().toBool() ? tr("Yes") : tr("No");
    itemOption.displayAlignment=Qt::AlignVCenter | Qt::AlignLeft;
    qApp->style()->drawControl(QStyle::CE_ItemViewItem,&itemOption,painter);
    return;
    }

  if (type==QVariant::Int)
    {
    itemOption.text=QString::number(index.data().toInt());
    itemOption.displayAlignment=Qt::AlignVCenter | Qt::AlignRight;
    qApp->style()->drawControl(QStyle::CE_ItemViewItem,&itemOption,painter);
    return;
    }

  itemOption.text=index.data().toString();
  qApp->style()->drawControl(QStyle::CE_ItemViewItem,&itemOption,painter);
  }
//
QWidget* TItemDelegate::createEditor(QWidget *owner, const QStyleOptionViewItem &option, const QModelIndex &index) const
  {
  const TSqlTableModel *tableModel=qobject_cast<const TSqlTableModel*>(index.model());
  QVariant::Type type=tableModel->record().field(index.column()).type();

  QSqlRecord record=tableModel->record();
  foreach (TSqlRelation relation,tableModel->relations())
    {
    if (index.column()==record.indexOf(relation.visibleField()) && tableModel->isValidRelation(relation))
      {
      TComboBox *comboBox=new TComboBox(owner);
      comboBox->setMaximumWidth(option.rect.width());
      comboBox->setModel(TDataModule::instanse()->tableModel(relation.relatedTable()),QStringList()<<relation.relatedField(),relation.indexField());
      return comboBox;
      }
    }

  if (type==QVariant::Bool)
    {
    TComboBox *comboBox=new TComboBox(owner);
    comboBox->addItem(tr("No"),0);
    comboBox->addItem(tr("Yes"),1);
    return comboBox;
    }

  if (type==QVariant::Int)
    {
    QSpinBox *spinBox=new QSpinBox(owner);
    spinBox->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
    return spinBox;
    }

  if (type==QVariant::String)
    {
    QLineEdit *lineEdit=new QLineEdit(owner);
    return lineEdit;
    }

  return QStyledItemDelegate::createEditor(owner,option,index);
  }
//
void TItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
  {
  const TSqlTableModel *tableModel=qobject_cast<const TSqlTableModel*>(index.model());
  QVariant::Type type=tableModel->record().field(index.column()).type();

  QSqlRecord record=tableModel->record();
  foreach (TSqlRelation relation,tableModel->relations())
    {
    if (index.column()==record.indexOf(relation.visibleField()) && tableModel->isValidRelation(relation))
      {
      TComboBox *comboBox=qobject_cast <TComboBox*>(editor);
      comboBox->setCurrentIndex(comboBox->findData(tableModel->index(index.row(),record.indexOf(relation.field())).data().toInt()));
      return;
      }
    }

  if (type==QVariant::Bool)
    {
    TComboBox *comboBox=qobject_cast <TComboBox*>(editor);
    comboBox->setCurrentIndex(index.data().toInt());
    return;
    }

  if (type==QVariant::String)
    {
    QLineEdit *lineEdit=qobject_cast <QLineEdit*>(editor);
    lineEdit->setText(index.data().toString());
    return;
    }

  QStyledItemDelegate::setEditorData(editor,index);
  }
//
void TItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
  {
  const TSqlTableModel *tableModel=qobject_cast<const TSqlTableModel*>(index.model());
  QVariant::Type type=tableModel->record().field(index.column()).type();

  QSqlRecord record=tableModel->record();
  foreach (TSqlRelation relation, tableModel->relations())
    {
    if (index.column()==record.indexOf(relation.visibleField()) && tableModel->isValidRelation(relation))
      {
      TComboBox *comboBox=qobject_cast <TComboBox*>(editor);
      model->setData(tableModel->index(index.row(),record.indexOf(relation.field())),comboBox->itemData(comboBox->currentIndex()));
      return;
      }
    }

  if (type==QVariant::Bool)
    {
    TComboBox *comboBox=qobject_cast <TComboBox*>(editor);
    model->setData(index,comboBox->currentIndex());
    return;
    }

  if (type==QVariant::String)
    {
    QLineEdit *lineEdit=qobject_cast <QLineEdit*>(editor);
    model->setData(index,lineEdit->text());
    return;
    }

  QStyledItemDelegate::setModelData(editor,model,index);
  }
