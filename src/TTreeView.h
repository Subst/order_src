#pragma once

#include <QTreeView>

class TTreeView : public QTreeView
  {
    Q_OBJECT
  public:
    explicit TTreeView(QWidget *parent=nullptr);

  protected:
    void keyPressEvent(QKeyEvent *event);

  signals:
    void ctrlEnterPressed(const QModelIndex &index);
    void ctrlDeletePressed(const QModelIndex &index);
    void ctrlInsertPressed(const QModelIndex &index);
    void ctrlPageDownPressed(const QModelIndex &index);
    void ctrlPageUpPressed(const QModelIndex &index);
    void ctrlEndPressed(const QModelIndex &index);
    void ctrlHomePressed(const QModelIndex &index);
  };

