#include "TAttestationModel.h"

#include <QSqlRecord>

TAttestationModel::TAttestationModel(QObject *parent, const QSqlDatabase &dataBase) : TSqlTableModel(parent, dataBase)
  {
  setTable("attestations");
  setTitle(tr("Attestations"));

  setHeaderField("attestation");
  setNodeType("attestation_item");

  setSort(fieldIndex("id"),Qt::AscendingOrder);
  setEditStrategy(QSqlTableModel::OnFieldChange);
  }
//
void TAttestationModel::setHeaderNames()
  {
  setHeaderData(fieldIndex("id"),Qt::Horizontal,tr("ID"),Qt::DisplayRole);
  setHeaderData(fieldIndex("attestation"),Qt::Horizontal,tr("Attestation"),Qt::DisplayRole);

  for (int i=0;i<record().count();i++)
    setHeaderData(i,Qt::Horizontal,record().fieldName(i),Qt::UserRole);
  }
//
QString TAttestationModel::selectStatement() const
  {
  QString statement="select attestations.id, attestations.attestation from attestations";

  if (filter().isEmpty()==false)
    statement.append(" where "+filter());

  statement.append(" "+orderByClause());
  return statement;
  }
//
