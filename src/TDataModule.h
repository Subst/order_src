#pragma once

#include <QSqlDatabase>
#include <QObject>
#include <QApplication>
#include <QSqlRecord>

class TSqlTableModel;

class TProfileModel;
class TSpecialityModel;
class TAttestationModel;
class TPersonModel;

class TCollegeModel;

class TDataModule : public QObject
  {
    Q_OBJECT
  public:
    TDataModule(QObject *parent);
    static TDataModule *instanse();

    bool openBase(QString baseName=qApp->applicationDirPath()+"/dbase/order.sqt");
    void closeBase();

    QSqlDatabase database() const;
    QString baseName() const;

    TSqlTableModel *tableModel(const QString &table);
    QSqlRecord tableRecord(const QString &table) const;
    quint8 fieldIndex(const QString &table, const QString &field="header_field");

    void updateModel(TSqlTableModel *model);
    void updateModel(const QString &table);

  private:
    static TDataModule *m_instance;

    QString m_baseName;
    QSqlDatabase m_orderBase;

    TProfileModel *m_profileModel;
    TSpecialityModel *m_specialityModel;

    TAttestationModel *m_attestationModel;
    TPersonModel *m_personModel;

    TCollegeModel *m_collegeModel;
    QHash <QString, TSqlTableModel*> m_tableHash;
  };
