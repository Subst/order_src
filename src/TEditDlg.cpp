#include "TEditDlg.h"
#include "TSettings.h"

TEditDlg::TEditDlg(QWidget *parent, const QString &text, bool readOnly) : QDialog(parent)
  {
  setupUi(this);

  textEdit->setReadOnly(readOnly);
  textEdit->setPlainText(text);
  initCore();
  initIface();
  }
void TEditDlg::initCore()
  {
  connect(okBtn,&QPushButton::clicked,this,&TEditDlg::accept);
  connect(cancelBtn,&QPushButton::clicked,this,&TEditDlg::reject);
  }
//
void TEditDlg::initIface()
  {
  readSettings();
  }
//
void TEditDlg::readSettings()
  {
  restoreGeometry(TSettings().getXmlValue("edit_dialog/geometry","",0).toByteArray());
  }
//
void TEditDlg::writeSettings()
  {
  TSettings().setXmlValue("edit_dialog/geometry","",saveGeometry());
  }
//
void TEditDlg::accept()
  {
  writeSettings();
  QDialog::accept();
  }
//
void TEditDlg::reject()
  {
  writeSettings();
  QDialog::reject();
  }
//
QString TEditDlg::text() const
  {
  return textEdit->toPlainText();
  }
