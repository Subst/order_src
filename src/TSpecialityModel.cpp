#include "TSpecialityModel.h"

#include <QSqlRecord>

TSpecialityModel::TSpecialityModel(QObject *parent, const QSqlDatabase &dataBase) : TSqlTableModel(parent, dataBase)
  {
  setTable("specialities");
  setTitle(tr("Specialities"));

  setHeaderField("speciality");
  setNodeType("speciality");

  setSort(fieldIndex("id"),Qt::AscendingOrder);
  setEditStrategy(QSqlTableModel::OnFieldChange);
  }
//
void TSpecialityModel::setHeaderNames()
  {
  setHeaderData(fieldIndex("id"),Qt::Horizontal,tr("ID"),Qt::DisplayRole);
  setHeaderData(fieldIndex("speciality"),Qt::Horizontal,tr("Speciality"),Qt::DisplayRole);

  for (int i=0;i<record().count();i++)
    setHeaderData(i,Qt::Horizontal,record().fieldName(i),Qt::UserRole);
  }
//
QString TSpecialityModel::selectStatement() const
  {
  QString statement="select specialities.id, specialities.speciality from specialities";

  if (filter().isEmpty()==false)
    statement.append(" where "+filter());

  statement.append(" "+orderByClause());
  return statement;
  }
//
