#include "TTableView.h"

#include <QKeyEvent>
#include <QMouseEvent>
#include <QToolTip>
#include <QDebug>

TTableView::TTableView(QWidget *parent) : QTableView(parent)
  {

  }
//
void TTableView::keyPressEvent(QKeyEvent *event)
  {
  // сначала Ctrl + Enter(Return)
  if (event->modifiers().testFlag(Qt::ControlModifier)==true && (event->key()==Qt::Key_Enter || event->key()==Qt::Key_Return))
    {
    emit ctrlEnterPressed(currentIndex());
    return;
    }

  // осталось просто Enter(Return)
  // TODO: если description, то сразу в окне делать (или если текст длинный, но насколько длинный? - QFonMetrics)?
  /*if ((event->key()==Qt::Key_Enter || event->key()==Qt::Key_Return) && state()!=QAbstractItemView::EditingState &&
      editTriggers().testFlag(QAbstractItemView::EditKeyPressed)==true)
    {
    edit(currentIndex());
    return;
    }*/

  QTableView::keyPressEvent(event);
  }
//
void TTableView::mousePressEvent(QMouseEvent *event)
  {
  QTableView::mousePressEvent(event);
  if (event->button()==Qt::LeftButton)
    QToolTip::showText(mapToGlobal(event->pos()), currentIndex().data().toString());
  }
//
