/********************************************************************************
** Form generated from reading UI file 'TCollegeDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TCOLLEGEDLG_H
#define UI_TCOLLEGEDLG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_TCollegeInfoDlg
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *titleLbl;
    QPlainTextEdit *titleEdit;
    QLabel *cityLbl;
    QLineEdit *cityEdit;
    QLabel *regionLbl;
    QLineEdit *regionEdit;
    QLabel *rectorLbl;
    QLineEdit *rectorEdit;
    QFrame *buttonFrame;
    QPushButton *cancelBtn;
    QPushButton *okBtn;

    void setupUi(QDialog *TCollegeInfoDlg)
    {
        if (TCollegeInfoDlg->objectName().isEmpty())
            TCollegeInfoDlg->setObjectName(QString::fromUtf8("TCollegeInfoDlg"));
        TCollegeInfoDlg->resize(420, 280);
        TCollegeInfoDlg->setMinimumSize(QSize(420, 280));
        verticalLayout = new QVBoxLayout(TCollegeInfoDlg);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        titleLbl = new QLabel(TCollegeInfoDlg);
        titleLbl->setObjectName(QString::fromUtf8("titleLbl"));
        titleLbl->setMinimumSize(QSize(0, 16));
        titleLbl->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(titleLbl);

        titleEdit = new QPlainTextEdit(TCollegeInfoDlg);
        titleEdit->setObjectName(QString::fromUtf8("titleEdit"));

        verticalLayout->addWidget(titleEdit);

        cityLbl = new QLabel(TCollegeInfoDlg);
        cityLbl->setObjectName(QString::fromUtf8("cityLbl"));
        cityLbl->setMinimumSize(QSize(0, 16));
        cityLbl->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(cityLbl);

        cityEdit = new QLineEdit(TCollegeInfoDlg);
        cityEdit->setObjectName(QString::fromUtf8("cityEdit"));

        verticalLayout->addWidget(cityEdit);

        regionLbl = new QLabel(TCollegeInfoDlg);
        regionLbl->setObjectName(QString::fromUtf8("regionLbl"));
        regionLbl->setMinimumSize(QSize(0, 16));
        regionLbl->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(regionLbl);

        regionEdit = new QLineEdit(TCollegeInfoDlg);
        regionEdit->setObjectName(QString::fromUtf8("regionEdit"));

        verticalLayout->addWidget(regionEdit);

        rectorLbl = new QLabel(TCollegeInfoDlg);
        rectorLbl->setObjectName(QString::fromUtf8("rectorLbl"));
        rectorLbl->setMinimumSize(QSize(0, 16));
        rectorLbl->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(rectorLbl);

        rectorEdit = new QLineEdit(TCollegeInfoDlg);
        rectorEdit->setObjectName(QString::fromUtf8("rectorEdit"));

        verticalLayout->addWidget(rectorEdit);

        buttonFrame = new QFrame(TCollegeInfoDlg);
        buttonFrame->setObjectName(QString::fromUtf8("buttonFrame"));
        buttonFrame->setMinimumSize(QSize(0, 28));
        buttonFrame->setMaximumSize(QSize(16777215, 28));
        cancelBtn = new QPushButton(buttonFrame);
        cancelBtn->setObjectName(QString::fromUtf8("cancelBtn"));
        cancelBtn->setGeometry(QRect(128, 4, 120, 23));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/cancel"), QSize(), QIcon::Normal, QIcon::Off);
        cancelBtn->setIcon(icon);
        cancelBtn->setAutoDefault(false);
        okBtn = new QPushButton(buttonFrame);
        okBtn->setObjectName(QString::fromUtf8("okBtn"));
        okBtn->setGeometry(QRect(4, 4, 120, 23));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/ok"), QSize(), QIcon::Normal, QIcon::Off);
        okBtn->setIcon(icon1);
        okBtn->setAutoDefault(false);

        verticalLayout->addWidget(buttonFrame);


        retranslateUi(TCollegeInfoDlg);

        QMetaObject::connectSlotsByName(TCollegeInfoDlg);
    } // setupUi

    void retranslateUi(QDialog *TCollegeInfoDlg)
    {
        TCollegeInfoDlg->setWindowTitle(QCoreApplication::translate("TCollegeInfoDlg", "College Info", nullptr));
        titleLbl->setText(QCoreApplication::translate("TCollegeInfoDlg", "College Title", nullptr));
        cityLbl->setText(QCoreApplication::translate("TCollegeInfoDlg", "City", nullptr));
        regionLbl->setText(QCoreApplication::translate("TCollegeInfoDlg", "Region", nullptr));
        rectorLbl->setText(QCoreApplication::translate("TCollegeInfoDlg", "Rector", nullptr));
        cancelBtn->setText(QCoreApplication::translate("TCollegeInfoDlg", "Cancel", nullptr));
        okBtn->setText(QCoreApplication::translate("TCollegeInfoDlg", "Ok", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TCollegeInfoDlg: public Ui_TCollegeInfoDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TCOLLEGEDLG_H
