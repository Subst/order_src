/********************************************************************************
** Form generated from reading UI file 'TMainWnd.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TMAINWND_H
#define UI_TMAINWND_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QWidget>
#include "src/TTableView.h"

QT_BEGIN_NAMESPACE

class Ui_TMainWnd
{
public:
    QAction *openOrderActn;
    QAction *saveOrderActn;
    QAction *exitActn;
    QAction *specialityActn;
    QAction *aboutQtActn;
    QAction *aboutActn;
    QAction *rankActn;
    QAction *degreeActn;
    QAction *profileActn;
    QAction *attestationActn;
    QAction *personActn;
    QAction *collegeActn;
    QAction *newOrderActn;
    QAction *designReportActn;
    QAction *previewReportActn;
    QAction *printReportActn;
    QAction *printReportToPdfActn;
    QAction *createDatabaseActn;
    QAction *openDatabaseActn;
    QAction *saveDatabaseActn;
    QAction *removeDatabaseActn;
    QAction *removeOrderActn;
    QAction *viewDatabaseActn;
    QAction *viewOrderActn;
    QAction *viewReportActn;
    QAction *viewServiceActn;
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QSplitter *mainSplitter;
    QFrame *frame;
    QGridLayout *gridLayout_2;
    QSplitter *tableSplitter;
    QFrame *specialityFrame;
    QGridLayout *gridLayout_3;
    TTableView *specialityTable;
    QLabel *specialityLbl;
    QFrame *profileFrame;
    QGridLayout *gridLayout_4;
    QLabel *profileLbl;
    TTableView *profileTable;
    QFrame *personFrame;
    QGridLayout *gridLayout_5;
    QLabel *personLbl;
    TTableView *personTable;
    QFrame *attestationFrame;
    QGridLayout *gridLayout_6;
    QLabel *attestationLbl;
    TTableView *attestationTable;
    QTreeView *orderTree;
    QMenuBar *mainMenu;
    QMenu *fileMenu;
    QMenu *optionsMenu;
    QMenu *directoryMenu;
    QMenu *helpMenu;
    QMenu *reportMenu;
    QMenu *viewMenu;
    QStatusBar *statusbar;
    QToolBar *databaseToolBar;
    QToolBar *orderToolBar;
    QToolBar *reportToolBar;
    QToolBar *serviceToolBar;

    void setupUi(QMainWindow *TMainWnd)
    {
        if (TMainWnd->objectName().isEmpty())
            TMainWnd->setObjectName(QString::fromUtf8("TMainWnd"));
        TMainWnd->resize(941, 629);
        TMainWnd->setMinimumSize(QSize(800, 600));
        openOrderActn = new QAction(TMainWnd);
        openOrderActn->setObjectName(QString::fromUtf8("openOrderActn"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/document_open"), QSize(), QIcon::Normal, QIcon::Off);
        openOrderActn->setIcon(icon);
        saveOrderActn = new QAction(TMainWnd);
        saveOrderActn->setObjectName(QString::fromUtf8("saveOrderActn"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/document_save"), QSize(), QIcon::Normal, QIcon::Off);
        saveOrderActn->setIcon(icon1);
        exitActn = new QAction(TMainWnd);
        exitActn->setObjectName(QString::fromUtf8("exitActn"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/exit"), QSize(), QIcon::Normal, QIcon::Off);
        exitActn->setIcon(icon2);
        specialityActn = new QAction(TMainWnd);
        specialityActn->setObjectName(QString::fromUtf8("specialityActn"));
        aboutQtActn = new QAction(TMainWnd);
        aboutQtActn->setObjectName(QString::fromUtf8("aboutQtActn"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/qt"), QSize(), QIcon::Normal, QIcon::Off);
        aboutQtActn->setIcon(icon3);
        aboutActn = new QAction(TMainWnd);
        aboutActn->setObjectName(QString::fromUtf8("aboutActn"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/info"), QSize(), QIcon::Normal, QIcon::Off);
        aboutActn->setIcon(icon4);
        rankActn = new QAction(TMainWnd);
        rankActn->setObjectName(QString::fromUtf8("rankActn"));
        degreeActn = new QAction(TMainWnd);
        degreeActn->setObjectName(QString::fromUtf8("degreeActn"));
        profileActn = new QAction(TMainWnd);
        profileActn->setObjectName(QString::fromUtf8("profileActn"));
        attestationActn = new QAction(TMainWnd);
        attestationActn->setObjectName(QString::fromUtf8("attestationActn"));
        personActn = new QAction(TMainWnd);
        personActn->setObjectName(QString::fromUtf8("personActn"));
        collegeActn = new QAction(TMainWnd);
        collegeActn->setObjectName(QString::fromUtf8("collegeActn"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/home"), QSize(), QIcon::Normal, QIcon::Off);
        collegeActn->setIcon(icon5);
        newOrderActn = new QAction(TMainWnd);
        newOrderActn->setObjectName(QString::fromUtf8("newOrderActn"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/document_new"), QSize(), QIcon::Normal, QIcon::Off);
        newOrderActn->setIcon(icon6);
        designReportActn = new QAction(TMainWnd);
        designReportActn->setObjectName(QString::fromUtf8("designReportActn"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/images/design"), QSize(), QIcon::Normal, QIcon::Off);
        designReportActn->setIcon(icon7);
        previewReportActn = new QAction(TMainWnd);
        previewReportActn->setObjectName(QString::fromUtf8("previewReportActn"));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/images/preview"), QSize(), QIcon::Normal, QIcon::Off);
        previewReportActn->setIcon(icon8);
        printReportActn = new QAction(TMainWnd);
        printReportActn->setObjectName(QString::fromUtf8("printReportActn"));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/images/print"), QSize(), QIcon::Normal, QIcon::Off);
        printReportActn->setIcon(icon9);
        printReportToPdfActn = new QAction(TMainWnd);
        printReportToPdfActn->setObjectName(QString::fromUtf8("printReportToPdfActn"));
        QIcon icon10;
        icon10.addFile(QString::fromUtf8(":/images/pdf"), QSize(), QIcon::Normal, QIcon::Off);
        printReportToPdfActn->setIcon(icon10);
        createDatabaseActn = new QAction(TMainWnd);
        createDatabaseActn->setObjectName(QString::fromUtf8("createDatabaseActn"));
        QIcon icon11;
        icon11.addFile(QString::fromUtf8(":/images/database_new"), QSize(), QIcon::Normal, QIcon::Off);
        createDatabaseActn->setIcon(icon11);
        openDatabaseActn = new QAction(TMainWnd);
        openDatabaseActn->setObjectName(QString::fromUtf8("openDatabaseActn"));
        QIcon icon12;
        icon12.addFile(QString::fromUtf8(":/images/database_open"), QSize(), QIcon::Normal, QIcon::Off);
        openDatabaseActn->setIcon(icon12);
        saveDatabaseActn = new QAction(TMainWnd);
        saveDatabaseActn->setObjectName(QString::fromUtf8("saveDatabaseActn"));
        QIcon icon13;
        icon13.addFile(QString::fromUtf8(":/images/database_save"), QSize(), QIcon::Normal, QIcon::Off);
        saveDatabaseActn->setIcon(icon13);
        removeDatabaseActn = new QAction(TMainWnd);
        removeDatabaseActn->setObjectName(QString::fromUtf8("removeDatabaseActn"));
        QIcon icon14;
        icon14.addFile(QString::fromUtf8(":/images/database_remove"), QSize(), QIcon::Normal, QIcon::Off);
        removeDatabaseActn->setIcon(icon14);
        removeOrderActn = new QAction(TMainWnd);
        removeOrderActn->setObjectName(QString::fromUtf8("removeOrderActn"));
        QIcon icon15;
        icon15.addFile(QString::fromUtf8(":/images/document_remove"), QSize(), QIcon::Normal, QIcon::Off);
        removeOrderActn->setIcon(icon15);
        viewDatabaseActn = new QAction(TMainWnd);
        viewDatabaseActn->setObjectName(QString::fromUtf8("viewDatabaseActn"));
        viewDatabaseActn->setCheckable(true);
        viewDatabaseActn->setChecked(false);
        QIcon icon16;
        icon16.addFile(QString::fromUtf8(":/images/database"), QSize(), QIcon::Normal, QIcon::Off);
        viewDatabaseActn->setIcon(icon16);
        viewOrderActn = new QAction(TMainWnd);
        viewOrderActn->setObjectName(QString::fromUtf8("viewOrderActn"));
        viewOrderActn->setCheckable(true);
        viewOrderActn->setChecked(false);
        QIcon icon17;
        icon17.addFile(QString::fromUtf8(":/images/documents"), QSize(), QIcon::Normal, QIcon::Off);
        viewOrderActn->setIcon(icon17);
        viewReportActn = new QAction(TMainWnd);
        viewReportActn->setObjectName(QString::fromUtf8("viewReportActn"));
        viewReportActn->setCheckable(true);
        viewReportActn->setChecked(false);
        QIcon icon18;
        icon18.addFile(QString::fromUtf8(":/images/report"), QSize(), QIcon::Normal, QIcon::Off);
        viewReportActn->setIcon(icon18);
        viewServiceActn = new QAction(TMainWnd);
        viewServiceActn->setObjectName(QString::fromUtf8("viewServiceActn"));
        viewServiceActn->setCheckable(true);
        viewServiceActn->setChecked(false);
        QIcon icon19;
        icon19.addFile(QString::fromUtf8(":/images/tools"), QSize(), QIcon::Normal, QIcon::Off);
        viewServiceActn->setIcon(icon19);
        centralwidget = new QWidget(TMainWnd);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setSpacing(0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        mainSplitter = new QSplitter(centralwidget);
        mainSplitter->setObjectName(QString::fromUtf8("mainSplitter"));
        mainSplitter->setOrientation(Qt::Horizontal);
        mainSplitter->setChildrenCollapsible(false);
        frame = new QFrame(mainSplitter);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(200, 0));
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Sunken);
        gridLayout_2 = new QGridLayout(frame);
        gridLayout_2->setSpacing(0);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        tableSplitter = new QSplitter(frame);
        tableSplitter->setObjectName(QString::fromUtf8("tableSplitter"));
        tableSplitter->setOrientation(Qt::Vertical);
        specialityFrame = new QFrame(tableSplitter);
        specialityFrame->setObjectName(QString::fromUtf8("specialityFrame"));
        specialityFrame->setFrameShape(QFrame::NoFrame);
        specialityFrame->setFrameShadow(QFrame::Raised);
        gridLayout_3 = new QGridLayout(specialityFrame);
        gridLayout_3->setSpacing(0);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        specialityTable = new TTableView(specialityFrame);
        specialityTable->setObjectName(QString::fromUtf8("specialityTable"));
        specialityTable->setContextMenuPolicy(Qt::CustomContextMenu);
        specialityTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        specialityTable->setAlternatingRowColors(true);
        specialityTable->setSelectionBehavior(QAbstractItemView::SelectRows);
        specialityTable->horizontalHeader()->setMinimumSectionSize(10);
        specialityTable->verticalHeader()->setMinimumSectionSize(20);
        specialityTable->verticalHeader()->setDefaultSectionSize(20);

        gridLayout_3->addWidget(specialityTable, 1, 0, 1, 1);

        specialityLbl = new QLabel(specialityFrame);
        specialityLbl->setObjectName(QString::fromUtf8("specialityLbl"));
        specialityLbl->setMinimumSize(QSize(0, 16));
        specialityLbl->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(specialityLbl, 0, 0, 1, 1);

        tableSplitter->addWidget(specialityFrame);
        profileFrame = new QFrame(tableSplitter);
        profileFrame->setObjectName(QString::fromUtf8("profileFrame"));
        profileFrame->setFrameShape(QFrame::NoFrame);
        profileFrame->setFrameShadow(QFrame::Raised);
        gridLayout_4 = new QGridLayout(profileFrame);
        gridLayout_4->setSpacing(0);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        profileLbl = new QLabel(profileFrame);
        profileLbl->setObjectName(QString::fromUtf8("profileLbl"));
        profileLbl->setMinimumSize(QSize(0, 16));
        profileLbl->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(profileLbl, 0, 0, 1, 1);

        profileTable = new TTableView(profileFrame);
        profileTable->setObjectName(QString::fromUtf8("profileTable"));
        profileTable->setContextMenuPolicy(Qt::CustomContextMenu);
        profileTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        profileTable->setAlternatingRowColors(true);
        profileTable->setSelectionBehavior(QAbstractItemView::SelectRows);
        profileTable->horizontalHeader()->setMinimumSectionSize(10);
        profileTable->verticalHeader()->setMinimumSectionSize(20);
        profileTable->verticalHeader()->setDefaultSectionSize(20);

        gridLayout_4->addWidget(profileTable, 1, 0, 1, 1);

        tableSplitter->addWidget(profileFrame);
        personFrame = new QFrame(tableSplitter);
        personFrame->setObjectName(QString::fromUtf8("personFrame"));
        personFrame->setFrameShape(QFrame::NoFrame);
        personFrame->setFrameShadow(QFrame::Raised);
        gridLayout_5 = new QGridLayout(personFrame);
        gridLayout_5->setSpacing(0);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_5->setContentsMargins(0, 0, 0, 0);
        personLbl = new QLabel(personFrame);
        personLbl->setObjectName(QString::fromUtf8("personLbl"));
        personLbl->setMinimumSize(QSize(0, 16));
        personLbl->setAlignment(Qt::AlignCenter);

        gridLayout_5->addWidget(personLbl, 0, 0, 1, 1);

        personTable = new TTableView(personFrame);
        personTable->setObjectName(QString::fromUtf8("personTable"));
        personTable->setContextMenuPolicy(Qt::CustomContextMenu);
        personTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        personTable->setAlternatingRowColors(true);
        personTable->setSelectionBehavior(QAbstractItemView::SelectRows);
        personTable->horizontalHeader()->setMinimumSectionSize(10);
        personTable->verticalHeader()->setMinimumSectionSize(20);
        personTable->verticalHeader()->setDefaultSectionSize(20);

        gridLayout_5->addWidget(personTable, 1, 0, 1, 1);

        tableSplitter->addWidget(personFrame);
        attestationFrame = new QFrame(tableSplitter);
        attestationFrame->setObjectName(QString::fromUtf8("attestationFrame"));
        attestationFrame->setFrameShape(QFrame::NoFrame);
        attestationFrame->setFrameShadow(QFrame::Raised);
        gridLayout_6 = new QGridLayout(attestationFrame);
        gridLayout_6->setSpacing(0);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        gridLayout_6->setContentsMargins(0, 0, 0, 0);
        attestationLbl = new QLabel(attestationFrame);
        attestationLbl->setObjectName(QString::fromUtf8("attestationLbl"));
        attestationLbl->setMinimumSize(QSize(0, 16));
        attestationLbl->setAlignment(Qt::AlignCenter);

        gridLayout_6->addWidget(attestationLbl, 0, 0, 1, 1);

        attestationTable = new TTableView(attestationFrame);
        attestationTable->setObjectName(QString::fromUtf8("attestationTable"));
        attestationTable->setMouseTracking(true);
        attestationTable->setContextMenuPolicy(Qt::CustomContextMenu);
        attestationTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        attestationTable->setAlternatingRowColors(true);
        attestationTable->setSelectionBehavior(QAbstractItemView::SelectRows);
        attestationTable->horizontalHeader()->setMinimumSectionSize(10);
        attestationTable->verticalHeader()->setMinimumSectionSize(20);
        attestationTable->verticalHeader()->setDefaultSectionSize(20);

        gridLayout_6->addWidget(attestationTable, 1, 0, 1, 1);

        tableSplitter->addWidget(attestationFrame);

        gridLayout_2->addWidget(tableSplitter, 0, 0, 1, 1);

        mainSplitter->addWidget(frame);
        orderTree = new QTreeView(mainSplitter);
        orderTree->setObjectName(QString::fromUtf8("orderTree"));
        orderTree->setMinimumSize(QSize(200, 0));
        orderTree->setContextMenuPolicy(Qt::CustomContextMenu);
        orderTree->setAlternatingRowColors(true);
        orderTree->setSelectionMode(QAbstractItemView::SingleSelection);
        mainSplitter->addWidget(orderTree);

        gridLayout->addWidget(mainSplitter, 0, 0, 1, 1);

        TMainWnd->setCentralWidget(centralwidget);
        mainMenu = new QMenuBar(TMainWnd);
        mainMenu->setObjectName(QString::fromUtf8("mainMenu"));
        mainMenu->setGeometry(QRect(0, 0, 941, 21));
        fileMenu = new QMenu(mainMenu);
        fileMenu->setObjectName(QString::fromUtf8("fileMenu"));
        optionsMenu = new QMenu(mainMenu);
        optionsMenu->setObjectName(QString::fromUtf8("optionsMenu"));
        directoryMenu = new QMenu(optionsMenu);
        directoryMenu->setObjectName(QString::fromUtf8("directoryMenu"));
        QIcon icon20;
        icon20.addFile(QString::fromUtf8(":/images/dictionary"), QSize(), QIcon::Normal, QIcon::Off);
        directoryMenu->setIcon(icon20);
        helpMenu = new QMenu(mainMenu);
        helpMenu->setObjectName(QString::fromUtf8("helpMenu"));
        reportMenu = new QMenu(mainMenu);
        reportMenu->setObjectName(QString::fromUtf8("reportMenu"));
        viewMenu = new QMenu(mainMenu);
        viewMenu->setObjectName(QString::fromUtf8("viewMenu"));
        TMainWnd->setMenuBar(mainMenu);
        statusbar = new QStatusBar(TMainWnd);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        TMainWnd->setStatusBar(statusbar);
        databaseToolBar = new QToolBar(TMainWnd);
        databaseToolBar->setObjectName(QString::fromUtf8("databaseToolBar"));
        TMainWnd->addToolBar(Qt::TopToolBarArea, databaseToolBar);
        orderToolBar = new QToolBar(TMainWnd);
        orderToolBar->setObjectName(QString::fromUtf8("orderToolBar"));
        TMainWnd->addToolBar(Qt::TopToolBarArea, orderToolBar);
        reportToolBar = new QToolBar(TMainWnd);
        reportToolBar->setObjectName(QString::fromUtf8("reportToolBar"));
        TMainWnd->addToolBar(Qt::TopToolBarArea, reportToolBar);
        serviceToolBar = new QToolBar(TMainWnd);
        serviceToolBar->setObjectName(QString::fromUtf8("serviceToolBar"));
        TMainWnd->addToolBar(Qt::TopToolBarArea, serviceToolBar);

        mainMenu->addAction(fileMenu->menuAction());
        mainMenu->addAction(optionsMenu->menuAction());
        mainMenu->addAction(reportMenu->menuAction());
        mainMenu->addAction(viewMenu->menuAction());
        mainMenu->addAction(helpMenu->menuAction());
        fileMenu->addAction(createDatabaseActn);
        fileMenu->addAction(removeDatabaseActn);
        fileMenu->addAction(openDatabaseActn);
        fileMenu->addAction(saveDatabaseActn);
        fileMenu->addSeparator();
        fileMenu->addAction(newOrderActn);
        fileMenu->addAction(removeOrderActn);
        fileMenu->addAction(openOrderActn);
        fileMenu->addAction(saveOrderActn);
        fileMenu->addSeparator();
        fileMenu->addAction(exitActn);
        optionsMenu->addAction(collegeActn);
        optionsMenu->addSeparator();
        optionsMenu->addAction(directoryMenu->menuAction());
        optionsMenu->addSeparator();
        directoryMenu->addAction(specialityActn);
        directoryMenu->addAction(profileActn);
        directoryMenu->addSeparator();
        directoryMenu->addAction(attestationActn);
        directoryMenu->addSeparator();
        directoryMenu->addAction(personActn);
        helpMenu->addAction(aboutQtActn);
        helpMenu->addAction(aboutActn);
        reportMenu->addAction(designReportActn);
        reportMenu->addAction(previewReportActn);
        reportMenu->addAction(printReportActn);
        reportMenu->addAction(printReportToPdfActn);
        viewMenu->addAction(viewDatabaseActn);
        viewMenu->addAction(viewOrderActn);
        viewMenu->addAction(viewReportActn);
        viewMenu->addAction(viewServiceActn);
        databaseToolBar->addAction(createDatabaseActn);
        databaseToolBar->addSeparator();
        databaseToolBar->addAction(removeDatabaseActn);
        databaseToolBar->addSeparator();
        databaseToolBar->addAction(openDatabaseActn);
        databaseToolBar->addAction(saveDatabaseActn);
        databaseToolBar->addSeparator();
        databaseToolBar->addSeparator();
        orderToolBar->addAction(newOrderActn);
        orderToolBar->addSeparator();
        orderToolBar->addAction(removeOrderActn);
        orderToolBar->addSeparator();
        orderToolBar->addAction(openOrderActn);
        orderToolBar->addSeparator();
        orderToolBar->addAction(saveOrderActn);
        orderToolBar->addSeparator();
        reportToolBar->addAction(designReportActn);
        reportToolBar->addSeparator();
        reportToolBar->addAction(previewReportActn);
        reportToolBar->addSeparator();
        reportToolBar->addAction(printReportActn);
        reportToolBar->addSeparator();
        reportToolBar->addAction(printReportToPdfActn);
        reportToolBar->addSeparator();
        serviceToolBar->addAction(exitActn);
        serviceToolBar->addSeparator();

        retranslateUi(TMainWnd);

        QMetaObject::connectSlotsByName(TMainWnd);
    } // setupUi

    void retranslateUi(QMainWindow *TMainWnd)
    {
        TMainWnd->setWindowTitle(QCoreApplication::translate("TMainWnd", "Order", nullptr));
        openOrderActn->setText(QCoreApplication::translate("TMainWnd", "Open Order", nullptr));
        saveOrderActn->setText(QCoreApplication::translate("TMainWnd", "Save Order", nullptr));
        exitActn->setText(QCoreApplication::translate("TMainWnd", "Exit", nullptr));
        specialityActn->setText(QCoreApplication::translate("TMainWnd", "Specialities", nullptr));
        aboutQtActn->setText(QCoreApplication::translate("TMainWnd", "About QT", nullptr));
        aboutActn->setText(QCoreApplication::translate("TMainWnd", "About program", nullptr));
        rankActn->setText(QCoreApplication::translate("TMainWnd", "Ranks", nullptr));
        degreeActn->setText(QCoreApplication::translate("TMainWnd", "Degrees", nullptr));
        profileActn->setText(QCoreApplication::translate("TMainWnd", "Profiles", nullptr));
        attestationActn->setText(QCoreApplication::translate("TMainWnd", "Attestations", nullptr));
        personActn->setText(QCoreApplication::translate("TMainWnd", "Persons", nullptr));
        collegeActn->setText(QCoreApplication::translate("TMainWnd", "College Info", nullptr));
#if QT_CONFIG(tooltip)
        collegeActn->setToolTip(QCoreApplication::translate("TMainWnd", "College Info", nullptr));
#endif // QT_CONFIG(tooltip)
        newOrderActn->setText(QCoreApplication::translate("TMainWnd", "New Order", nullptr));
        designReportActn->setText(QCoreApplication::translate("TMainWnd", "Design Report", nullptr));
        previewReportActn->setText(QCoreApplication::translate("TMainWnd", "Preview Report", nullptr));
        printReportActn->setText(QCoreApplication::translate("TMainWnd", "Print Report", nullptr));
        printReportToPdfActn->setText(QCoreApplication::translate("TMainWnd", "Print Report To PDF", nullptr));
        createDatabaseActn->setText(QCoreApplication::translate("TMainWnd", "Create Database", nullptr));
        openDatabaseActn->setText(QCoreApplication::translate("TMainWnd", "Open Database", nullptr));
        saveDatabaseActn->setText(QCoreApplication::translate("TMainWnd", "Save Database", nullptr));
        removeDatabaseActn->setText(QCoreApplication::translate("TMainWnd", "Remove Database", nullptr));
        removeOrderActn->setText(QCoreApplication::translate("TMainWnd", "Remove Order", nullptr));
        viewDatabaseActn->setText(QCoreApplication::translate("TMainWnd", "Database toolbar", nullptr));
        viewOrderActn->setText(QCoreApplication::translate("TMainWnd", "Orders toolbar", nullptr));
#if QT_CONFIG(tooltip)
        viewOrderActn->setToolTip(QCoreApplication::translate("TMainWnd", "Orders toolbar", nullptr));
#endif // QT_CONFIG(tooltip)
        viewReportActn->setText(QCoreApplication::translate("TMainWnd", "Reports toolbar", nullptr));
        viewServiceActn->setText(QCoreApplication::translate("TMainWnd", "Service toolbar", nullptr));
        specialityLbl->setText(QCoreApplication::translate("TMainWnd", "Specialities", nullptr));
        profileLbl->setText(QCoreApplication::translate("TMainWnd", "Profiles", nullptr));
        personLbl->setText(QCoreApplication::translate("TMainWnd", "Persons", nullptr));
        attestationLbl->setText(QCoreApplication::translate("TMainWnd", "Attestations", nullptr));
        fileMenu->setTitle(QCoreApplication::translate("TMainWnd", "&File", nullptr));
        optionsMenu->setTitle(QCoreApplication::translate("TMainWnd", "&Options", nullptr));
        directoryMenu->setTitle(QCoreApplication::translate("TMainWnd", "Directories", nullptr));
        helpMenu->setTitle(QCoreApplication::translate("TMainWnd", "&Help", nullptr));
        reportMenu->setTitle(QCoreApplication::translate("TMainWnd", "&Report", nullptr));
        viewMenu->setTitle(QCoreApplication::translate("TMainWnd", "&View", nullptr));
        databaseToolBar->setWindowTitle(QCoreApplication::translate("TMainWnd", "Database toolbar", nullptr));
        orderToolBar->setWindowTitle(QCoreApplication::translate("TMainWnd", "Orders toolbar", nullptr));
        reportToolBar->setWindowTitle(QCoreApplication::translate("TMainWnd", "Reports toolbar", nullptr));
        serviceToolBar->setWindowTitle(QCoreApplication::translate("TMainWnd", "Service toolbar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TMainWnd: public Ui_TMainWnd {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TMAINWND_H
