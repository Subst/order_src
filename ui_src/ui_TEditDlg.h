/********************************************************************************
** Form generated from reading UI file 'TEditDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TEDITDLG_H
#define UI_TEDITDLG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_TEditDlg
{
public:
    QVBoxLayout *verticalLayout;
    QPlainTextEdit *textEdit;
    QFrame *buttonFrame;
    QPushButton *cancelBtn;
    QPushButton *okBtn;

    void setupUi(QDialog *TEditDlg)
    {
        if (TEditDlg->objectName().isEmpty())
            TEditDlg->setObjectName(QString::fromUtf8("TEditDlg"));
        TEditDlg->resize(420, 300);
        TEditDlg->setMinimumSize(QSize(420, 280));
        verticalLayout = new QVBoxLayout(TEditDlg);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        textEdit = new QPlainTextEdit(TEditDlg);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));

        verticalLayout->addWidget(textEdit);

        buttonFrame = new QFrame(TEditDlg);
        buttonFrame->setObjectName(QString::fromUtf8("buttonFrame"));
        buttonFrame->setMinimumSize(QSize(0, 28));
        buttonFrame->setMaximumSize(QSize(16777215, 28));
        cancelBtn = new QPushButton(buttonFrame);
        cancelBtn->setObjectName(QString::fromUtf8("cancelBtn"));
        cancelBtn->setGeometry(QRect(128, 4, 120, 23));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/cancel"), QSize(), QIcon::Normal, QIcon::Off);
        cancelBtn->setIcon(icon);
        cancelBtn->setAutoDefault(false);
        okBtn = new QPushButton(buttonFrame);
        okBtn->setObjectName(QString::fromUtf8("okBtn"));
        okBtn->setGeometry(QRect(4, 4, 120, 23));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/ok"), QSize(), QIcon::Normal, QIcon::Off);
        okBtn->setIcon(icon1);
        okBtn->setAutoDefault(false);

        verticalLayout->addWidget(buttonFrame);


        retranslateUi(TEditDlg);

        QMetaObject::connectSlotsByName(TEditDlg);
    } // setupUi

    void retranslateUi(QDialog *TEditDlg)
    {
        TEditDlg->setWindowTitle(QCoreApplication::translate("TEditDlg", "Text", nullptr));
        cancelBtn->setText(QCoreApplication::translate("TEditDlg", "Cancel", nullptr));
        okBtn->setText(QCoreApplication::translate("TEditDlg", "Ok", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TEditDlg: public Ui_TEditDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TEDITDLG_H
