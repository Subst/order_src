/********************************************************************************
** Form generated from reading UI file 'TDirectoryDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TDIRECTORYDLG_H
#define UI_TDIRECTORYDLG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <src/TTableView.h>

QT_BEGIN_NAMESPACE

class Ui_TDirectoryDlg
{
public:
    QAction *removeRecordActn;
    QAction *appendRecordActn;
    QAction *editRecordActn;
    QAction *loadListActn;
    QAction *clearListActn;
    QAction *sortListActn;
    QAction *upRecordActn;
    QAction *downRecordActn;
    QAction *editInWindowActn;
    QVBoxLayout *mainLayout;
    TTableView *tableView;
    QFrame *buttonFrame;
    QPushButton *okBtn;

    void setupUi(QDialog *TDirectoryDlg)
    {
        if (TDirectoryDlg->objectName().isEmpty())
            TDirectoryDlg->setObjectName(QString::fromUtf8("TDirectoryDlg"));
        TDirectoryDlg->resize(320, 240);
        TDirectoryDlg->setMinimumSize(QSize(320, 240));
        removeRecordActn = new QAction(TDirectoryDlg);
        removeRecordActn->setObjectName(QString::fromUtf8("removeRecordActn"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/remove"), QSize(), QIcon::Normal, QIcon::Off);
        removeRecordActn->setIcon(icon);
        appendRecordActn = new QAction(TDirectoryDlg);
        appendRecordActn->setObjectName(QString::fromUtf8("appendRecordActn"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/append"), QSize(), QIcon::Normal, QIcon::Off);
        appendRecordActn->setIcon(icon1);
        editRecordActn = new QAction(TDirectoryDlg);
        editRecordActn->setObjectName(QString::fromUtf8("editRecordActn"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/edit"), QSize(), QIcon::Normal, QIcon::Off);
        editRecordActn->setIcon(icon2);
        loadListActn = new QAction(TDirectoryDlg);
        loadListActn->setObjectName(QString::fromUtf8("loadListActn"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/load"), QSize(), QIcon::Normal, QIcon::Off);
        loadListActn->setIcon(icon3);
        clearListActn = new QAction(TDirectoryDlg);
        clearListActn->setObjectName(QString::fromUtf8("clearListActn"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/empty"), QSize(), QIcon::Normal, QIcon::Off);
        clearListActn->setIcon(icon4);
        sortListActn = new QAction(TDirectoryDlg);
        sortListActn->setObjectName(QString::fromUtf8("sortListActn"));
        sortListActn->setCheckable(true);
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/sort"), QSize(), QIcon::Normal, QIcon::Off);
        sortListActn->setIcon(icon5);
        upRecordActn = new QAction(TDirectoryDlg);
        upRecordActn->setObjectName(QString::fromUtf8("upRecordActn"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/up"), QSize(), QIcon::Normal, QIcon::Off);
        upRecordActn->setIcon(icon6);
        downRecordActn = new QAction(TDirectoryDlg);
        downRecordActn->setObjectName(QString::fromUtf8("downRecordActn"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/images/down"), QSize(), QIcon::Normal, QIcon::Off);
        downRecordActn->setIcon(icon7);
        editInWindowActn = new QAction(TDirectoryDlg);
        editInWindowActn->setObjectName(QString::fromUtf8("editInWindowActn"));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/images/editraise"), QSize(), QIcon::Normal, QIcon::Off);
        editInWindowActn->setIcon(icon8);
        mainLayout = new QVBoxLayout(TDirectoryDlg);
        mainLayout->setSpacing(0);
        mainLayout->setObjectName(QString::fromUtf8("mainLayout"));
        mainLayout->setContentsMargins(0, 0, 0, 0);
        tableView = new TTableView(TDirectoryDlg);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setContextMenuPolicy(Qt::ActionsContextMenu);
        tableView->setEditTriggers(QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed);
        tableView->setAlternatingRowColors(true);
        tableView->setSelectionMode(QAbstractItemView::ExtendedSelection);
        tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableView->verticalHeader()->setMinimumSectionSize(20);
        tableView->verticalHeader()->setDefaultSectionSize(20);

        mainLayout->addWidget(tableView);

        buttonFrame = new QFrame(TDirectoryDlg);
        buttonFrame->setObjectName(QString::fromUtf8("buttonFrame"));
        buttonFrame->setMinimumSize(QSize(0, 28));
        buttonFrame->setMaximumSize(QSize(16777215, 28));
        okBtn = new QPushButton(buttonFrame);
        okBtn->setObjectName(QString::fromUtf8("okBtn"));
        okBtn->setGeometry(QRect(4, 4, 120, 23));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/images/ok"), QSize(), QIcon::Normal, QIcon::Off);
        okBtn->setIcon(icon9);
        okBtn->setAutoDefault(false);

        mainLayout->addWidget(buttonFrame);


        retranslateUi(TDirectoryDlg);

        okBtn->setDefault(false);


        QMetaObject::connectSlotsByName(TDirectoryDlg);
    } // setupUi

    void retranslateUi(QDialog *TDirectoryDlg)
    {
        TDirectoryDlg->setWindowTitle(QCoreApplication::translate("TDirectoryDlg", "Directory", nullptr));
        removeRecordActn->setText(QCoreApplication::translate("TDirectoryDlg", "Remove Record", nullptr));
#if QT_CONFIG(tooltip)
        removeRecordActn->setToolTip(QCoreApplication::translate("TDirectoryDlg", "Remove Record", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        removeRecordActn->setShortcut(QCoreApplication::translate("TDirectoryDlg", "Ctrl+D", nullptr));
#endif // QT_CONFIG(shortcut)
        appendRecordActn->setText(QCoreApplication::translate("TDirectoryDlg", "Append Record", nullptr));
#if QT_CONFIG(tooltip)
        appendRecordActn->setToolTip(QCoreApplication::translate("TDirectoryDlg", "Append Record", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        appendRecordActn->setShortcut(QCoreApplication::translate("TDirectoryDlg", "Ctrl+N", nullptr));
#endif // QT_CONFIG(shortcut)
        editRecordActn->setText(QCoreApplication::translate("TDirectoryDlg", "Edit Record", nullptr));
#if QT_CONFIG(tooltip)
        editRecordActn->setToolTip(QCoreApplication::translate("TDirectoryDlg", "Edit Record", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        editRecordActn->setShortcut(QCoreApplication::translate("TDirectoryDlg", "Ctrl+E", nullptr));
#endif // QT_CONFIG(shortcut)
        loadListActn->setText(QCoreApplication::translate("TDirectoryDlg", "Load List", nullptr));
#if QT_CONFIG(tooltip)
        loadListActn->setToolTip(QCoreApplication::translate("TDirectoryDlg", "Load List", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        loadListActn->setShortcut(QCoreApplication::translate("TDirectoryDlg", "Ctrl+L", nullptr));
#endif // QT_CONFIG(shortcut)
        clearListActn->setText(QCoreApplication::translate("TDirectoryDlg", "Clear List", nullptr));
#if QT_CONFIG(tooltip)
        clearListActn->setToolTip(QCoreApplication::translate("TDirectoryDlg", "Clear List", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        clearListActn->setShortcut(QCoreApplication::translate("TDirectoryDlg", "Ctrl+C", nullptr));
#endif // QT_CONFIG(shortcut)
        sortListActn->setText(QCoreApplication::translate("TDirectoryDlg", "Sort List", nullptr));
#if QT_CONFIG(tooltip)
        sortListActn->setToolTip(QCoreApplication::translate("TDirectoryDlg", "Sort List", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        sortListActn->setShortcut(QCoreApplication::translate("TDirectoryDlg", "Ctrl+S", nullptr));
#endif // QT_CONFIG(shortcut)
        upRecordActn->setText(QCoreApplication::translate("TDirectoryDlg", "Move Up", nullptr));
#if QT_CONFIG(tooltip)
        upRecordActn->setToolTip(QCoreApplication::translate("TDirectoryDlg", "Move Up", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        upRecordActn->setShortcut(QCoreApplication::translate("TDirectoryDlg", "Ctrl+Up", nullptr));
#endif // QT_CONFIG(shortcut)
        downRecordActn->setText(QCoreApplication::translate("TDirectoryDlg", "Move Down", nullptr));
#if QT_CONFIG(tooltip)
        downRecordActn->setToolTip(QCoreApplication::translate("TDirectoryDlg", "Move Down", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        downRecordActn->setShortcut(QCoreApplication::translate("TDirectoryDlg", "Ctrl+Down", nullptr));
#endif // QT_CONFIG(shortcut)
        editInWindowActn->setText(QCoreApplication::translate("TDirectoryDlg", "Edit in window", nullptr));
#if QT_CONFIG(shortcut)
        editInWindowActn->setShortcut(QCoreApplication::translate("TDirectoryDlg", "Ctrl+W", nullptr));
#endif // QT_CONFIG(shortcut)
        okBtn->setText(QCoreApplication::translate("TDirectoryDlg", "Ok", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TDirectoryDlg: public Ui_TDirectoryDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TDIRECTORYDLG_H
